import React from 'react';
import { Helmet } from 'react-helmet';
import { PageProps } from 'gatsby';

import Layout from '../components/Layout/Layout';
import Hero from '../components/Hero/Hero';
import Section from '../components/Section/Section';
import Button from '../components/Button/Button';
import Iframe from '../components/Iframe/Iframe';
import TableOfContents from '../components/TableOfContents/TableOfContents';
import ArticleSlider from '../components/ArticleSlider/ArticleSlider';

// Article Slider
import SilonaArticle from '../images/about/slider/silona-bonewald.png';
import TeamArticle from '../images/about/slider/team-photo.png';
import MediumArticle from '../images/about/slider/cover_medium.png';
import FlossArticle from '../images/about/slider/floss_albumart_mask.png';

const Events: React.FC<PageProps> = () => {
  return (
    <Layout>
      <Helmet>
        <title>Events - IEEE SA OPEN</title>
      </Helmet>
      <Hero heroIntro={'IEEE SA Open'} heroPrimary={'Events'} />
      <TableOfContents
      items={[
        {
          text: 'SA OPEN Events Calendar',
          link: '#calendar',
        },
        {
          text: 'Submit Event',
          link: '#submit',
        },
        {
          text: 'Prior Events',
          link: '#prior',
        },
        {
          text: 'Training Events',
          link: '#training',
        },
      ]}
    />
      <Section type={'light'}>
        <h1>Events</h1>
        <a className={'anchor'} id={'calendar'} />
        <p>
          This greater Open Source event calendar helps centralize all Open
          Source events on one calendar to make it easier for you to attend and
          contribute. If you'd like to submit your event to be added, please
          invite this email to your event and{' '}
          <a
            href="https://opensource.ieee.org/marketing-advisory-group/events/-/issues/new"
            target="_blank" rel="noreferrer"
          >
            fill out a brief form
          </a>{' '}
          Not a member with IEEE SA OPEN?{' '}
          <a
            href="https://www.ieee.org/profile/public/createwebaccount/showCreateAccount.html?url=https%3A%2F%2Fwww.ieee.org%2F%2F"
            target="_blank" rel="noreferrer"
          >
            Create your account
          </a>{' '}
          to submit your event today!{' '}
        </p>
        <p>
          All times on calendar below are shown in the the user's computer time
          zone.
        </p>
            <div className={'center'}> 
          <Iframe
            src={
              'https://calendar.google.com/calendar/embed?src=c_iuboq60p6a8rekskm9v6299as4%40group.calendar.google.com&ctz=local'
            }
            width={'800'}
            height={'600'}
          />
        </div>
      </Section>
      <Section type={'light'}>
        <div className={'center'}>
        <a className={'anchor'} id={'submit'} />
          <h4>Submit an Event</h4>
          <p>
            If you have an event you would like to add to the calendar, please
            submit it using the corresponding template on the event submission
            form.
          </p>
          <Button
            link={
              'https://opensource.ieee.org/marketing-advisory-group/events/-/issues/new'
            }
          >
            Submit Event
          </Button>
        </div>
      </Section>
      <Section type={'dark'}>
      <div className={'center'}>
      <a className={'anchor'} id={'prior'} />
      <h4>Prior Events</h4>
        </div>
        <p>
          <ul>
            <li>
              <a href="/workshop1" target="_blank">
                Advisory Group Workshop The Spirit Of Mentorship, December 8th,
                2020
              </a>
            </li>
            <li>
              <a href="/workshop2" target="_blank">
                Marketing Advisory Group Workshop MAGic In The Making: Creating
                Open Source Marketing Resources May 4th, 2021
              </a>
            </li>
            <li>
              <a href="/workshop3" target="_blank">
                TAG You're IT: Technical Infrastructure In Open Source
                Communities June 16th, 2021
              </a>
              </li>
          </ul>
        </p>
      <ArticleSlider
        articles={[
          {
            image: SilonaArticle,
            description:
              'Silona Bonewald Joins IEEE SA OPEN as First Executive Director',
            link:
              'https://standards.ieee.org/news/2020/silona-bonewald-ieeesa-open-first-executive-director.html',
          },
          {
            image: TeamArticle,
            description:
              'Why your open source project needs more than just coders',
            link:
              'https://opensource.com/article/20/9/open-source-role-diversity',
          },
          {
            image: MediumArticle,
            description: 'CHAOSS Podcast: Role Diversity with Silona Bonewald',
            link: 'https://podcast.chaoss.community/16',
          },
          {
            image: FlossArticle,
            description:
              'FLOSS Weekly Podcast: Embracing Open Source & Open Standards',
            link:
              'https://twit.tv/shows/floss-weekly/episodes/584?autostart=false',
          },
        ]}
      />
    </Section>
      <Section type={'light'}>
        <div className={'center'}>
        <a className={'anchor'} id={'training'} />
          <h4>Training Events</h4>
          <p>
            If your company or organization is interested in training events for
            integrating the IEEE SA OPEN platform with your team. Please contact
            us directly. We we can design a virtual training event tailored to
            meet your needs.
          </p>
          <Button link={'mailto:opensource@ieee.org'}>Contact Us</Button>
        </div>
      </Section>
    </Layout>
  );
};

export default Events;


