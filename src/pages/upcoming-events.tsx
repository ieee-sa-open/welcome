import React from 'react';
import { Helmet } from 'react-helmet';
import { PageProps } from 'gatsby';

import Layout from '../components/Layout/Layout';
import Hero from '../components/Hero/Hero';
import Section from '../components/Section/Section';

const UpcomingEvents: React.FC<PageProps> = () => {
  return (
    <Layout>
      <Helmet>
        <title>Upcoming Events - IEEE SA OPEN</title>
      </Helmet>
      <Hero heroIntro={'IEEE SA Open'} heroPrimary={'Upcoming Events'} />
      <Section type={'light'}>
        <div className={'center'}>
          <h6>SA OPEN @ All Things Open Conference 2021</h6>
          <p>
          <strong>October 17-19, 2021.</strong>
          </p>
          <p>
            <a href="https://saopen.ieee.org/ato2021/">
              Visit our virtual booth and check out talks by our community volunteers and contributors.
            </a>
          </p>
        </div>
      </Section>
    </Layout>
  );
};

export default UpcomingEvents;
