import React from 'react';
import { Helmet } from 'react-helmet';

import Layout from '../components/Layout/Layout';
import Hero from '../components/Hero/Hero';
import Section from '../components/Section/Section';

import BBB_1 from '../images/bbb/BBB-mute-user.png';
import BBB_2 from '../images/bbb/BBB-mute-all-users.png';
import BBB_3 from '../images/bbb/BBB-rejoin-audio.png';
import BBB_4 from '../images/bbb/BBB-join-with-mic.png';
import BBB_5 from '../images/bbb/BBB-echo-test.png';
import BBB_6 from '../images/bbb/BBB-share-your-screen.png';
import BBB_7 from '../images/bbb/BBB-share-your-screen-step2.png';
import BBB_8 from '../images/bbb/BBB-stop-sharing-screen.png';
import BBB_9 from '../images/bbb/BBB-presenter-give.png';
import BBB_10 from '../images/bbb/BBB-presenter-icon.png';
import BBB_11 from '../images/bbb/BBB-presenter-take-back.png';
import BBB_12 from '../images/bbb/BBB-poll-start.png';
import BBB_13 from '../images/bbb/BBB-poll-setup.png';
import BBB_14 from '../images/bbb/BBB-whiteboard-start.png';
import BBB_15 from '../images/bbb/BBB-whiteboard-use.png';
import BBB_16 from '../images/bbb/BBB-remove-user.png';





const FacilitatorGuide: React.FC<PageProps> = () => (
  <Layout>
    <Helmet>
      <title>Getting Started - IEEE SA OPEN</title>
    </Helmet>
    <Hero heroIntro={'IEEE SA Open'} heroPrimary={'Guides'} />
    <Section type={'light'}>
      <h6> Leading Workshop Breakout Rooms on BBB<br />
Facilitator Guide</h6>
     

<p><strong>Purpose:</strong> Breakout rooms are a time for event attendees to collaborate. It is a working session. Sessions are led by a facilitator, but do not follow a structured agenda.</p>

<p>Welcome attendees into the room, remind people to join with the audio microphone icon, not the headset (listen only) icon. If a person has an icon with a headset next to their name, they have joined via listen only.</p>

<p>Ask users to close the main meeting hall BBB tab in their browser while they are in the breakout room. As the facilitator, please make sure you also do this. Audio from any other open BBB meeting rooms in other tabs will come through the users computer audio and it will be heard in the breakout room.</p>

<p>Breakout rooms may be recorded.  Prior to the beginning of recording, the facilitator should notify the attendees that the session will be recorded. If people do not want to be visible in the recording, they should turn off their video. The recording will only record user webcam video and audio input during the session. Recordings will not capture presentations, shared screens, public or private chats, or shared notes.</p>

<p>At the end of the breakout session, post the link to the main hall in the public chat so attendees can navigate back to the main room. Remind people to close the current meeting room window tab in their browser.</p>

<p>Copy the notes from the shared notes section and send them to the appropriate person for posting to the Gitlab group.</p>

<h6>Actions: </h6>

<p><strong>Mute a user</strong> </p>
<p>If you need to mute a user because of background noise from their mic, click on their name in the participant list. Select MUTE USER in the list</p>
<div className={'center'}>
<p><img alt={'mute user'} src={BBB_1} /></p>
</div>


<p><strong>Mute all users</strong> </p>
<p>If you need to mute all users or all users except the presenter, click the settings icon above the user list and select either MUTE ALL USERS or MUTE ALL USERS EXCEPT PRESENTER 
</p>
<div className={'center'}>
<p><img alt={'mute all users'} src={BBB_2} /></p>
</div>

<p><strong>Tell users how to switch from listen only to audio</strong></p>

<p>If a user is in listen only, instruct them to:</p>
<p>
<ul>
<li>Leave the audio conference by clicking on the blue headphone button on the bottom of their screen.</li>
<li>Join the audio conference again by clicking the same button, that now has a picture of a white phone with a line through it and says "Join Audio" when the mouse hovers over it.</li>
</ul>
</p>
<div className={'center'}>
<p><img alt={'rejoin audio'} src={BBB_3} /></p>
</div>

<p>
<ul>
<li>When prompted on how they would like to join audio, select the "microphone" icon</li>
</ul>
</p>
<div className={'center'}>
<p><img alt={'join with mic'} src={BBB_4} /></p>
</div>

<p>
<ul>
<li>Complete the echo test when prompted and they will be added back into the audio conferencing in the room</li>
</ul>
</p>
<div className={'center'}>
<p><img alt={'echo test'} src={BBB_5} /></p>
</div>


<p><strong>Share your screen</strong></p>
<p>To share your screen with attendees: </p>

<p>
<ul>
<li>Click on the round button at the bottom of the screen on the right side of the button row, with the white picture of the monitor with a line through icon. </li>
</ul>
</p>
<div className={'center'}>
<p><img alt={'share your screen'} src={BBB_6} /></p>
</div>


<p>
<ul>
<li>When prompted by the pop up window, select the picture of the screen you want to share (there may be more than one if you are using multiple monitors) click SHARE.
</li>
</ul>
</p>
<div className={'center'}>
<p><img alt={'share your screen step 2'} src={BBB_7} /></p>
</div>

<p>
<ul>
<li>To stop sharing your screen, click STOP SHARING on the bar at the bottom of your shared screen</li>
</ul>
</p>

<div className={'center'}>
<p><img alt={'stop sharing screen'} src={BBB_8} /></p>
</div>



<p><strong>If a participant wants to share their screen </strong></p>
<p>
<ul>
<li>Give them presenter mode by clicking on the user's name on the attendee list and select MAKE PRESENTER.</li>
</ul>
</p>

<div className={'center'}>
<p><img alt={'presenter given screen share'} src={BBB_9} /></p>
</div>

<p>
<ul>
<li>The user with presenter status will have the presenter icon on their username icon</li>
</ul>
</p>

<div className={'center'}>
<p><img alt={'presenter icon'} src={BBB_10} /></p>
</div>

<p>
<ul>
<li>After the user is done presenting, click on your name in the user list and select TAKE PRESENTER
</li>
</ul>
</p>
<div className={'center'}>
<p><img alt={'presenter take back'} src={BBB_11} /></p>
</div>




<p><strong>Create a Poll</strong> </p>
<p>
<ul>
<li>When you have presenter status, you can create a poll by clicking on the plus sign button on the left side of the bottom bar. Select "Start a Poll". 
</li>
</ul>
</p>
<div className={'center'}>
<p><img alt={'start a poll'} src={BBB_12} /></p>
</div>

<p>
<ul>
<li>Use the pane that pops up on the left side to create the poll questions and response options. 
</li>
</ul>
</p>

<div className={'center'}>
<p><img alt={'poll setup'} src={BBB_13} /></p>
</div>



<p>After users vote, it will publish results on the current presentation slide. Results are displayed as totals. The presenter can see individual responses from users as they come in, but the final displayed poll is an anonymous total and does not identify how individual users voted.</p>   

<p>
<strong>Start an interactive multi-user whiteboard</strong> <br />
In presenter mode, you can write on the whiteboard as part of a presentation or make it a shared whiteboard so all users can collaborate and write/type on it.
Steps:  </p>

<p>
<ul>
<li>Start the whiteboard by progressing the presentation slides forward until you come to a blank slide</li>
</ul>
</p>

<div className={'center'}>
<p><img alt={'whiteboard start'} src={BBB_14} /></p>
</div>


<p>
<ul>
<li>Click on the picture of the white board icon at the bottom of the editing toolbar on the right side of the slide, it will say "turn on multi-user whiteboard" when you hover your mouse over it. The icon will show two whiteboards after you click on it, indicating the whiteboard is interactive and all users can write on it.
</li>
<li>Click on the hand at the top of the toolbar and select an editing tool to use
</li>
</ul>
</p>

<div className={'center'}>
<p><img alt={'whiteboard use'} src={BBB_15} /></p>
</div>


<p>To leave the white board so only the presenter can draw on it, turn off the multi-user whiteboard icon at the bottom of the editing toolbar and select the editing tools you want to use.</p>

<p>
 <strong>Removing people from the room - </strong> 
</p>
<p>
<ul>
<li>If you need to remove someone from the room or they need assistance leaving the room, click on the user’s name on the attendee list and select REMOVE USER
</li>
</ul>
 </p>

 <div className={'center'}>
<p><img alt={'remove user'} src={BBB_16} /></p>
</div>
</Section>
  </Layout>
);

export default FacilitatorGuide;
