import PropTypes from 'prop-types';
import React from 'react';
import Logo from '../Logo/Logo';
import styles from './Footer.module.css';
import { Link } from 'gatsby';

const Footer = () => (
  <div className={styles.footerContainer}>
    <div className={styles.footerTop}>
      <div className={styles.logoContainer}>
        <Logo />
      </div>
      <div className={styles.aboutContainer}>
        <div className={styles.columnHeader}>About IEEE</div>
        <div className={styles.aboutHeader}>Ready to get started?</div>
        <div className={styles.aboutDetails}>
          IEEE is the world's largest technical professional organization
          dedicated to advancing technology for the benefit of humanity.
        </div>
      </div>
      <div className={styles.dividerContainer} />
      <div className={styles.linkWrapper}>
        <div className={styles.linkContainer}>
          <div className={styles.linkColumn}>
            {/*<div className={styles.columnHeader}>Road Maps</div>*/}
            {/*<div className={styles.link}>*/}
            {/*  <Link to={'/home'}>Link 1</Link>*/}
            {/*</div>*/}
            {/*<div className={styles.link}>*/}
            {/*  <Link to={'/home'}>Link 2</Link>*/}
            {/*</div>*/}
            {/*<div className={styles.link}>*/}
            {/*  <Link to={'/home'}>Link 3</Link>*/}
            {/*</div>*/}
            {/*<div className={styles.link}>*/}
            {/*  <Link to={'/home'}>Link 4</Link>*/}
            {/*</div>*/}
            {/*<div className={styles.link}>*/}
            {/*  <Link to={'/home'}>Link 5</Link>*/}
            {/*</div>*/}
          </div>
          <div className={styles.linkColumn}>
            {/*<div className={styles.columnHeader}>Other</div>*/}
            {/*<div className={styles.link}>*/}
            {/*  <Link to={'/home'}>Link 1</Link>*/}
            {/*</div>*/}
            {/*<div className={styles.link}>*/}
            {/*  <Link to={'/home'}>Link 2</Link>*/}
            {/*</div>*/}
            {/*<div className={styles.link}>*/}
            {/*  <Link to={'/home'}>Link 3</Link>*/}
            {/*</div>*/}
            {/*<div className={styles.link}>*/}
            {/*  <Link to={'/home'}>Link 4</Link>*/}
            {/*</div>*/}
            {/*<div className={styles.link}>*/}
            {/*  <Link to={'/home'}>Link 5</Link>*/}
            {/*</div>*/}
          </div>
        </div>
      </div>
    </div>
    <div className={styles.footerBottom}>
      <div className={styles.bottomLinksContainer}>
        <div className={styles.bottomLinks}>
          <Link to={'/'}>Home</Link>
        </div>
        <div className={styles.bottomLinks}>|</div>
        <div className={styles.bottomLinks}>
          <a href={'https://saopen.ieee.org/sitemap/'}>Sitemap</a>
        </div>
        <div className={styles.bottomLinks}>|</div>
        <div className={styles.bottomLinks}>
          <a href={'mailto:opensource@ieee.org'}>Contact & Support</a>
        </div>
        <div className={styles.bottomLinks}>|</div>
        <div className={styles.bottomLinks}>
          <Link to={'/legal'}>Legal</Link>
        </div>
        <div className={styles.bottomLinks}>|</div>
        <div className={styles.bottomLinks}>
          <a href={'https://www.ieee.org/accessibility_statement.html'}>
            Accessibility
          </a>
        </div>
        <div className={styles.bottomLinks}>|</div>
        <div className={styles.bottomLinks}>
          <a href={'https://www.ieee.org/nondiscrimination'}>
            Nondiscrimination Policy
          </a>
        </div>
        <div className={styles.bottomLinks}>|</div>
        <div className={styles.bottomLinks}>
          <a href={'https://www.ieee.org/privacy'}>IEEE Privacy Policy</a>
        </div>
      </div>
      <div className={styles.copyright}>
        © 2022 IEEE – All rights reserved. Use of this website signifies your
        agreement to the{' '}
        <a href={'https://www.ieee.org/about/help/site-terms-conditions.html'}>
          IEEE Terms and Conditions
        </a>
        .
        <br />A not-for-profit organization, IEEE is the world's largest
        technical professional organization dedicated to advancing technology
        for the benefit of humanity.
      </div>
    </div>
  </div>
);

Footer.propTypes = {
  siteTitle: PropTypes.string,
};

Footer.defaultProps = {
  siteTitle: ``,
};

export default Footer;
