import React from 'react';
import { Helmet } from 'react-helmet';
import { PageProps } from 'gatsby';

import Layout from '../components/Layout/Layout';
import Hero from '../components/Hero/Hero';
import Section from '../components/Section/Section';
import MultiColumn from '../components/MultiColumn/MultiColumn';
import Column from '../components/Column/Column';
import Example from '../components/Example/Example';

import Flyer1 from '../images/workshops/social_hour_flyer_sm.png';




const SocialHour: React.FC<PageProps> = () => {
  return (
    <Layout>
      <Helmet>
        <title>Upcoming Events - IEEE SA OPEN</title>
      </Helmet>
      <Hero heroIntro={'IEEE SA Open'} heroPrimary={'Monthly Social Hours'} />
    <Section type={'light'}> 
    <h6>Monthly Community Social Hours</h6>
   <p>
    <strong>When:</strong> Last Thursday of every month <br />
    <strong>Time:</strong> 6 PM - 7 PM USA Eastern time <br />
    <strong>Where:</strong> Community BigBlueButton  Room - <a href="http://community.leadingbit.com" target="_blank">http://community.leadingbit.com</a>
    </p>
    </Section>

    <Section type={'dark'}>
    <h6>2021 Social Hours</h6>
      <MultiColumn>
        <Column>   
        <Example>
          <ul>
          <li>April 29, 2021 </li>
          <li>May 27, 2021 </li>
          <li>June 24, 2021 </li>
          <li>July 29, 2021 </li>
          <li>September 30, 2021 </li>
          <li>October 28, 2021 </li>
          <li>November 18, 2021  (moved up a week due to Thanksgiving)</li>
        </ul>
        <p>Check out what the IEEE SA OPEN community is up to by visiting the Community Calendar.  
        You can find the Community Calendar on the IEEE SA OPEN website <a href="/community">Community Page</a>.</p>
        </Example>
        </Column>
        <Column>
        <img alt={'Monthly Social Hour'} src={Flyer1} />
        </Column>
      </MultiColumn>
    </Section>
   </Layout>
  );
};

export default SocialHour;
