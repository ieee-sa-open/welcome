import React from 'react';
import { Helmet } from 'react-helmet';

import Layout from '../components/Layout/Layout';
import Hero from '../components/Hero/Hero';
import Section from '../components/Section/Section';
import Example from '../components/Example/Example';
import TableOfContents from '../components/TableOfContents/TableOfContents';

import i326_1 from '../images/getting-started/326_1.png';
import i326_2 from '../images/getting-started/326_2.png';
import i326_3 from '../images/getting-started/326_3.png';
import i330_1 from '../images/getting-started/330_1.png';
import i330_2 from '../images/getting-started/330_2.png';
import i330_3 from '../images/getting-started/330_3.png';
import i330_4 from '../images/getting-started/330_4.png';
import i330_5 from '../images/getting-started/330_5.png';
import CLA_1 from '../images/getting-started/CLA-1.png';
import CLA_2 from '../images/getting-started/CLA-2.png';

const Participation: React.FC<PageProps> = () => (
  <Layout>
    <Helmet>
      <title>Getting Started - IEEE SA OPEN</title>
    </Helmet>
    <Hero heroIntro={'IEEE SA Open'} heroPrimary={'Participation'} />
    <TableOfContents
      items={[
        {
          text: 'Table of Contents',
          link: '/getting-started',
        },
        {
          text: 'Creating Your IEEE SA OPEN Single Sign On',
          link: '#sso',
        },
        {
          text: 'How to A Sign Contributor License Agreement',
          link: '#signcla',
        },
        {
          text: 'Notifications in GitLab',
          link: '#notifications',
        },
      ]}
    />
    <Section type={'light'}>
      <a className={'anchor'} id={'sso'} />
      <h1>Creating your IEEE SA OPEN Single Sign On</h1>

      <p>To sign in for the first time:</p>

      <ul>
        <li>
          If you are an IEEE member, log in to your IEEE account and then visit{' '}
          <a href={'https://opensource.ieee.org'}>
            https://opensource.ieee.org
          </a>{' '}
          and use your existing IEEE credentials to log in.
        </li>
        <li>
          If you are not an IEEE member, create a free account by visiting the{' '}
          <a href="https://www.ieee.org/profile/public/createwebaccount/showCreateAccount.html?url=https%3A%2F%2Fwww.ieee.org%2F%2F" target='_blank'>Create an IEEE Account page</a>. 
          Fill in the appropriate information (making sure to preserve these
          credentials for later use). When you have succeeded in creating an
          IEEE account, you will be returned to the main IEEE page. 
          </li>
          
          <li>
          Return to the IEEE main page indicates that your account has been created. You will need to return to{' '} 
          <a href="https://opensource.ieee.org"target='_blank'>https://opensource.ieee.org</a> and use your new login. You will now be able to access the IEEE SA OPEN site shown below.
        </li>
      </ul>
      <Example>
        <img alt={'create ieee sa open single sign on 1'} src={i326_1} />

        <p>
          Click in the inbox (highlighted in green in the above image) and you
          will be sent to the sign-in page (shown below). Use your IEEE login
          information to sign in.
        </p>

        <img alt={'create ieee sa open single sign on 2'} src={i326_2} />

        <p>
          The first time you log in, you will be asked to agree to the IEEE SA
          OPEN Terms of Use. Please scroll to the bottom of this page and click
          the green Accept terms button.
        </p>

        <img alt={'create ieee sa open single sign on 3'} src={i326_3} />

        <p>
          After you log in, you will be taken to the main page of IEEE SA OPEN.
          It runs on the popular GitLab CE platform. These same credentials will
          also allow you to log in the IEEE Mattermost chat server.
        </p>
      </Example>
      <a className={'anchor'} id={'signcla'} />
      <h3> How to Sign a Contributor License Agreement</h3>
      <Example>
        <p>
          When you sign on to the IEEE SA OPEN platform, you are joining a
          community of contributors. To make sure that your contributions to our
          community are licensed appropriately, please sign a Contributor
          License Agreement. Our community shares our work through the  <a href={'https://opensource.ieee.org/community/cla/apache'}> Apache
          2.0 license </a>
          </p>
        <p>
          If you need to sign a different license for a specific project, choose
          the <a href={'https://opensource.ieee.org/community/cla'}>Contributor License Agreement</a>
          (CLA) that corresponds with the license for the project you are
          working on. (As of July of 2021, IEEE SA OPEN supports the following licenses: 
          BSD 3-Clause, Apache 2.0, or CERN OHL 2.0.)
        </p>

        <img alt={'CLA'} src={CLA_1} />

        <p>
          There are three versions of each Contributing License Agreement.
          Please choose the one that is appropriate to your situation. If you
          are signing on behalf of a company, please complete the entity CLA. If
          you are contributing as an individual, please choose the individual
          CLA. If you are adding something to the public domain as a
          representative of a government, please complete the option with the
          public domain declaration.
        </p>

        <img alt={'CLA'} src={CLA_2} />

        <p>
          The first page of the CLA document provides instructions for how to
          choose a CLA document, how to fill it out, and how to submit it.
          Please email the signed CLA to{' '}
          <a href="mailto:oscontrib@ieee.org?subject=CLA%20%20from%20IEEE%20SA%20Website%20">
            oscontrib@ieee.org
          </a>
          .
        </p>

        <p>
          Once the CLA is signed and the submission process is completed, a CLA
          number will be provided to the representative or individual that
          submitted it.
        </p>
      </Example>
    </Section>
    <Section type={'dark'}>
      <a className={'anchor'} id={'notifications'} />
      <h1>Notifications In Gitlab</h1>

      <h4>Setting up Notifications</h4>
      <p>
        Notifications allow you to stay informed about what's happening in
        GitLab. With notifications enabled, you can receive updates about
        activity in projects and groups. Notifications are sent via email.
      </p>

      <h4>Global notification settings</h4>
      <p>
        Your Global notification settings are the default settings unless you
        select different values for a project or a group.
      </p>

      <Example>
        <img alt={'gitlab notifications 1'} src={i330_1} />
        <p>
          <b>Notification email</b>
          <br />
          This is the email address your notifications will be sent to.
        </p>
        <p>
          <b>Global notification level</b>
          <br />
          This is the default notification level which applies to all your
          notifications. Initially, this is set to the [Participate] option.
        </p>
      </Example>

      <h4>Notification scope</h4>
      <p>
        You can tune the scope of your notifications by selecting different
        notification levels for each project and group.
      </p>
      <p>
        Notification scope is applied in order of precedence (highest to
        lowest):
      </p>

      <Example>
        <p>
          <b>Project</b>
          <br />
          For each project, you can select a notification level. Your project
          setting overrides the group and global settings.
        </p>
        <p>
          <b>Group</b>
          <br />
          For each group, you can select a notification level. Your group
          setting overrides your default setting.
        </p>
        <p>
          <b>Global (default)</b>
          <br />
          Your global, or <i>default</i>, notification level applies if you have
          not selected a notification level for the project or group in which
          the activity occurred.
        </p>
      </Example>

      <h4>Project and Group Notifications</h4>
      <p>
        As noted above, you can select a notification level and email address
        for each project or group.
      </p>
      <p>
        To select a notification level for a project or group, use either of
        these methods:
      </p>

      <ul>
        <li>Click on your profile picture and select Settings.</li>
        <li>
          Click Notifications (highlighted green in the picture below) in the
          left sidebar.
        </li>
        <li>
          Locate a group or subgroup in the Groups section of the Notifications
          page to change its setting. A project may be found in the Projects
          section below the Groups section. If you would like to specify
          notification levels for individual projects in one group, you will
          need to visit that project's page and change the level there.
        </li>
        <li>Select the desired notification level.</li>
        <li>
          For groups only, you can also select an email address from the
          dropdown menu beside the notification level . This could be useful if
          you would like to separate email about different groups. This option
          is not available at the project level. Email addresses can be added to
          your account by clicking on the Emails option (highlighted pink in the
          picture below) and following the instructions there.
        </li>
      </ul>

      <Example>
        <img alt={'gitlab notifications 2'} src={i330_2} />
        <p>
          <b>OR</b>
        </p>
      </Example>

      <ul>
        <li>
          Alternately, you may change your notification settings by navigating
          to the group's or project's page.
        </li>
        <li>
          Click the notification dropdown, marked with a bell icon (highlighted
          in green below).
        </li>
        <li>Select the desired notification level.</li>
      </ul>

      <Example>
        <img alt={'gitlab notifications 3'} src={i330_3} />
      </Example>

      <h4>Notification Level Descriptions</h4>
      <p>
        For each group (or project) you can select one of the following levels:
      </p>

      <Example>
        <img alt={'gitlab notifications 4'} src={i330_4} />
      </Example>

      <h4>Standard Notification Events</h4>
      <p>All users will be notified of the following events:</p>

      <Example>
        <img alt={'gitlab notifications 4'} src={i330_5} />
      </Example>
    </Section>

    <Section type={'light'}>
    <div className={'center'}>
      <p><strong>Next section -></strong> <a href="/creating-in-gitlab">Creating in GitLab</a></p>
      </div>
    </Section>
  </Layout>
);

export default Participation;
