import React from 'react';
import { Helmet } from 'react-helmet';
import { PageProps } from 'gatsby';

import Layout from '../components/Layout/Layout';
import Hero from '../components/Hero/Hero';
import Section from '../components/Section/Section';

const BBBFaq: React.FC<PageProps> = () => (
  <Layout>
    <Helmet>
      <title>Community - IEEE SA OPEN</title>
    </Helmet>
    <Hero heroIntro={'IEEE SA OPEN'} heroPrimary={'Guides'} />
    <Section type={'light'}>
      <h6>BigBlueButton FAQ</h6>

      <p>
        <strong>
          Why am I having difficulty connecting after I try to join the meeting?
        </strong>
      </p>

      <p>
        VPNs often prevent BigBlueButton from connecting properly. Try turning
        off the VPN and reconnecting.
      </p>

      <p>
        BigBlueButton works best in Chrome and Firefox browsers. Try using one
        of these browsers and make sure you approve permissions for it to access
        your microphone and webcam when your browser asks for permissions.
      </p>

      <p>
        <strong>Why is my audio not connecting?</strong>
      </p>

      <p>
        If you selected join with MICROPHONE, then make sure you gave permission
        for BigBlueButton to access your microphone in your browser. Your
        browser will give you a prompt window, asking if you want to grant
        permissions.
      </p>

      <p>
        <strong>Why did my audio stop working?</strong>
      </p>
      <p>
        You switched to a new speaker/microphone during the meeting. You will
        need to leave and rejoin the audio conference by clicking on the picture
        of the phone at the bottom of your screen. You will need to do the sound
        test during reentry and select your new audio device. If it does not
        auto connect during the sound test, select the red button to say you
        don't hear it. It will then let you select your new device.
      </p>
      <p>
        <strong>Why won’t my video connect?</strong>
      </p>

      <p>
        Make sure you grant BigBlueButton access to your webcam when your
        browser prompts you to give permission. If that does not work, try
        leaving the meeting and reconnecting.
      </p>

      <p>
        <strong>Why is there no microphone button for me to speak?</strong>
      </p>

      <p>
        If there is a picture of headphones on your user icon in the User list
        on the left side, you are in listen only mode. Click on the button with
        the headphones icon at the bottom of the screen. Click the same button
        again when it has a picture of a phone with a line through it. You will
        then be prompted to join the audio call. Select the join with MICROPHONE
        button.
      </p>

      <p>
        <strong>How do I improve unclear audio?</strong>
      </p>

      <p>
        If there is lag in video or audio, you may be having internet connection
        or speed issues. You can put BigBlueButton into data saving mode to help
        with clarity. Data saving mode will allow you to turn off webcams. Click
        on the three dot hamburger menu at the top right of your screen and go
        to settings. Select DATA SAVINGS and turn off "enable webcams".
      </p>
    </Section>
  </Layout>
);

export default BBBFaq;

