import React from 'react';
import { Helmet } from 'react-helmet';

import Layout from '../components/Layout/Layout';
import Hero from '../components/Hero/Hero';
import Section from '../components/Section/Section';
import TableOfContents from '../components/TableOfContents/TableOfContents';

import BBB_1 from '../images/bbb/BBB-mute-user.png';
import BBB_2 from '../images/bbb/BBB-mute-all-users.png';
import BBB_3 from '../images/bbb/BBB-rejoin-audio.png';
import BBB_4 from '../images/bbb/BBB-join-with-mic.png';
import BBB_5 from '../images/bbb/BBB-echo-test.png';
import BBB_6 from '../images/bbb/BBB-share-your-screen.png';
import BBB_7 from '../images/bbb/BBB-share-your-screen-step2.png';
import BBB_8 from '../images/bbb/BBB-stop-sharing-screen.png';
import BBB_9 from '../images/bbb/BBB-presenter-give.png';
import BBB_10 from '../images/bbb/BBB-presenter-icon.png';
import BBB_11 from '../images/bbb/BBB-presenter-take-back.png';
import BBB_12 from '../images/bbb/BBB-poll-start.png';
import BBB_13 from '../images/bbb/BBB-poll-setup.png';
import BBB_14 from '../images/bbb/BBB-whiteboard-start.png';
import BBB_15 from '../images/bbb/BBB-whiteboard-use.png';
import BBB_16 from '../images/bbb/BBB-remove-user.png';
import BBB_17 from '../images/bbb/BBB-share-webcam.png';
import BBB_18 from '../images/bbb/BBB-share-webcam-permissions.png';
import BBB_19 from '../images/bbb/BBB-private-chat-start.png';
import BBB_20 from '../images/bbb/BBB-private-chat-pt2.png';
import BBB_21 from '../images/bbb/BBB-shared-notes.png';


const BigBlueButton: React.FC<PageProps> = () => (
  <Layout>
    <Helmet>
      <title>Getting Started - IEEE SA OPEN</title>
    </Helmet>
    <Hero heroIntro={'IEEE SA Open'} heroPrimary={'Big Blue Button'} />
    <TableOfContents
      items={[
        {
          text: 'Table of Contents',
          link: '/getting-started',
        },
        {
          text: 'Participant Prep',
          link: '#prep',
        },
        {
          text: 'BBB FAQ',
          link: '#faq',
        },
        {
          text: 'Facilitator Guide',
          link: '#facilitator',
        },     
      ]}
    />
  <Section type={'light'}>
    <a className={'anchor'} id={'prep'} />
    <a className={'anchor'} id={'#prep'} />
    <h1>Participant Prep</h1>
    <h6>Preparing for Video Conferencing on BigBlueButton</h6>

    <h6>Testing Your Equipment Before the Event</h6>

<p>Go to the BigBlueButton test site to test your browser and equipment: <a href="https://test.bigbluebutton.org/" target="_blank">https://test.bigbluebutton.org/</a>
</p>

<p>
<ul>
<li>Check to make sure headset and mic works</li>
<li>Check your internet connection</li>
</ul>
</p>

<p>The BigBlueButton room runs in the internet browser window and works best with Google Chrome and Firefox </p>

<h6>Joining the Meeting</h6>

<p>
<ul>
<li>Open the event link in Google Chrome or Mozilla Firefox</li>
<li>Put your name in the box and click join</li>
<li>When prompted “How would you like to join the audio?” select JOIN WITH MICROPHONE</li>
</ul>
</p>

<div className={'center'}>
<p><img alt={'join with mic'} src={BBB_4} /></p>
</div>

<p>
<ul>
<li>Do the echo test to ensure your microphone and speakers are configured correctly</li>
</ul>
</p>

<div className={'center'}>
<p><img alt={'echo test'} src={BBB_5} /></p>
</div>


<p>
<ul>
<li>Mute your audio when you join the video conference room using the mic button at the bottom of the screen. You can also click on your user name and select mute</li>
<li>Put your name and affiliation in the shared notes on the left side bar</li>
</ul>
</p>

<div className={'center'}>
<p><img alt={'shared notes'} src={BBB_21} /></p>
</div>

<h6>Participating in the Meeting</h6>

<p>Unmute to speak</p>

<p>
<ul>
<li>If you want to speak click on the mic button at the bottom of the screen or click your user name and select unmute in the User list on the left side of the screen</li>
</ul>
</p>

<p>Turn on webcam</p>

<p>
<ul>
<li>If you want to join the meeting with video, select the camera icon at the bottom of the screen</li>
</ul>
</p>

<div className={'center'}>
<p><img alt={'share webcam'} src={BBB_17} /></p>
</div>

<p>
<ul>
<li>Give your browser permission to access your webcam when the permission request pops up</li>
</ul>
</p>

<div className={'center'}>
<p><img alt={'webcam permissions'} src={BBB_18} /></p>
</div>

<p>Start a private chat</p>

<p>
<ul>
<li>Click on the users name in the user list and select START A PRIVATE CHAT</li>
</ul>
</p>

<div className={'center'}>
<p><img alt={'start private chat'} src={BBB_19} /></p>
</div>

<p>
<ul>
<li>The private chat can be accessed under the public chat tab on the left side</li>
</ul>
</p>

<div className={'center'}>
<p><img alt={'start private chat part 2'} src={BBB_20} /></p>
</div>

<p>Use public chat</p>

<p>
<ul>
<li>The public chat is the first thing visible in the left pane when you log onto the BigBlueButton meeting</li>
<li>Any public conversations that occurred before you joined the meeting will be above the welcome box in the public chat</li>
<li>The public chat is not recorded in the meeting recording. If there are notes or information you want recorded for later reference, put it in the shared notes if they are being used for the meeting. Shared notes can be copy and pasted into another location at the end of the meeting</li>
</ul>
</p>

<p>Collaborate in shared notes</p>

<p>
<ul>
<li>Any user in the meeting can write on the shared notes pad</li>
<li>Open shared notes and type in the window</li> 
</ul>
</p>

<div className={'center'}>
<p><img alt={'shared notes'} src={BBB_21} /></p>
</div>

<p>
<ul>
<li>The user name does not appear next to the text that each person types. If you want your name attached to what you contribute in the notes, you will need to put your name in the text</li>
<li>Shared notes do not get captured in the recording. If you want a copy of the notes at the end of the meeting, you will need to copy and paste them into another location</li>
</ul>
</p>

<h6>Conferencing etiquette</h6>

<p>
<ul>
<li>Choose a quiet location without distractions</li>
<li>Use a headset to eliminate background noise and feedback that your microphone picks up</li>
<li>If you will have your webcam turned on, keep in mind that BigBlueButton does not have virtual backgrounds. Sit in a place that has a background you are okay with other people seeing</li>
<li>Stay muted when you are not speaking to prevent background noise and feedback</li>
</ul>
</p>
  </Section>
  <Section type={'dark'}>
  <a className={'anchor'} id={'faq'} />
  <a className={'anchor'} id={'#faq'} />
    <h1>Big Blue Button FAQ</h1>

    <p><strong>Why am I having difficulty connecting after I try to join the meeting?</strong></p>

    <p>VPNs often prevent BigBlueButton from connecting properly. Try turning off the VPN and reconnecting.</p>

    <p>BigBlueButton works best in Chrome and Firefox browsers. Try using one of these browsers and make sure you approve permissions for it to access your microphone and webcam when your browser asks for permissions.</p>

    <p><strong>Why is my audio not connecting?</strong></p>

    <p>If you selected join with MICROPHONE, then make sure you gave permission for BigBlueButton to access your microphone in your browser. Your browser will give you a prompt window, asking if you want to grant permissions.</p>

    <p><strong>Why won’t my video connect?</strong></p>

    <p>Make sure you grant BigBlueButton access to your webcam when your browser prompts you to give permission. If that does not work, try leaving the meeting and reconnecting.</p>

    <p><strong>Why is there no microphone button for me to speak?</strong></p>

    <p>If there is a picture of headphones on your user icon in the User list on the left side, you are in listen only mode. Click on the button with the headphones icon at the bottom of the screen. Click the same button again when it has a picture of a phone with a line through it. You will then be prompted to join the audio call. Select the join with MICROPHONE button.</p>

    <p><strong>How do I improve unclear audio?</strong></p>

    <p>If there is lag in video or audio, you may be having internet connection or speed issues. You can put BigBlueButton into data saving mode to help with clarity. Data saving mode will allow you to turn off webcams. Click on the three dot hamburger menu at the top right of your screen and go to settings. Select DATA SAVINGS and turn off "enable webcams".</p>
    </Section>
    <Section type={'light'}>
    <a className={'anchor'} id={'facilitator'} />
      <a className={'anchor'} id={'#facilitator'} />
      <h1>Facilitator Guide</h1>
      <h6> Leading Conferencing Rooms on BBB</h6>
     

    <p><strong>Purpose:</strong> Breakout rooms are a time for event attendees to collaborate. It is a working session. Sessions are led by a facilitator, but do not follow a structured agenda.</p>

    <p>Welcome attendees into the room, remind people to join with the audio microphone icon, not the headset (listen only) icon. If a person has an icon with a headset next to their name, they have joined via listen only.</p>

    <p>Ask users to close the main meeting hall BBB tab in their browser while they are in the breakout room. As the facilitator, please make sure you also do this. Audio from any other open BBB meeting rooms in other tabs will come through the users computer audio and it will be heard in the breakout room.</p>

    <p>Breakout rooms may be recorded.  Prior to the beginning of recording, the facilitator should notify the attendees that the session will be recorded. If people do not want to be visible in the recording, they should turn off their video. The recording will only record user webcam video and audio input during the session. Recordings will not capture presentations, shared screens, public or private chats, or shared notes.</p>

    <p>At the end of the breakout session, post the link to the main hall in the public chat so attendees can navigate back to the main room. Remind people to close the current meeting room window tab in their browser.</p>

    <p>Copy the notes from the shared notes section and send them to the appropriate person for posting to the Gitlab group.</p>

  <h6>Actions: </h6>

  <p><strong>Mute a user</strong> </p>

<p>If you need to mute a user because of background noise from their mic, click on their name in the participant list. Select MUTE USER in the list</p>

<div className={'center'}>
<p><img alt={'mute user'} src={BBB_1} /></p>
</div>

<p><strong>Mute all users</strong> </p>

<p>If you need to mute all users or all users except the presenter, click the settings icon above the user list and select either MUTE ALL USERS or MUTE ALL USERS EXCEPT PRESENTER 
</p>

<div className={'center'}>
<p><img alt={'mute all users'} src={BBB_2} /></p>
</div>

<p><strong>Tell users how to switch from listen only to audio</strong></p>

<p>If a user is in listen only, instruct them to:</p>

<p>
<ul>
<li>Leave the audio conference by clicking on the blue headphone button on the bottom of their screen</li>
<li>Join the audio conference again by clicking the same button, that now has a picture of a white phone with a line through it and 
  says "Join Audio" when the mouse hovers over it</li>
</ul>
</p>

<div className={'center'}>
<p><img alt={'rejoin audio'} src={BBB_3} /></p>
</div>

<p>
<ul>
<li>When prompted on how they would like to join audio, select the "microphone" icon</li>
</ul>
</p>

<div className={'center'}>
<p><img alt={'join with mic'} src={BBB_4} /></p>
</div>

<p>
<ul>
<li>Complete the echo test when prompted and they will be added back into the audio conferencing in the room</li>
</ul>
</p>

<div className={'center'}>
<p><img alt={'echo test'} src={BBB_5} /></p>
</div>

<p><strong>Share your screen</strong></p>
<p>To share your screen with attendees: </p>

<p>
<ul>
<li>Click on the round button at the bottom of the screen on the right side of the button row, with the white picture of the monitor with a line through icon</li>
</ul>
</p>

<div className={'center'}>
<p><img alt={'share your screen'} src={BBB_6} /></p>
</div>

<p>
<ul>
<li>When prompted by the pop up window, select the picture of the screen you want to share (there may be more than one if you are using multiple monitors) click SHARE</li>
</ul>
</p>

<div className={'center'}>
<p><img alt={'share your screen step 2'} src={BBB_7} /></p>
</div>

<p>
<ul>
<li>To stop sharing your screen, click STOP SHARING on the bar at the bottom of your shared screen</li>
</ul>
</p>

<div className={'center'}>
<p><img alt={'stop sharing screen'} src={BBB_8} /></p>
</div>

<p><strong>If a participant wants to share their screen </strong></p>

<p>
<ul>
<li>Give them presenter mode by clicking on the user's name on the attendee list and select MAKE PRESENTER</li>
</ul>
</p>

<div className={'center'}>
<p><img alt={'presenter given screen'} src={BBB_9} /></p>
</div>

<p>
<ul>
<li>The user with presenter status will have the presenter icon on their username icon</li>
</ul>
</p>

<div className={'center'}>
<p><img alt={'presenter icon'} src={BBB_10} /></p>
</div>

<p>
<ul>
<li>After the user is done presenting, click on your name in the user list and select TAKE PRESENTER
</li>
</ul>
</p>

<div className={'center'}>
<p><img alt={'presenter take back'} src={BBB_11} /></p>
</div>

<p><strong>Create a Poll</strong> </p>

<p>
<ul>
<li>When you have presenter status, you can create a poll by clicking on the plus sign button on the left side of the bottom bar. Select "Start a Poll"
</li>
</ul>
</p>

<div className={'center'}>
<p><img alt={'start a poll'} src={BBB_12} /></p>
</div>

<p>
<ul>
<li>Use the pane that pops up on the left side to create the poll questions and response options
</li>
</ul>
</p>

<div className={'center'}>
<p><img alt={'poll setup'} src={BBB_13} /></p>
</div>

<p>After users vote, it will publish results on the current presentation slide. Results are displayed as totals. The presenter can see individual responses from users as they come in, but the final displayed poll is an anonymous total and does not identify how individual users voted.</p>   

<p>
<strong>Start an interactive multi-user whiteboard</strong> <br />
In presenter mode, you can write on the whiteboard as part of a presentation or make it a shared whiteboard so all users can collaborate and write/type on it.
Steps:  
</p>

<p>
<ul>
<li>Start the whiteboard by progressing the presentation slides forward until you come to a blank slide</li>
</ul>
</p>

<div className={'center'}>
<p><img alt={'whiteboard start'} src={BBB_14} /></p>
</div>

<p>
<ul>
<li>Click on the picture of the white board icon at the bottom of the editing toolbar on the right side of the slide, it will say "turn on multi-user whiteboard" when you hoover your mouse over it. The icon will show two whiteboards after you click on it, indicating the whiteboard is interactive and all users can write on it.
</li>
<li>Click on the hand at the top of the toolbar and select an editing tool to use
</li>
</ul>
</p>

<div className={'center'}>
<p><img alt={'whiteboard use'} src={BBB_15} /></p>
</div>


<p>To leave the white board so only the presenter can draw on it, turn off the multi-user whiteboard icon at the bottom of the editing toolbar and select the editing tools you want to use.</p>

<p>
 <strong>Removing people from the room </strong> 
</p>

<p>
<ul>
<li>If you need to remove someone from the room or they need assistance leaving the room, click on the user's name on the attendee list and select REMOVE USER
</li>
</ul>
</p>

 <div className={'center'}>
<p><img alt={'remove user'} src={BBB_16} /></p>
</div>
</Section>
   </Layout>
);

export default BigBlueButton;
