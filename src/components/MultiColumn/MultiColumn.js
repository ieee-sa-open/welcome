import React from 'react';
import styles from './MultiColumn.module.css';
import PropTypes from 'prop-types';

const MultiColumn = ({ children }) => {
  return <div className={styles.multiColumn}>{children}</div>;
};

MultiColumn.propTypes = {
  children: PropTypes.node,
};

export default MultiColumn;
