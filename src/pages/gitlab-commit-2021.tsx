import React from 'react';
import { Helmet } from 'react-helmet';
import { PageProps } from 'gatsby';

import Layout from '../components/Layout/Layout';
import Hero from '../components/Hero/Hero';
import Section from '../components/Section/Section';

const GitLab2021: React.FC<PageProps> = () => (
  <Layout>
    <Helmet>
      <title>GitLab Commit Conference 2021</title>
    </Helmet>
    <Hero heroIntro={'SA OPEN'} heroPrimary={'GitLab Commit 2021'} />
    <Section type={'light'}>
      <div className={'center'}>
        <h6>SA OPEN Contributors Featured Talks @ Commit Virtual</h6>
        <p>
          <strong>
            It Takes a Village - How IEEE SA OPEN Supports Diversity
          </strong>
          <iframe
            width="560"
            height="315"
            src="https://www.youtube.com/embed/CAyqr8rg6yw"
            title="YouTube video player"
            frameborder="0"
            allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture"
            allowfullscreen
          ></iframe>
        </p>

        <p>
          <strong>Community Handbook Best Practices for Diversity & Inclusion</strong>
          <iframe
            width="560"
            height="315"
            src="https://www.youtube.com/embed/nrci2Z32gUI"
            title="YouTube video player"
            frameborder="0"
            allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture"
            allowfullscreen
          ></iframe>
        </p>
      </div>
    </Section>
  </Layout>
);

export default GitLab2021;
