import React from 'react';
import { Helmet } from 'react-helmet';
import { PageProps } from 'gatsby';

import Layout from '../components/Layout/Layout';
import Hero from '../components/Hero/Hero';
import Section from '../components/Section/Section';
import Button from '../components/Button/Button';
import Iframe from '../components/Iframe/Iframe';
import TableOfContents from '../components/TableOfContents/TableOfContents';

const Events: React.FC<PageProps> = () => {
  return (
    <Layout>
      <Helmet>
        <title>Events - IEEE SA OPEN</title>
      </Helmet>
      <Hero heroIntro={'IEEE SA Open'} heroPrimary={'Events'} />
      <TableOfContents
        items={[
          {
            text: 'Events Calendar',
            link: '#calendar',
          },
          {
            text: 'Submit an Event',
            link: '#submit',
          },
          {
            text: 'Prior Events',
            link: '#prior',
          },
          {
            text: 'Training',
            link: '#training',
          },
        ]}
      />
      <Section type={'light'}>
        <a className={'anchor'} id={'calendar'} />
        <h1>Events</h1>
        <p>
          This greater Open Source event calendar helps centralize all Open
          Source events on one calendar to make it easier for you to attend and
          contribute. If you'd like to submit your event to be added, please
          invite this email to your event and{' '}
          <a
            href="https://opensource.ieee.org/marketing-advisory-group/events/-/issues/new"
            target="_blank"
            rel="noreferrer"
          >
            fill out a brief form
          </a>{' '}
          Not a member with IEEE SA OPEN?{' '}
          <a
            href="https://www.ieee.org/profile/public/createwebaccount/showCreateAccount.html?url=https%3A%2F%2Fwww.ieee.org%2F%2F"
            target="_blank"
            rel="noreferrer"
          >
            Create your account
          </a>{' '}
          to submit your event today!{' '}
        </p>
        <p>
          All times on calendar below are shown in the the user's computer time
          zone.
        </p>
        <div className={'center'}>
          <Iframe
            src={
              'https://calendar.google.com/calendar/embed?src=c_iuboq60p6a8rekskm9v6299as4%40group.calendar.google.com&ctz=local'
            }
            width={'800'}
            height={'600'}
          />
        </div>
      </Section>
      <Section type={'light'}>
        <a className={'anchor'} id={'submit'} />
        <div className={'center'}>
          <h4>Submit an Event</h4>
          <p>
            If you have an event you would like to add to the calendar, please
            submit it using the corresponding template on the event submission
            form.
          </p>
          <Button
            link={
              'https://opensource.ieee.org/marketing-advisory-group/events/-/issues/new'
            }
          >
            Submit Event
          </Button>
        </div>
      </Section>
      <Section type={'dark'}>
        <a className={'anchor'} id={'prior'} />
        <div className={'center'}>
          <h4>Prior Events</h4>
        </div>
        <p>
          <ul>
            <li>
              <a href="/ato2021" target="_blank">
                All Things Open Conference October 17-19, 2021
              </a>
            </li>
            <li>
              <a href="/gitlab-commit-2021" target="_blank">
                GitLab Commit Virtual 2021 August 3-4, 2021
              </a>
            </li>
            <li>
              <a href="/workshop3" target="_blank">
                TAG You're IT: Technical Infrastructure In Open Source
                Communities June 16th, 2021
              </a>
            </li>
            <li>
              <a href="/workshop2" target="_blank">
                Marketing Advisory Group Workshop MAGic In The Making: Creating
                Open Source Marketing Resources May 4th, 2021
              </a>
            </li>
            <li>
              <a href="/workshop1" target="_blank">
                Advisory Group Workshop The Spirit Of Mentorship, December 8th,
                2020
              </a>
            </li>
          </ul>
        </p>
      </Section>
      <Section type={'light'}>
        <a className={'anchor'} id={'training'} />
        <div className={'center'}>
          <h4>Training Events</h4>
          <p>
            If your company or organization is interested in training events for
            integrating the IEEE SA OPEN platform with your team. Please contact
            us directly. We we can design a virtual training event tailored to
            meet your needs.
          </p>
          <Button link={'mailto:opensource@ieee.org'}>Contact Us</Button>
        </div>
      </Section>
    </Layout>
  );
};

export default Events;
