import React from 'react';
import { Helmet } from 'react-helmet';
import { PageProps } from 'gatsby';

import Layout from '../components/Layout/Layout';
import Hero from '../components/Hero/Hero';
import Section from '../components/Section/Section';
import Bio from '../components/Bio/Bio';
import Button from '../components/Button/Button';
import ArticleSlider from '../components/ArticleSlider/ArticleSlider';

// Headshots
import SilonaHeadshot from '../images/bio/silona_headshot.png';
import RaulHeadshot from '../images/bio/raul_headshot.png';
import KatherineHeadshot from '../images/bio/katherine_schueths_headshot.png';
import BethHeadshot from '../images/bio/beth_headshot.png';
import StaciHeadshot from '../images/bio/staci_headshot.png';
import AndyHeadshot from '../images/bio/andy_headshot.png';
import SoniaHeadshot from '../images/bio/sonia_santana_headshot.png';
import JoshHeadshot from '../images/bio/josh_headshot.png';
import LisaHeadshot from '../images/bio/lisa_headshot.png';
import AaronHeadshot from '../images/bio/aaron_geiser_headshot.png';
import RobbyHeadshot from '../images/bio/robby_headshot.png';
import HeatherHeadshot from '../images/bio/heather_headshot.png';
import EmilyHeadshot from '../images/bio/emily_brown_headshot.png';
import SecretHeadshot from '../images/bio/secret_awesome_woman_headshot.png';
import ToddHeadshot from '../images/bio/todd_higgins_headshot.png';


// Article Slider
import SilonaArticle from '../images/about/slider/silona-bonewald.png';
import TeamArticle from '../images/about/slider/team-photo.png';
import MediumArticle from '../images/about/slider/cover_medium.png';
import FlossArticle from '../images/about/slider/floss_albumart_mask.png';

const OurTeam: React.FC<PageProps> = () => (
  <Layout>
    <Helmet>
      <title>Our Team - IEEE SA OPEN</title>
    </Helmet>
    <Hero heroIntro={'IEEE SA Open'} heroPrimary={'Our Team'} />
    <Section type={'light'}>
      <h1>Contact Us</h1>
      <div className={'center'}>
        <p>
          Got a question about IEEE’s Open Source Community? We want to hear
          from you.
        </p>
        <Button link={'mailto:opensource@ieee.org'}>Contact Us</Button>
      </div>
      <p>
        Whether your question is about IEEE SA OPEN, an IPR issue, export controls, using the IEEE SA OPEN platform, 
      creating your own partnership or anything else, our team is standing by. Reach out by topic below or reach out 
      to a member of our team on IEEE SA OPEN Mattermost. Our handles are in the bios below.
      </p>
      <ul>
        <li>
          <a href={'https://opensource.ieee.org/users/sign_in'}>Sign up</a>
        </li>
        <li>
          <a href="mailto:opensourceopps@ieee.org">Partnerships</a>
        </li>
        <li>
          <a href={'https://opensource.ieee.org/welcome/service-desk/-/issues'}>Support</a>
        </li>
        <li>
          <a
            href={'https://opensource.ieee.org/community/cla/faq/-/wikis/home'}
          >
            IEEE SA OPEN FAQ
          </a>
        </li>
        <li>
          <a href={'https://opensource-connect.ieee.org/community/'}>Chat</a>
        </li>
      </ul>
    </Section>
    <Section type={'dark'}>
      <h1>Our Team</h1>
      <Bio
        image={SilonaHeadshot}
        name={'Silona Bonewald'}
        title={'Executive Director of IEEE SA OPEN'}
        description={
          'Silona Bonewald is the Executive Director for IEEE SA OPEN, a comprehensive platform offering the open source community cost-effective options for developing and validating their projects. Previously she was vice president of community architecture at Hyperledger, a global open source collaborative effort hosted by The Linux Foundation, where she integrated leaders in finance, banking, Internet of Things (IoT), supply chains, and manufacturing. Other notable career accomplishments include, while with PayPal, pioneering the InnerSource process; for Siemens AG, creating a cutting-edge and Six Sigma-compliant e-commerce website; and for Ubisoft, creating an international content management system (CMS) architecture.'
        }
      />
      <div className={'left'}>
      <p>@silona</p>
      </div>
      <Bio
        image={JoshHeadshot}
        name={'Joshua Gay'}
        title={'IEEE Senior Manager for Community & Infrastructure'}
        description={
          'Joshua has over 20 years of experience working in the free and open source software world. He has had roles as advocate, developer, licensing and compliance manager, communications director, and community manager. Joshua has a strong background in both non-profit organizational management and textbook publishing. He has served as the campaigns manager and  licensing and the compliance manager for the Free Software Foundation. He was a technical-content manager for the open education textbook organization, CK-12 Foundation. Joshua has also  been an affiliate with the Berkman-Klein Center for Internet & Society at Harvard.'
        }
      />
        <div className={'left'}>
      <p>@jgay</p>
      </div>
      <Bio
        image={EmilyHeadshot}
        name={'Emily Brown'}
        description={
          "Emily is an outgoing business management and marketing strategist with over ten years of experience working with for profit and nonprofit organizations. Her passions include serving underserved communities in financial, technological, and community advancements. She started her career in the nonprofit sector and dedicates her time consulting for small businesses to help them get the resources they need. She has been a part of multiple large operations at festivals, including South by Southwest and Austin City Limits. In her free time She helps run Linkedin Local ATX, a business networking event. She enjoys reading, listening to audiobooks, and playing with her dog. Emily's background with community advancement makes her a strong asset to the opensource community."
        }
      />
      <div className={'left'}>
      <p>@emily</p>
      </div>
      <Bio
        image={BethHeadshot}
        name={'Beth Hancock'}
        description={
          'Beth is a logistics expert who enjoys creating and refining processes. Over the past thirty years, she has held a variety of titles varying from Team Lead to Director. She has a deep commitment to supporting community-led projects and growing them through co-active coaching and servant-leadership. Beth has worked with a range of cooperative efforts: from volunteer-run arts festivals to team-led Agile software development. A supporter of Open Source software since writing email in Pine during her college years, she is grateful to have this opportunity to support the Open Source community.'
        }
      />
       <div className={'left'}>
      <p>@emh.contact</p>
      </div>
      <Bio
        image={RaulHeadshot}
        name={'Raul Pineda'}
        description={
          'Raul is a passionate technologist who brings more than 24 years of hands-on experience in information systems, information technologies, and operations to the team. He has served as a senior leader of Cloud Architecture and Operations in the US Federal Department of Defense and Government Public Sectors for over 19 years. Prior to that, he was a solution architect in the banking, insurance, energy sectors. Outside of work he has been an IEEE SA OPEN Cloud SME volunteer and Technical Board Member, a DoD industry representative to the Society of Hispanic Professional Engineers (SHPE), and a School of Business & Technology at Huston-Tillotson University Board Member. When he isn’t working on tech, you will find him building art cars and camping.'
        }
      />
      <div className={'left'}>
      <p>@raul</p>
      </div>
      <Bio
        image={KatherineHeadshot}
        name={'Katie Schueths'}
        description={
          'Katie is a coordination guru whose domain lies behind the scenes. She has been a program manager and live event production coordinator for over 10 years. She has coordinated installations at the SXSW Festival and produced a wide range of live events. In her free time, she is involved in a number of community organizations. Katie hosts LinkedIn Local ATX professional networking events and collaborates on volunteer-run community art events, running operations logistics. Open source technology is a new domain for her and she is excited about the new opportunities within the open source community.'
        }
      />
      <div className={'left'}>
      <p>@katie</p>
      </div>
      <Bio
        image={SoniaHeadshot}
        name={'Sonia Santana'}
        description={
          "Sonia is an innovative web developer and small business owner with over 20 years of experience working with non-profit and for-profit clients, providing website design and hosting services. She has extensive experience designing, testing and maintaining websites. Sonia is skilled with Content Management Systems. She specializes in using HTML, CSS, JavaScript, PHP and SQL web development tools."
        }
      />
      <div className={'left'}>
      <p>@sonia</p>
      </div>
      <Bio
        image={LisaHeadshot}
        name={'Lisa Maginnis'}
        description={
          'Lisa Marie Maginnis is a long time free software advocate and hacker. She joined the IEEE as a contractor in 2019. Previously she worked in free software advocacy as well as developed systems to assist survivors of sexual assault. In her free time she is CEO for a software mentorship non-profit and co-owns a Mushroom farm and Mycology Lab where she resides in rural Appalachia.'
        }
      />
      <div className={'left'}>
      <p>@lisam</p>
      </div>
      <Bio
        image={SecretHeadshot}
        name={'Secret Awesome Woman'}
        description={
          "This awesome lady is an energetic and dedicated developer with over eight years of experience. She specializes in systems administration and has seven years of experience working with the open source community. In her free time, she enjoys working on engines and working with the community to bring about positive social development."
        }
      />
      <Bio
        image={AaronHeadshot}
        name={'Aaron Geiser'}
        description={
          'Aaron is a creative technologist and open source software advocate with over 15 years of experience designing and implementing technical solutions for organizations of all sizes.  He has led design and software engineering teams to build new, innovative products and has consulted with a broad portfolio of Fortune 500 clients to aid digital transformation initiatives'
        }
      />
      <div className={'left'}>
      <p>@aarongeiser</p>
      </div>
       <Bio
      image={ToddHeadshot}
      name={'Todd Higgins'}
      description={
        "Todd is an ardent technologist with 15 years of experience in information technology, administration, and DevOps. He has successfully built teams and transitioned organizations to DevOps methodologies, industry compliance standards, and security proficiency. When he isn't working on tech, Todd enjoys reading about astronomy, broadcasting, and flying spaceships online."
      }
    />
      <div className={'left'}>
      <p>@todd</p>
      </div>
      <h1>Other Contributors</h1>
      <Bio
        image={RobbyHeadshot}
        name={'Robby Robson'}
        description={
          'Dr. Robby Robson is a researcher, entrepreneur, and standards professional who is co-founder and CEO of Eduworks Corporation and serves as a member of the IEEE Standards Association Board of Governors and as chair of the IEEE Standards Education Committee as well as the Open Source Committee and IEEE Computer Society Standards Activity Board Standards Committee. He has contributed to areas ranging from pure mathematics to web-based learning, digital libraries, and applications of AI to learning systems and has been involved in developing interoperability and data standards and open source technology related to learning, education, training, and workforce development for over 20 years. His recent efforts focus on applying AI and open data to provide talent analytics insights and enable rapid, effective, and equitable reskilling for workers. '
        }
      />
    <h1>Prior Contributors</h1>
      <Bio
        image={HeatherHeadshot}
        name={'Heather Vescent'}
        description={
          'Heather Vescent is a futurist, author and researcher. Vescent has delivered strategic research insights to governments and corporations in digital identity, military learning, payments, transactions, and new economic models. She is the writer/producer of fourteen documentaries and short films about future technology. Her research has been covered in the New York Times, CNN, American Banker, CNBC, Fox, and the Atlantic. She is an author of The Secret of Spies and The Cyber Attack Survival Manual published by Weldon-Owen, and co-authored The Comprehensive Guide to Self Sovereign Identity. Her work has won multiple awards from the Association of Professional Futurists.'
        }
      />
       <Bio
        image={StaciHeadshot}
        name={'Staci Andrews'}
        description={
          "Staci is an experience designer, design researcher and artist. She has had the privilege of designing across many platforms and verticals. Over the years she's designed and developed a broad array of physical and digital experiences for everyone from DARPA and Pfizer to Disney and Meow Wolf. After completing her degree in Business and Communication Design she went on to build a foundation in design and design thinking working for frog Design in Austin. Later she spent time working to merge the worlds of digital and physical at the international architecture firm, Gensler. Currently she works as a full time Experience Designer for Meow Wolf crafting pathways through the multiverse while collaborating on other compelling projects and ventures."
        }
      />
       <Bio
        image={AndyHeadshot}
        name={'Andy Oram'}
        description={
          "Andy is a writer and editor in the computer field. His editorial projects at O'Reilly Media ranged from a legal guide covering intellectual property to graphic novels. Andy often writes topics of health IT, on Internet related policy issues, and on trends affecting technical innovation and its effects on society. Print publications where his work has appeared include The Economist, Communications of the ACM, Copyright World, the Journal of Information Technology & Politics, Vanguardia Dossier, and Internet Law and Business. Conferences where he has presented talks include O'Reilly's Open Source Convention, FISL (Brazil), FOSDEM (Brussels), DebConf, and LibrePlanet. Andy participates in the Association for Computing Machinery's policy organization, USTPC."
        }
      />
    </Section>
    <Section type={'light'}>
      <ArticleSlider
        articles={[
          {
            image: SilonaArticle,
            description:
              'Silona Bonewald Joins IEEE SA OPEN as First Executive Director',
            link:
              'https://standards.ieee.org/news/2020/silona-bonewald-ieeesa-open-first-executive-director.html',
          },
          {
            image: TeamArticle,
            description:
              'Why your open source project needs more than just coders',
            link:
              'https://opensource.com/article/20/9/open-source-role-diversity',
          },
          {
            image: MediumArticle,
            description: 'CHAOSS Podcast: Role Diversity with Silona Bonewald',
            link: 'https://podcast.chaoss.community/16',
          },
          {
            image: FlossArticle,
            description:
              'FLOSS Weekly Podcast: Embracing Open Source & Open Standards',
            link:
              'https://twit.tv/shows/floss-weekly/episodes/584?autostart=false',
          },
        ]}
      />
    </Section>
  </Layout>
);

export default OurTeam;
