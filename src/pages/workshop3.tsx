import React from 'react';
import { Helmet } from 'react-helmet';
import { PageProps } from 'gatsby';

import Layout from '../components/Layout/Layout';
import Hero from '../components/Hero/Hero';
import Section from '../components/Section/Section';
import TableOfContents from "../components/TableOfContents/TableOfContents";
import MultiColumn from '../components/MultiColumn/MultiColumn';
import Column from '../components/Column/Column';
// Headshots
import Bio1 from '../images/workshops/nuritzi-sanchez-headshot.png';
import Bio2 from '../images/workshops/john-coghlan-headshot.png';
import Bio3 from '../images/workshops/christopher-litsinger-headshot.png';

const TAGWorkshop: React.FC<PageProps> = () => (
  <Layout>
        <Helmet>
            <title>TAG You're IT: Technical Infrastructure In Open Source Communities</title>
        </Helmet>
        <Hero heroIntro={'SA OPEN'} heroPrimary={'Workshop Events'} />
        <TableOfContents
            items={[
                {
                    text: 'Workshop Notes',
                    link: '#notes',
                },
                {
                    text: 'Breakout Session 1',
                    link: '#breakout1',
                },
                {
                    text: 'Breakout Session 2',
                    link: '#breakout2',
                },
                {
                    text: 'Guest Speakers',
                    link: '#speakers',
                },
            ]} />
        <Section type={'light'}>
            <h6>
                Technical Advisory Group Workshop
                <br />
                TAG You're IT: Technical Infrastructure In Open Source Communities<br />
                June 16th, 2021</h6>
            <p>
                {' '}
                Managing an open source project gives contributors the opportunity to
                make a positive impact on the global community. It also comes with the
                complexities of maintaining the infrastructure to support it. Join us
                as we collaborate on Open Source infrastructure processes and discuss
                strategies for making your journey as seamless as possible. Topics at
                the workshop will include security, production readiness, project
                marketing foundations, and Open Source tool evaluation.
            </p>
        </Section>
 
        <Section type={'dark'}>
            <a className={'anchor'} id={'notes'} />
            <h6>Workshop Notes:</h6>
            <div className={'center'}>
                <p>
                    <a href="https://opensource.ieee.org/workshops/june-tag-workshop/20210616-tag-workshop/-/blob/main/Event%20Notes%20and%20Recordings/readme.md" target='_blank'>Workshop Notes & Recordings</a>.
                </p>
            </div>
        </Section>
        <Section type={'light'}>
            <a className={'anchor'} id={'breakout1'} />
            <h6>Breakout Session 1:</h6>
            <p>
                <b>11:40 AM</b> <br />
                <ul>
                    <li><b>Breakout Room 1:</b>{' '} 
                    <a href="https://opensource.ieee.org/workshops/june-tag-workshop/20210616-tag-workshop/-/blob/main/Event%20Notes%20and%20Recordings/Notes/os-security-breakout.md">Open Source Security</a> <br />
                    Protecting your community from vulnerabilities is critical to your project's sustainability. We identify tools and best practices for open source security.</li>
                    <li><b>Breakout Room 2:</b>{' '}
                    <a href="https://opensource.ieee.org/workshops/june-tag-workshop/20210616-tag-workshop/-/blob/main/Event%20Notes%20and%20Recordings/Notes/production-readiness-breakout.md">Production Readiness: Planning for Success</a><br />
                    Building software for a global audience requires a good plan. This
                    discussion shared ideas on what it takes to be ready for when your
                    community grows! Marketing Metrics and Badging Initiatives Defining
                    how badging initiatives can help promote marketing goals
                    </li>
                </ul>
            </p>
        </Section>
       <Section type={'dark'}>
            <a className={'anchor'} id={'breakout2'} />
            <h6>Breakout Session 2:</h6>
            <p>
                <b>1:05 PM</b> <br />
                <ul>
                  <li><b>Breakout Room 1:</b>{' '}
                  <a href="https://opensource.ieee.org/workshops/june-tag-workshop/20210616-tag-workshop/-/blob/main/Event%20Notes%20and%20Recordings/Notes/Open%20Source%20Marketing%20Foundations%20for%20Success.md">Source Marketing: Foundations for Success</a> <br />
                  Explore how the Marketing Advisory Group on the IEEE SA OPEN platform can create processes and resources to help with project
                  and community success.
                 </li>
                 <li><b>Breakout Room 2:</b>{' '} 
                  <a href="https://opensource.ieee.org/workshops/maymagworkshop/202105-workshop-notes-and-recordings/-/blob/main/Notes/mag-session2-badgingb-notes.md">
                  Marketing Metrics and Badging Initiatives Defining the measure of success in the marketing open source community</a>
                </li>
                <li><b>Breakout Room 3:</b>{' '}
                <a href="https://opensource.ieee.org/workshops/june-tag-workshop/20210616-tag-workshop/-/blob/main/Event%20Notes%20and%20Recordings/Notes/tool-eval-breeakout.md">Finding the Right Tools: Creating Best Practices for Feature and Tool Evaluation</a> <br />
                What tools do you use in creating, supporting and delivering opeN source projects? This discussion was about developing feature and tool evaluation
                best practices for the IEEE SA OPEN Community.
                </li>
                </ul>
            </p>
        </Section>
        <Section type={'light'}>
        <a className={'anchor'} id={'speakers'} />
        <h1>Workshop Guest Speakers</h1>
        <h6>How to develop an open source mindset that enables greater impact</h6>
        <MultiColumn>
        <Column>
            <h6>Nuritzi Sanchez</h6>
            <p>
              Nuritzi is a Sr. Open Source Program Manager at GitLab, Inc.,
              where she leads the{' '}
              <a
                href="https://about.gitlab.com/solutions/open-source/"
                target='_blank"'
              >
                GitLab for Open Source program
              </a>
              . Previously, Nuritzi helped start Endless, a company creating
              computers for users with little-to-no internet access, and she is
              a former Chairperson of the Board of Directors for the GNOME
              Foundation. Bringing a people-centric lens to all she does,
              Nuritzi aims to help open source projects thrive and help more
              people become contributors. 
            </p>
          </Column>
        <Column>
            <img alt={'Nuritzi Sanchez'} src={Bio1} />
          </Column>
        </MultiColumn>
        <MultiColumn>
        <Column>
            <img alt={'John Goghlan'} src={Bio2} />
          </Column>
          <Column>
            <h6>John Coghlan</h6>
            <p>
              {' '}
              John's passion is building inclusive communities that help people
              love their work. This has taken him from managing large scale
              corporate volunteer projects at United Way of New York City to
              founding one of Brooklyn’s first coworking spaces to becoming a
              community professional with a focus on technology. Currently, he
              manages Developer Evangelism for GitLab.
            </p>
          </Column>
          </MultiColumn>
         
          <p>
           <strong>Nuritzi & John's Combined Talk and Presentation:</strong>
              <ul>
               <li><a href="https://opensource.ieee.org/workshops/june-tag-workshop/20210616-tag-workshop/-/blob/main/Event%20Notes%20and%20Recordings/Notes/IEEE_TAG_Talk_How_to_develop_an_open_source_mindset.pdf">Presentation PDF file </a></li>
               <li><a href="https://opensource.ieee.org/workshops/june-tag-workshop/20210616-tag-workshop/-/blob/main/Event%20Notes%20and%20Recordings/Breakout%20Recordings/Speaker%20recordings/Nuritzi_John_Ieee_Sa_Open_Presentation_small.m4v">Speaker Recording</a></li>
              </ul>
        </p>       
        <h6>A Backyard Jungle of Technical Debt</h6>
        <MultiColumn>
        <Column>
        <h6>Christopher Litsinger</h6>
        <p>
          Christopher worked in various development and engineering roles before
          settling into roles focusing on the intersection of infrastructure and
          code. Then, after years of resisting suggestions that he try out
          management, Christopher agreed to take on a leadership role and
          quickly discovered that building teams is still building things. He's
          been focused on how to effectively lead engineering teams since.
          Christopher is currently Director, Software Development and
          Engineering at Comcast Cable. <br /> 
          <a href="https://opensource.ieee.org/workshops/june-tag-workshop/20210616-tag-workshop/-/blob/main/Event%20Notes%20and%20Recordings/Breakout%20Recordings/Speaker%20recordings/A_Backyard_Jungle_Of_Technical_Debt_Ieee_Sa_Open_Video_small.webm">Speaker Recording</a>
        </p>
        </Column>
        <Column>
            <img alt={'Christopher Litsinger'} src={Bio3} />
        </Column>
        </MultiColumn>
      </Section>
   </Layout>
);

export default TAGWorkshop;

