import React from 'react';
import { Helmet } from 'react-helmet';
import { PageProps, Link } from 'gatsby';

import Layout from '../components/Layout/Layout';
import Hero from '../components/Hero/Hero';
import Section from '../components/Section/Section';
import Example from '../components/Example/Example';
import TableOfContents from '../components/TableOfContents/TableOfContents';

import i336_1_1 from '../images/getting-started/336_1_1.png';
import i336_2_1 from '../images/getting-started/336_2_1.png';
import i336_3_1 from '../images/getting-started/336_3_1.png';
import i336_4_1 from '../images/getting-started/336_4_1.png';
import i336_5_1 from '../images/getting-started/336_5_1.png';
import i338_1 from '../images/getting-started/338_1_1.png';
import i338_2 from '../images/getting-started/338_2_1.png';
import i338_3 from '../images/getting-started/338_3_1.png';
import i338_4 from '../images/getting-started/338_4_1.png';
import i338_5 from '../images/getting-started/338_5_1.png';
import i338_6 from '../images/getting-started/338_6_1.png';
import i338_Issue_1_1 from '../images/getting-started/338_Issue_1_1.png';
import i338_Issue_2_1 from '../images/getting-started/338_Issue_2_1.png';
import i338_Issue_3_1 from '../images/getting-started/338_Issue_3_1.png';
import i338_Issue_4_1 from '../images/getting-started/338_Issue_4_1.png';
import i338_Issue_5_1 from '../images/getting-started/338_Issue_5_1.png';
import i338_Issue_6_1 from '../images/getting-started/338_Issue_6_1.png';

const CreatingInGitlab: React.FC<PageProps> = () => (
  <Layout>
    <Helmet>
      <title>Getting Started - IEEE SA OPEN</title>
    </Helmet>
    <Hero heroIntro={'IEEE SA Open'} heroPrimary={'Creating in GitLab'} />
    <TableOfContents
      items={[
        {
          text: 'Table of Contents',
          link: '/getting-started',
        },
        {
          text: 'Creating a Group',
          link: '#group',
        },
        {
          text: 'Creating a Project',
          link: '#project',
        },
        {
          text: 'Creating an Issue',
          link: '#issue',
        },      
      ]}
    />
    <Section type={'light'}>
    <a className={'anchor'} id={'group'} />
    <a className={'anchor'} id={'#group'} />
    <h1>Creating a Group</h1>
      <h3>Groups</h3>
      <p>Gitlab CE is arranged into different levels:</p>
      <Example>
        <p>
          <b>
            Groups {'>'} Subgroups {'>'} <Link to={'#project'}>Projects</Link>{' '}
            {'>'} <Link to={'#issue'}>Issues</Link>
          </b>
        </p>
      </Example>
      <p>
        As volunteers and group members, you will be working primarily within
        your projects and with issues. If you would like to begin work on a new
        project, create it in the{' '}
        <a href={'https://opensource.ieee.org/hosted-projects/'}>
          007 Hosted Projects Group
        </a>
        . If you want to experiment with GitLab CE and its functions, please use
        the{' '}
        <a href={'https://opensource.ieee.org/sandbox'}>
          006 Sandbox Group
        </a>
        . This space will be periodically cleaned up.
      </p>

      <h3>How to Create a Group</h3>
      <p>To create a group, follow the instructions here.</p>
      <Example>
        <p>
          In the top menu, click Groups and then Your Groups, and click the
          green "New group" button.
        </p>
        <img alt="group chat 1" src={i336_1_1} />
        <p>
          Or, in the top menu, click on the "+" sign and choose "New group".
        </p>
        <img alt="group chat 2" src={i336_2_1} />
        <p>
          The New Group page will open and you will add the following
          information:
        </p>
        <img alt="group chat 3" src={i336_3_1} />
      </Example>
      <h4>Group Name</h4>
      <p>
        The Group Name will automatically populate the URL. Optionally, you can
        change it. This is the name that displays in group views.
      </p>
      <p>
        The Group Name can contain only alphanumeric characters, underscores,
        dashes, dots, and spaces.
      </p>

      <h4>Group URL</h4>
      <p>
        The Group URL is the namespace under which your projects will be hosted.
        The URL can contain only alphanumeric characters, underscores, dashes
        and dots. It cannot start with dashes or end in a dot.
      </p>

      <h4>Group Description</h4>
      <p>
        The description field is optional, but is incredibly helpful to
        communicate with others who may help with your projects.
      </p>

      <h4>Group Avatar</h4>
      <p>
        You may choose to upload a custom avatar for your group here. This will
        be auto-populated if you do not, and may also be changed later.
      </p>

      <h4>Group Visibility Level</h4>
      <p>
        The Visibility Level of the group is an important choice. GitLab allows
        owners to set a project’s visibility as public, internal, or private.
        When making this choice, please keep in mind that subgroups or projects
        cannot have a higher visibility level than that of the group in which
        they are created. We strongly suggest creating groups with visibilities
        of either Public or Internal.
      </p>

      <Example>
        <p>
          <b>Public Groups</b>
          <br />
          These groups will be visible in the Explore Public Groups directory
          for all community members and potential members to explore. Any logged
          in user will have guest permissions on the repository.
        </p>
        <p>
          <b>Internal Groups</b>
          <br />
          These groups are also listed in the Explore Public Groups directory,
          but are visible only to logged in users. Any logged in user will have
          guest permissions on the repository.
        </p>
        <p>
          <b>Private Groups</b>
          <br />
          Private groups can only be viewed by group members (except for guests
          specifically invited to view the group). They will appear in the
          Explore Public Groups directory for group members only.
        </p>
      </Example>

      <h4>Mattermost</h4>
      <p>
        IEEE SA OPEN also hosts a{' '}
        <a href={'https://opensource-connect.ieee.org/'}>
          Mattermost chat service
        </a>
        . This can be accessed with the same credentials that you use to log in
        to the IEEE SA OPEN GitLab. If you would like to have a chat discussion
        space for your group, please check the box next to “Create a Mattermost
        team…” A Team will be created in Mattermost, as shown below. More
        information about joining
        <Link to={'#mmbasics'}> IEEE SA OPEN Mattermost</Link> is available.
      </p>

      <Example>
        <img alt="group chat 4" src={i336_4_1} />
      </Example>

      <h3>Group settings</h3>
      <p>
        After creating a group, you can manage its settings by navigating to the
        group's dashboard and clicking [Settings] in the left menu bar.
      </p>

      <Example>
        <img alt="group chat 5" src={i336_5_1} />
      </Example>
    </Section>
    <Section type={'dark'}>
      <a className={'anchor'} id={'project'} />
      <a className={'anchor'} id={'#project'} />
      <h1>Creating a Project</h1>

      <h3>Projects</h3>
      <p>Gitlab CE is arranged into different levels:</p>

      <Example>
        <p>
          <b>
            <Link to={'#group'}>Groups</Link> {'>'} Subgroups {'>'} Projects{' '}
            {'>'} <Link to={'#issue'}>Issues</Link>
          </b>
        </p>
      </Example>

      <p>
        As volunteers and group members, you will be working primarily within
        your projects and with issues. If you would like to begin work on a new
        project that does not belong in an existing group, please create it in
        the{' '}
        <a href={'https://opensource.ieee.org/hosted-projects'}>
          007 Hosted Projects Group
        </a>
        . If you want to experiment with GitLab CE and its functions, please use
        the{' '}
        <a href={'https://opensource.ieee.org/sandbox'}>
          006 Sandbox Group
        </a>
        . This space will be periodically cleaned up.
      </p>

      <h3>Create a Project</h3>

      <p>
        Most work in IEEE SA OPEN GitLab is done within a project. Files and
        code are saved in projects; and most features are used within the scope
        of projects. In GitLab, you can create a project for hosting your
        codebase, use it as an issue tracker, collaborate on code, and
        continuously build, test, and deploy your app with built-in GitLab
        CI/CD.
      </p>

      <p>To create a project in GitLab:</p>

      <Example>
        <p>
          Click the green New Project button at the top right of the Projects
          page, at the top right of a Group page or or use the [+] icon in the
          main navigation bar. These actions all open the New Project page.
        </p>
        <img alt={'gitlab projects 1'} src={i338_1} />
        <p>On the New Project page, choose if you want to:</p>
        <ul>
          <li>Create a Blank Project.</li>
          <li>
            Create a project using one of the available project templates.
          </li>
          <li>
            <a
              href={'https://docs.gitlab.com/ee/user/project/import/index.html'}
            >
              Import
            </a>{' '}
            a project from a different repository.
          </li>
        </ul>
      </Example>

      <h3>Blank Projects</h3>

      <p>To create a new Blank Project on the New Project page:</p>

      <Example>
        <img alt={'gitlab projects 2'} src={i338_2} />
      </Example>

      <p>On the Blank Project tab, provide the following information:</p>

      <Example>
        <p>
          <b>Project Name</b>
          <br />
          Add the name of your project in the Project Name field. Note that
          GitLab CE allows the use of spaces, hyphens, underscores or even emoji
          when naming a project, but does not allow the use of special
          characters. When adding the name, the Project Slug will auto-populate.
          The slug is what the GitLab instance will use as the URL path to the
          project. If you want a different slug, input the Project Name first,
          then change the slug after.
        </p>
        <p>
          <b>Project Slug</b>
          <br />
          The path to your project in the Project Slug field. This is the URL
          path for your project that the GitLab instance will use. If you choose
          to populate the Project Slug before the Project Name, the Project Name
          will auto populate when you fill in the Project Slug.
        </p>
        <p>
          <b>Project Description</b>
          <br />
          The Project Description field enables you to enter a description for
          your project's dashboard, which will help others understand what your
          project is about. Although it's not required, filling this in is a
          good idea.
        </p>
        <p>
          <b>Project Visibility Level</b>
          <br />
          Changing the visibility level modifies the Project's viewing and
          access rights for users.
        </p>
        <p>Public Projects:</p>
        <ul>
          <li>
            Public projects can be cloned without any authentication over HTTPS.
          </li>
          <li>
            They will be listed in the Explore Public Groups directory for all
            community members and potential members.
          </li>
          <li>
            Any logged in user will have Guest permissions on the repository.
          </li>
        </ul>
        <p>Internal Projects</p>
        <ul>
          <li>Internal projects can be cloned by any logged in user.</li>
          <li>
            Internal projects will be listed in the Explore Public Groups
            directory, but will only be visible logged in users.
          </li>
          <li>
            Any logged in user will have Guest permissions on the repository.
          </li>
        </ul>
        <p>Private Projects</p>
        <ul>
          <li>
            Private Projects can only be cloned and viewed by project members
            (except for invited Guests).
          </li>
          <li>
            They will appear in the Explore Public Groups directory for project
            members only.
          </li>
        </ul>
        <p>
          <b>Initialize repository with a README</b>
          <br />
          Selecting this option creates a README file so that the Git repository
          is initialized, has a default branch, and can be cloned.
        </p>
      </Example>

      <p>
        When this information has been entered, click the green [Create project]
        button.
      </p>

      <h3>Built-in Templates</h3>
      <p>
        Built-in templates are project templates that are released with GitLab.
      </p>

      <Example>
        <img alt={'gitlab projects 3'} src={i338_3} />
      </Example>

      <p>To use a built-in template on the New Project page:</p>

      <Example>
        <p>On the Create from template tab, select the Built-in tab.</p>
        <p>From the list of available built-in templates, click:</p>
        <ul>
          <li>The Preview button to look at the template source itself.</li>
          <li>The [Use the template] button to start creating the project.</li>
        </ul>
        <p>
          Finish creating the project by filling out the project's details. The
          process is the same as that for creating a Blank Project, above.
        </p>
      </Example>

      <h3>Importing a New Project</h3>
      <p>
        <a href={'https://docs.gitlab.com/ee/user/project/import/index.html'}>
          Guidelines for importing
        </a>{' '}
        a project from a different repository are available. Projects can be
        imported from a variety of different sources.
      </p>

      <Example>
        Select the Invite Group tab
        <img alt={'gitlab projects 4'} src={i338_4} />
      </Example>

      <h3>Sharing Projects with a Group</h3>
      <p>
        The primary mechanism to give a group access to a project is to make
        that group the owner of the project. If the project already belongs to
        another group, group sharing feature can be of use.
      </p>

      <Example>
        <ul>
          <li>Use the left navigation menu to go to Members</li>
          <li>Select the Invite Group tab</li>
        </ul>
        <img alt={'gitlab projects 5'} src={i338_5} />
        <ul>
          <li>
            Add the chosen group with the *maximum* access level of your choice.
          </li>
          <li>Click Invite to share the project.</li>
          <li>
            After sharing the project with the chosen group, the group will
            appear in the Project Members screen. The project will be listed on
            the group's dashboard.
          </li>
        </ul>
        <img alt={'gitlab projects 6'} src={i338_6} />
      </Example>

      <p>Note that you can only share a project with:</p>

      <Example>
        <ul>
          <li>Groups for which you have an explicitly defined membership.</li>
          <li>
            Groups that contain a nested subgroup or project for which you have
            an explicitly defined role.
          </li>
        </ul>
      </Example>
    </Section>
    <Section type={'light'}>
      <a className={'anchor'} id={'issue'} />
      <a className={'anchor'} id={'#issue'} />
      <h1>Creating an Issue</h1>

      <h3>Issues</h3>

      <p>Gitlab is arranged into different levels:</p>

      <Example>
        <p>
          <b>
            <Link to={'#group'}>Groups</Link> {'>'} Subgroups {'>'}{' '}
            <Link to={'#project'}>Projects</Link> {'>'} Issues
          </b>
        </p>
      </Example>

      <p>
        As volunteers and group members, you will be working primarily within
        your projects and with issues.
      </p>

      <p>
        Issues are the fundamental medium for collaborating on ideas and
        planning work in the IEEE SA OPEN GitLab. They can be used to discuss
        the implementation of a new idea, track tasks and work status, accept
        feature proposals, ask questions, make support requests or bug reports,
        or elaborate on new code implementations.
      </p>

      <h3>Create a New Issue</h3>

      <p>There are many ways to create a new issue from within a project:</p>

      <Example>
        <p>
          In any Project's Dashboard, click the Issues bar (shown in bright
          green below). The two views most frequently used to track are List and
          Boards. The List view is seen below. In this view, you can create a
          New Issue by clicking the dark green New Issue button at the top
          right.
        </p>
        <img alt={'gitlab issues 1'} src={i338_Issue_1_1} />
        <p>
          This will take you to the full-page New Issue form. Discussion of the
          different parts of the form is below in Elements of the New Issue
          Form.
        </p>
        <img alt={'gitlab issues 2'} src={i338_Issue_2_1} />
        <p>
          In the Boards view, a New Issue can be created by clicking the [+]
          icon in the Open column of the board, highlighted here in bright
          green.
        </p>
        <img alt={'gitlab issues 3'} src={i338_Issue_3_1} />
        <p>
          The New Issue form in the Boards view is smaller, but contains the
          same main elements as the full form. Its elements are discussed in the
          next section.
        </p>
        <img alt={'gitlab issues 4'} src={i338_Issue_4_1} />
        <p>
          From an open issue in your project, click the New Issue button
          (highlighted bright green below) to create a new issue in the same
          project. This button will take you to the full-page New Issue form,
          shown above in the List section.
        </p>
        <img alt={'gitlab issues 5'} src={i338_Issue_5_1} />
      </Example>

      <h3>Elements of the New Issue Form</h3>

      <p>
        When you're creating a new issue, these are the elements to be
        completed:
      </p>

      <Example>
        <ul>
          <li>
            <b>Title</b> - a short summary that describes the issue
          </li>
          <li>
            <b>Description</b> - a complete description of the issue
          </li>
          <li>
            <b>Make Issue confidential</b> - Most issues should not be
            confidential but available to the whole community.
          </li>
          <li>
            <b>Assignee</b> - Issues should have assignees. If you don't know
            whom to assign the issue to, please assign it to yourself and ask
            for guidance when you next meet with your group, or reach out to the
            community in chat.
          </li>
          <li>
            <b>Due date</b> - The date the task should be completed by.
          </li>
          <li>
            <b>Milestone</b> - This is optional and not used by some groups.
          </li>
          <li>
            <b>Labels</b> - The <Link to={'#labels'}>use of labels</Link> is
            discussed in further detail on a separate page.
          </li>
        </ul>
      </Example>

      <h4>Issue Duplication</h4>

      <p>
        To prevent duplication of issues for the same topic, GitLab searches for
        similar issues when new issues are being created.
      </p>

      <p>
        When typing in the title in the New Issue page, GitLab searches titles
        and descriptions across all issues the user has access to in the current
        project. Up to five similar issues, sorted by most recently updated, are
        displayed below the title box.
      </p>

      <h3>Be Notified about an Issue</h3>

      <p>
        To keep track of an Issue that you are interested in, click on the issue
        in either List or Boards view. You will then see the screen below. Turn
        on notifications by toggling the switch at the bottom of the right
        sidebar (highlighted here in bright green). You will automatically
        receive notifications about issues that you create.
      </p>

      <Example>
        <img alt={'gitlab issues 6'} src={i338_Issue_6_1} />
      </Example>
    </Section>

    <Section type={'dark'}>
    <div className={'center'}>
      <p><strong>Next section -></strong> <a href="/gitlab">GitLab Project Management</a></p>
      </div>
    </Section>
  </Layout>
);

export default CreatingInGitlab;
