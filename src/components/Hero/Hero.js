import React from 'react';
import PropTypes from 'prop-types';
import styles from './Hero.module.css';

const Hero = ({ heroIntro, heroPrimary }) => (
  <div className={styles.hero}>
    <span className={styles.heroIntro}>
      {heroIntro}
      <br />
    </span>
    <span className={styles.heroPrimary}>{heroPrimary}</span>
  </div>
);

Hero.propTypes = {
  heroIntro: PropTypes.string,
  heroPrimary: PropTypes.string,
};

Hero.defaultProps = {
  heroIntro: '',
  heroPrimary: '',
};

export default Hero;
