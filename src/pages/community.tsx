import React from 'react';
import { Helmet } from 'react-helmet';
import { PageProps } from 'gatsby';

import Layout from '../components/Layout/Layout';
import Hero from '../components/Hero/Hero';
import Section from '../components/Section/Section';
import TableOfContents from '../components/TableOfContents/TableOfContents';
import Iframe from '../components/Iframe/Iframe';

const Community: React.FC<PageProps> = () => (
  <Layout>
    <Helmet>
      <title>Community - IEEE SA OPEN</title>
    </Helmet>
    <Hero heroIntro={'IEEE SA OPEN'} heroPrimary={'Community'} />
    <TableOfContents
      items={[
        {
          text: 'Community Advisory Group',
          link: '#CAG',
        },
        {
          text: 'Marketing Advisory Group',
          link: '#MAG',
        },
        {
          text: 'Technical Advisory Group',
          link: '#TAG',
        },
        {
          text: 'IEEE SA Open Source Committee',
          link: '#OSCOM',
        },
      ]}
    />
    <Section type={'light'}>
        <p>Need more help getting started? Join us for IEEE SA OPEN Office Hours. 
          During our office hours you'll have live access to community members and 
          facilitators ready to help with any issues you have.
        </p>
        <p>Our office is open from 11am to noon Central every Thursday, and everyone 
          is welcome to join!
        </p>
        <p>
            All times on calendar below are shown in the the user's computer time zone.
        </p>
      <div className={'center'}>
        <Iframe
          src={
            'https://calendar.google.com/calendar/embed?src=c_8tt9afis1as7hqnj3rke6ut0j4%40group.calendar.google.com&ctz=local'
          }
          width={'800'}
          height={'600'}
        />
      </div>
    </Section>
    <Section type={'dark'}>
      <a className={'anchor'} id={'CAG'} />
      <h1>Community Advisory Group</h1>
      <h2>(CAG)</h2>
      <h3>Purpose</h3>
      <p>
        The Community Advisory Group (CAG) defines and promotes best practices
        for participation in open source projects, especially within the IEEE SA
        OPEN framework. We consult with members of communities, leaders in the
        broader open source world, and existing documentation, while actively
        involving people who are diverse in race, nationality, gender, and
        ability.
      </p>
      <h3>Meetings</h3>
      <p>
        Community Advisory Group{' '}
        <a
          href={'https://opensource.ieee.org/community-advisory-group/meetings/-/blob/master/README.md#community-advisory-group-meetings'}
        >
          meetings are announced
        </a>{' '}
        and{' '}
        <a
          href={
             'https://opensource.ieee.org/community-advisory-group/meetings/-/tree/master/#meeting-notes'
          }
        >
          minutes
        </a>{' '}
        are recorded on the Community Advisory Group GitLab.
      </p>
      <h3>Communication</h3>
      <p>
        The Community Advisory Group maintains a{'  '}
        <a
          href={'https://opensource-connect.ieee.org/community-advisory-group/'}
        >
          Mattermost channel
        </a>{' '}
        . Anyone is welcome to subscribe. If you have a question about the IEEE
        SA OPEN CAG, please send email to{' '}
        <a href="mailto:opensource@ieee.org?subject=CAG%20questions%20from%20Community%20page%20">
          opensource@ieee.org
        </a> .
      </p>
      <h3>Subgroups</h3>
      <p>The Community Advisory Group currently hosts a number of subgroups to encourage cooperation and outreach as our community works to make Open Source stronger. These include  <a
          href={'https://opensource.ieee.org/community-advisory-group/documentation-curation/about/-/blob/main/README.md'}
          >
        Documentation and Curation</a>, {' '}
        working to organize our volunteers work into a Community Handbook and to create a toolkit for other communities to make their own handbooks;{' '} 
      <a
          href={'https://opensource.ieee.org/community-advisory-group/education/about/-/blob/main/README.md'}>
        Education </a>, {' '}
        figuring out how to best support students and educators from PreK-Post Grad with Open Source tools; and {' '}
      <a
          href={'https://opensource.ieee.org/community-advisory-group/community-badging/about/-/blob/main/README.md'}>
       Community Badging</a>, {' '}
        working to recognize milestones in volunteer contributions and project maturity.
      </p>
    </Section>
    <Section type={'light'}>
      <a className={'anchor'} id={'MAG'} />
      <h1>Marketing Advisory Group</h1>
      <h2>(MAG)</h2>
      <h3>Purpose</h3>
      <p>
        The Marketing Advisory Group (MAG) defines and promotes best practices
        for marketing in open source projects, especially within the IEEE SA
        OPEN framework. We consult with members of marketing communities,
        leaders in the broader open source world, and existing documentation,
        while actively involving people who are diverse in race, nationality,
        gender, and ability. While the IEEE SA OPEN community supports itself
        through the mentorship and the assistance of our volunteers, the IEEE SA
        Marketing team shares their best practices with the community to act as
        an initial resource.
      </p>
      <p>
        The Marketing Advisory Group allows all participants to share and
        contribute in the larger Open Source community, increasing role
        diversity and providing opportunities and processes to quantify
        contributions that are often overlooked.
      </p>
      <p>
        Due to branding and trademarking issues, Official IEEE SA OPEN projects
        must follow the guidelines set by the IEEE SA Marketing team. Please
        contact them about approval.
      </p>
      <h3>Meetings</h3>
      <p>
        Marketing Advisory Group{'  '}
        <a
          href={'https://opensource.ieee.org/marketing-advisory-group/meetings/-/blob/master/README.md#marketing-advisory-group-meetings'}
        >
          meetings are announced
        </a>{' '}
        and{' '}
        <a
          href={
            'https://opensource.ieee.org/marketing-advisory-group/meetings/-/tree/master/#meeting-notes'
          }
        >
          minutes
        </a>{' '}
        are recorded on IEEE SA OPEN GitLab.
      </p>
      <h3>Communication</h3>
      <p>
        The Marketing Advisory Group maintains a{'  '}
        <a
          href={'https://opensource-connect.ieee.org/marketing-advisory-group/'}
        >
          Mattermost channel
        </a>
        . Anyone is welcome to subscribe. If you have a question about the IEEE
        SA OPEN MAG, please feel free to send email to{' '}
        <a href="mailto:opensource@ieee.org?subject=MAG%20questions%20from%20Community%20page%20">
          opensource@ieee.org
        </a>
        .
      </p>
      <h3>Subgroups</h3>
      <p>
        The Marketing Advisory Group on GitLab currently hosts subgroups
        encourage cooperation and mentorship as our community works to make Open
        Source stronger. Two examples of these are {' '}
        <a
          href={
            'https://opensource.ieee.org/marketing-advisory-group/OSINT/about-me/-/blob/main/README.md'
          }
        >
          OSINT (Open Source Intelligence)
        </a>{' '}
        which supports IEEE SA OPEN groups by providing relevant OSINT data, reviews, methodologies, resources, and best practices, and the unambiguous{' '}
        <a
          href={
            'https://opensource.ieee.org/marketing-advisory-group/how-to-market-open-source/about/-/blob/main/README.md'
          }
        >
          How to Market Open Source
        </a>{' '}
        group. Everyone is welcome to join these and learn from one another.
      </p>
    </Section>
    <Section type={'dark'}>
      <a className={'anchor'} id={'TAG'} />
      <h1>Technical Advisory Group</h1>
      <h2>(TAG)</h2>
      <h3>Purpose</h3>
      <p>
        The Technical Advisory Group (TAG) creates collateral, processes, and
        recommendations for the IEEE SA OPEN Platform and incoming groups. We
        consult with members of communities, leaders in the broader open source
        world, and existing documentation, while actively involving people who
        are diverse in race, nationality, gender, and ability.
      </p>
      <h3>Meetings</h3>
      <p>
        Technical Advisory Group{' '}
        <a
          href={'https://opensource.ieee.org/technical-advisory-group/meetings/-/blob/master/Readme.md#technical-advisory-group-meetings'}
        >
          meetings are announced
        </a>{' '}
        and{' '}
        <a
          href={
            'https://opensource.ieee.org/technical-advisory-group/meetings/-/tree/master#meeting-notes'
          }
        >
          minutes
        </a>{' '}
        are recorded on the IEEE SA OPEN GitLab.
      </p>
      <h3>Communication</h3>
      <p>
        The Technical Advisory Group maintains a{' '}
        <a
          href={'https://opensource-connect.ieee.org/technical-advisory-group/'}
        >
          Mattermost channel
        </a>
        . Anyone is welcome to subscribe. If you have a question about the IEEE
        SA OPEN Technical Advisory Group, please send email to{' '}
        <a href="mailto:opensource@ieee.org?subject=TAG%20questions%20from%20Community%20page%20">
          opensource@ieee.org
        </a>
        .
      </p>
      <h3>Subgroups</h3>
      <p>
        The Technical Advisory Group on GitLab currently hosts a number of
        subgroups to facilitate the development of our community&apos;s
        platform, encourage cooperation and mentorship among members, and
        document best practices in key technical areas. These include a group
        focusing on{' '}
        <a
          href={
            'https://opensource.ieee.org/technical-advisory-group/codereviews'
          }
        >
          Code Review
        </a>
        , a group whose primary focus is{' '}
        <a
          href={
            'https://opensource.ieee.org/technical-advisory-group/architecturegroup'
          }
        >
          Architecture
        </a>
        , and a group offering guidance regarding{' '}
        <a
          href={
            'https://opensource.ieee.org/technical-advisory-group/production-ready-applications'
          }
        >
          Production Ready Applications
        </a>
        . Everyone is welcome to join these and learn from one another.
      </p>
    </Section>
    <Section type={'light'}>
      <a className={'anchor'} id={'OSCOM'} />
      <h1>IEEE SA Open Source Committee</h1>
      <h2>(OSCom)</h2>
      <h3>Purpose</h3>
      <p>
        The IEEE SA Open Source Committee (OSCom) is authorized by the <a href="https://standards.ieee.org/about/index.html" target="_blank">IEEE
        Standards Association</a> (IEEE SA) <a href="https://standards.ieee.org/about/bog/index.html" target="_blank">Board of Governors</a> (BOG) to provide
        guidance, oversight, and life-cycle management support for IEEE Open
        Source Projects. OSCom operates in an open, transparent, and
        community-driven fashion in cooperation with IEEE SA and IEEE SA OPEN
        staff, who establish detailed policies and procedures for operations and
        maintenance of the IEEE Open Source Platform and IEEE Open Source
        Projects.
      </p>
      <p>
        OSCom is intended to support the IEEE SA OPEN Platform and IEEE Open
        Source Projects, as well as to provide governance. Governance functions
        include approving the use of the IEEE SA OPEN Platform for projects that
        wish to have an IEEE imprimatur (and for all projects that are
        incorporated into IEEE Standards), and reviewing and approving the
        Maintainers Manual, terms of use, and other policies and procedures.
        Support functions include advising the IEEE SA OPEN Staff, supporting
        the IEEE SA OPEN offerings and coordinating with the <a href="https://www.ieee.org/about/volunteers/tab.html" target="_blank">IEEE Technical
        Advisory Board</a> (TAB).
      </p>
      <h3>Policies, Procedures, and Guides</h3>
      <p>
        Policies and procedures are set forth in the{' '}
        <a
          href={
            'https://standards.ieee.org/content/dam/ieee-standards/standards/web/documents/other/OSCOM_Operations_Manual.pdf'
          }
        >
          OSCom Operations Manual
        </a>{' '}
        and the IEEE SA Open{' '}
        <a href={'https://opensource.ieee.org/oscom/maintain'}>
          Maintainers Manual
        </a>
        .
      </p>
      <h3>Meetings</h3>
      <p>
        OSCom{' '}
        <a
          href={
            'https://opensource.ieee.org/oscom/meetings/-/blob/master/README.md'
          }
        >
          meetings are announced
        </a>{' '}
        and{' '}
        <a href={'https://opensource.ieee.org/oscom/meetings/-/blob/master/meeting-minutes.md'}>
          minutes
        </a>{' '}
        are recorded in the OSCom GitLab Group.
      </p>
      <h3>Communication</h3>
      <p>
        OSCom maintains a{' '}
        <a
          href={
            'https://opensource-connect.ieee.org/oscom/channels/town-square'
          }
        >
          Mattermost channel
        </a>
        . Anyone is welcome to subscribe. If you have a question about OSCom,
        please send email to{' '}
        <a href="mailto:opensource@ieee.org?subject=OSCOM%20questions%20from%20Community%20page%20">
          opensource@ieee.org
        </a>
        .
      </p>
      <h3>Obtaining Approvals</h3>
      <p>
        If OSCom approval is required, an{' '}
        <a
          href={
            'https://opensource.ieee.org/oscom/official-project-requests/-/blob/master/.gitlab/issue_templates/official_project_proposal.md'
          }
        >
          official request
        </a>{' '}
        can be made on GitLab.
      </p>
      <h3>Membership</h3>
      <p>
      To view members of OScom and learn more about their roles visit the {''}
        <a href="/oscom-committee">OSCom membership page.</a>
      </p>
    </Section>
  </Layout>
);

export default Community;
