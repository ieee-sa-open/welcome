import React from 'react';
import { Helmet } from 'react-helmet';
import { PageProps } from 'gatsby';

import Layout from '../components/Layout/Layout';
import Hero from '../components/Hero/Hero';
import Section from '../components/Section/Section';
import TableOfContents from "../components/TableOfContents/TableOfContents";

const MAGWorkshop: React.FC<PageProps> = () => (
  <Layout>
    <Helmet>
      <title>MAGic in the Making: Creating Open Source Marketing Resources</title>
    </Helmet>
  <Hero heroIntro={'SA OPEN'} heroPrimary={'Workshop Events'} />
    <TableOfContents
      items={[
    {
          text: 'Workshop Notes',
          link: '#notes',
    },
	  {
	      text: 'Breakout Session 1',
	      link: '#breakout1',
    },
    {
        text: 'Breakout Session 2',
        link: '#breakout2',
    },
	    ]}
    />
    <Section type={'light'}>
      <h6>Marketing Advisory Group Workshop<br />
       MAGic in the Making: Creating Open Source Marketing Resources<br />
       May 4th, 2021</h6>

       <div className={'center'}>
       <p>
       <iframe width="560" height="315" src="https://www.youtube.com/embed/9UlbRxikXf0" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
        </p>
      </div>
      <p>
          The Open Source Marketing Advisory groups on the IEEE SA OPEN platform are creating resources for humanitarian projects and groups. The purpose of this workshop is to strategize how to use marketing resources to foster and improve project maturity, increase marketing involvement across industries within the Open Source community, and create a solid foundation for involvement within the overall open source community as it regards to marketing. All marketing professionals from diverse backgrounds, diverse roles, and diverse organizational structures are encouraged to participate.
      </p>
       </Section>
       <Section type={'dark'}>
      <a className={'anchor'} id={'notes'} />
      <h6>Workshop Notes:</h6>
    <div className={'center'}>
      <p>
      <a href="https://opensource.ieee.org/workshops/maymagworkshop/202105-workshop-notes-and-recordings/-/blob/main/README.md" target='_blank'>Workshop Notes & Recordings</a>.
      </p>
      </div>
    </Section>
    <Section type={'light'}>
      <a className={'anchor'} id={'breakout1'} />
      <h6>Breakout Session 1:</h6>
      <p>
      <b>11:40 AM</b> <br />
      <ul>
        <li><b>Breakout Room 1:</b> <a href="https://opensource.ieee.org/workshops/maymagworkshop/202105-workshop-notes-and-recordings/-/blob/main/Notes/session1-osinta-notes.md" target='_blank'>Open Source Intelligence (OSINT) in the World of Marketing Defining how OSINT can promote the future of marketing in open source</a></li>
        <li><b>Breakout Room 2:</b> <a href="https://opensource.ieee.org/workshops/maymagworkshop/202105-workshop-notes-and-recordings/-/blob/main/Notes/mag-session1-badginga-notes.md" target='_blank'>Marketing Metrics and Badging Initiatives Defining how badging initiatives can help promote marketing goals</a></li>
      </ul>
      </p>
    </Section>
    <Section type={'dark'}>
      <a className={'anchor'} id={'breakout2'} />
      <h6>Breakout Session 2:</h6>
      <p>
      <b>1:05 PM</b> <br />
      <ul>
        <li><b>Breakout Room 1:</b> <a href="https://opensource.ieee.org/workshops/maymagworkshop/202105-workshop-notes-and-recordings/-/blob/main/Notes/session2-osintb-notes.md" target='_blank'>Open Source Intelligence (OSINT) in the World of Marketing OSINT applications in open source marketing</a></li>
        <li><b>Breakout Room 2:</b> <a href="https://opensource.ieee.org/workshops/maymagworkshop/202105-workshop-notes-and-recordings/-/blob/main/Notes/mag-session2-badgingb-notes.md" target='_blank'>Marketing Metrics and Badging Initiatives Defining the measure of success in the marketing open source community</a></li>
        <li><b>Breakout Room 3:</b> <a href="https://opensource.ieee.org/workshops/maymagworkshop/202105-workshop-notes-and-recordings/-/blob/main/Notes/session2-tag-notes.md" target='_blank'>Open Source Features and Tools for Marketing Defining features and tools the marketing professionals need in open source</a></li>
      </ul>
      </p>
      </Section>
   </Layout>
);

export default MAGWorkshop;

