import React from 'react';
import { Helmet } from 'react-helmet';

import Layout from '../components/Layout/Layout';
import Hero from '../components/Hero/Hero';
import Section from '../components/Section/Section';


const GettingStartedToC: React.FC<PageProps> = () => (
  <Layout>
    <Helmet>
      <title>IEEE SA OPEN</title>
    </Helmet>
    <Hero heroIntro={'IEEE SA OPEN'} heroPrimary={'Getting Started'} /> 
    <Section type={'light'}>
    <h1>Table of Contents</h1>
    <h3>Participation</h3>
    <p>
        <ul>
        <li><a href="/participation/#sso">Creating Your IEEE SA OPEN Single Sign On</a></li>
        <li><a href="/participation/#signcla">How To A Sign Contributor License Agreement</a></li>
        <li><a href="/participation/#notifications">Notifications In GitLab</a></li>
        </ul>
    </p>
      <h3>Creating in GitLab</h3>
      <p>
        <ul>
        <li><a href="/creating-in-gitlab/#group">Creating A Group</a></li>
        <li><a href="/creating-in-gitlab/#project">Creating A Project</a></li>
        <li><a href="/creating-in-gitlab/#issue">Creating An Issue</a></li>
        </ul>
    </p>

      <h3>Gitlab Project Management</h3>
    <p>
        <ul>
        <li><a href="/gitlab/#tutorial">Project Management Basics</a></li>
        <li><a href="/gitlab/#labels">Using Labels</a></li>
        <li><a href="/gitlab/#gitlab">More On GitLab</a></li>
        </ul>
    </p>
      <h3>Mattermost</h3>
      <p>
        <ul>
        <li><a href="/mattermost/#mmbasics">Mattermost Basics</a></li>
        <li><a href="/mattermost/#mattermost">More On Mattermost</a></li>
        </ul>
    </p>
      <h3>Big Blue Button</h3>
      <p>
        <ul>
        <li><a href="/bigbluebutton/#prep">Participant Prep</a></li>
        <li><a href="/bigbluebutton/#faq">BigBlueButton FAQ</a></li>
        <li><a href="/bigbluebutton/#facilitator">Facilitator Guide</a></li>
       </ul>
    </p>
     
    </Section>
  </Layout>
);

export default GettingStartedToC;

