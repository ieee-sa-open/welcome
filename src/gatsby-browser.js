/**
 * Implement Gatsby's Browser APIs in this file.
 *
 * See: https://www.gatsbyjs.com/docs/browser-apis/
 */

exports.onRouteUpdate = ({ location, prevLocation }) => {
  window.addEventListener('load', function () {
    setTimeout(function () {
      if (window?.cookieconsent?.initialise) {
        window.cookieconsent.initialise(json);
      }
    }, 1500);
  });
};
