import React from 'react';
import { Helmet } from 'react-helmet';
import { PageProps, Link } from 'gatsby';

import Layout from '../components/Layout/Layout';
import Hero from '../components/Hero/Hero';
import Section from '../components/Section/Section';
import TableOfContents from '../components/TableOfContents/TableOfContents';

const Projects: React.FC<PageProps> = () => (
  <Layout>
    <Helmet>
      <title>Projects - IEEE SA OPEN</title>
    </Helmet>
    <Hero heroIntro={'IEEE SA Open'} heroPrimary={'Projects'} />
    <TableOfContents
      items={[
        {
          text: 'Types of projects',
          link: '#types-of-projects',
        },
        {
          text: 'Prerequisites',
          link: '#prerequisites',
        },
        {
          text: 'Setup a technical steering committee',
          link: '#technical-steering-committee',
        },
        {
          text: 'Choose a project name & location',
          link: '#choose-project-name',
        },
        {
          text: 'Project tools',
          link: '#project-tools',
        },
      ]}
    />
    <Section type={'light'}>
      <h1>Quick-start guide for starting a project</h1>
      <p>
        This page helps you do the basics you need to start a project on IEEE SA
        OPEN. There are many reasons for creating a project:
      </p>
      <ul>
        <li>
          You have an open source project, or a software project you want to
          convert to open source, and are attracted by the tools and processes
          provided by IEEE SA OPEN.
        </li>
        <li>
          You are working on an IEEE standard, or a project you hope to get
          accepted as an IEEE standard, and want it to be open source.
        </li>
        <li>
          You are a programmer or student with an idea for a new software
          project, and want to benefit from the guidance IEEE SA OPEN provides
          on modern software engineering and open source methods.
        </li>
      </ul>
      <p>
        This quick-start guide complements the official{' '}
        <a href={'https://opensource.ieee.org/community/manual/-/wikis/home'}>
          maintainers manual
        </a>{' '}
        provided by IEEE SA OPEN.
      </p>
    </Section>
    <Section type={'dark'}>
      <a className={'anchor'} id={'types-of-projects'} />
      <h3>Types of Projects</h3>
      <p>
        It's useful to discuss different needs for three different levels of
        project.
      </p>
      <ol>
        <li>
          Personal project: this is the smallest, simplest type, run by one
          person with help from others. Imposing a lot of infrastructure would
          just be a waste of time. If successful, this kind of project can
          proceed to the next stage, the large project.
        </li>
        <li>
          Large project: this has a couple dozen or more contributors. It needs
          more governance and infrastructure than a personal project. If the
          leaders want to propose an IEEE standard or get other kinds of help
          from IEEE, they can proceed to the next stage.
        </li>
        <li>
          Official project: this project demonstrates that it meets strict
          reqirements for governance, legal compliance, code quality, and
          documentation defined by IEEE SA OPEN. Project leaders present the
          project for approval to the board that runs IEEE SA OPEN, the Open
          Source Committee (OSCom). Official projects receive a great deal of
          attention, guidance, and advice from IEEE. They get legal advice to
          avoid problems with anti-trust laws and other regulations. If the
          project wants to establish an IEEE standard, the IEEE guides them
          along that process.
        </li>
      </ol>
      <p>
        Some of the guidance in this document apply to all three kinds of
        project, and some guidance applies to higher levels. As you move up to
        higher levels, please consult the{' '}
        <a href={'https://opensource.ieee.org/community/manual/-/wikis/home'}>
          maintainers manual
        </a>
        . In general, the recommendations in the maintainers; manual are
        valuable for all projects. They are certainly required for official
        projects. On the other hand, some rules may be overly complex and
        unnecessary for personal projects.
      </p>
    </Section>
    <Section type={'light'}>
      <a className={'anchor'} id={'prerequisites'} />
      <h3>Prerequisites</h3>
      <p>Every project should do the following before joining IEEE SA OPEN.</p>
      <ul>
        <li>Form a team</li>
        <li>
          Make sure existing code or contributions are owned or licensed
          properly
        </li>
        <li>Write essential documents</li>
      </ul>
      <h4>Form a team</h4>
      <p>
        A personal project might start with just one person; larger projects
        probably already have multiple contributors and hopefully have rules and
        processes for managing contributions. But as open source projects, they
        all should invite a diverse range of contributors and define clear
        management rules. Consider such issues as:
      </p>
      <ul>
        <li>How are decisions about new features made?</li>
        <li>
          Who gets to approve a code change, and what criteria do they use?
        </li>
        <li>How often are meetings held, and who attends?</li>
        <li>
          How are group memnbers promoted to higher levels of responsibility?
        </li>
        <li>What is the succession plan for providing new leadership?</li>
      </ul>
      <p>
        Choose a project leader, and set up a technical steering committee to
        handle technical decisions such as what tools and languages to use, what
        coding standards to enforce, etc.
      </p>
      <p>
        More information can be found in sectioon 2.1, "Governance principles"
        in the IEEE SA OPEN{' '}
        <a href={'https://opensource.ieee.org/community/manual/-/wikis/home'}>
          maintainers manual
        </a>
        .
      </p>
      <h4>
        Make sure existing code or contributions are owned or licensed properly
      </h4>
      <p>
        Every contribution to your project--particularly code, but also other
        materials such as documentation--should be under an open source licence
        recognized by IEEE SA OPEN. This requires a Contributor License
        Agreement (CLA). If you haven't been asking people to ensure that their
        contributions are open, you must to each person or company who
        contributed and ask them to sign your agreement.
      </p>
      <p>
        Too many projects are lax about demanding CLAs. When they want to go
        public or become open source, they found that they're using code they
        don't own. They may discover that the owner of the code didn't intend it
        to be used under the license chosen by the project, and may have plans
        to commercialize it. In any case, lacking a CLA, the project has to
        strip out the code. If it has been in the project for a while and others
        have built on top it, it could derail the project.
      </p>
      <p>
        There are two ways to get contributors to conform. The best way is for
        them to give ownership to the project. This does not limit anyone's
        rights in an open source project, because everyone is free to use the
        code under the open source license. The alternative is to let the
        contributor keep ownership, but to put the code under an open license.
        This is sub-optimal for many reasons, so keep things simple and require
        a CLA. Sample CLAs approved by IEEE SA OPEN are{' '}
        <a href={'https://opensource.ieee.org/community/cla'}>
          on their web site
        </a>
        .
      </p>
      <h4>Write essential documents</h4>
      <p>
        This section lists documents that are useful for open source projects.
        Some should be written before creating an IEE SA OPEN project, while
        others can wait till later. Similarly, your initial documents can be
        short and you can add detail to them later. At the start, you may need
        only the README.md and the Contributor License Agreement.
      </p>
      <h4>README.md</h4>
      <p>
        This is a general place for new people to go. You should highlight the
        file on your site after you set up your project. You can put plain text
        there, but it also accepts a very simple can kind of formatting called
        Markdown that allows more attractive headings and lists. A short{' '}
        <a
          href={
            'https://medium.com/@itsjzt/beginner-guide-to-markdown-229adce30074'
          }
        >
          guide introduced Markdown
        </a>
        . A table of contents is useful for this document as it gets longer.
      </p>
      <p>
        Think of the basic things a new member of your group will ask when
        joining a project, and put them in this file. Examples include:
      </p>
      <ul>
        <li>Project description.</li>
        <li>
          Team lead, committers, and other key leaders, with their contact
          information, time zones, and available times. We discourage th
          inclusion of email addresses for reasons of privacy. Instead include
          their chat handles, once they are all set up on opensource.ieee.org.
        </li>
        <li>
          Pointers to important documents that enforce good community behavior,
          such as the{' '}
          <a
            href={
              'https://www.ieee.org/content/dam/ieee-org/ieee/web/org/about/ieee_code_of_conduct.pdf'
            }
          >
            IEEE Code of Conduct
          </a>
          .
        </li>
        <li>Keywords for search purposes.</li>
        <li>How to sign up for the announcement list.</li>
        <li>
          Location of discussion forum, chat, and bug or issue tracker, once you
          are set up on opensource.ieee.org.
        </li>
        <li>Conventions for submitting a bug report.</li>
        <li>Any earlier names and code names for the project.</li>
      </ul>
      <h4>Contributor License Agreement</h4>
      <p>
        This was described in the "Prerequisites" section. Do not let people
        become members until they sign the agreement.
      </p>
      <h4>CONTRIBUTING.md</h4>
      <p>
        This contains guidelines for contributors. It can grow quite large,
        because a well-run project has to enforce rules on how to make features
        requests, how to code, how to run tests, and more. A table of contents
        is useful for this document.
      </p>
      <p>
        Start by pointing to the README.md file for community guidelines and
        information about leaders and committers. When time permits, add:
      </p>
      <ul>
        <li>Code conventions</li>
        <li>Testing conventions</li>
        <li>Branching conventions</li>
        <li>Conventions for commit messages</li>
        <li>Steps for creating good pull requests</li>
        <li>How to submit feature requests</li>
        <li>How to submit security issue reports</li>
        <li>How to write documentation</li>
        <li>Project dependencies</li>
        <li>Build schedule</li>
        <li>Sprint schedule</li>
        <li>Road map</li>
        <li>Helpful links, information, and documentation</li>
        <li>When the repositories will be closed to contributions</li>
      </ul>
      <h4>HELPWANTED.md</h4>
      <p>
        This file isn't found on every project, but it can be very useful in
        recruiting people to important tasks. New members can't be expected to
        scrutinize your bug database or think up a feature to work on. In this
        file, you can list your project's greatest needs. These aren't limited
        to coding--they can include advocacy, marketing, documentation,
        fund-raising, or anything else to benefit the project.
      </p>
      <p>
        You can also set a forum where requests and offers of help are posted,
        and link to that forum in this file.
      </p>
      <h4>GETTINGSTARTED.md</h4>
      <p>
        This material helps a potential contributor set up tools and begin
        coding. The material can be in a separate file is it's complex, or be
        folded into the README.md or CONTRIBUTING.md file. Writing this material
        is a good project to ask an intern or other new user to do. Feedback
        from contributors can improve and flesh out the information.
      </p>
    </Section>
    <Section type={'dark'}>
      <a className={'anchor'} id={'technical-steering-committee'} />
      <h3>Set up a technical steering committee</h3>
      <p>
        The technical steering committee (TSC) brings together technically
        sophisticated members of the project and embodies the vision for the
        project. The TSC makes the most important technical decisions regarding
        the project: which markets or users it is meant for, what features and
        interfaces to develop, what architecture to use, what language and tools
        to employ, and more.
      </p>
      <p>
        All technical decisions require an understanding of current practices as
        well as a deep sensitivity to particular needs of the project. Technical
        leaders should keep researching current innovations in tools, languages,
        and architecture while avoiding the temptation to jump into something
        because it's a fad. The question is: does the new technology meet the
        current and future needs of the project?
      </p>
      <p>Activities of a TSC include:</p>
      <ul>
        <li>Defining goals</li>
        <li>Defining use cases and markets</li>
        <li>Project architecture</li>
        <li>Choosing the toolchain</li>
      </ul>
    </Section>
    <Section type={'light'}>
      <a className={'anchor'} id={'choose-project-name'} />
      <h3>Choose a project name and location</h3>
      <p>
        Official projects start with the prefix "ieee-". Other guidelines for
        choosing a name are:
      </p>
      <ul>
        <li>Avoid trademarks</li>
        <li>Don't use flippant or overly difficult namess</li>
      </ul>
    </Section>
    <Section type={'dark'}>
      <a className={'anchor'} id={'project-tools'} />
      <h3>Project Tools</h3>
      <p>
        The following tools are the basis of modern software engineering, and
        are needed by almost every project.
      </p>
      <h4>Discussion forum</h4>
      <p>
        Set up a forum that people can reach through email or through a web
        site--there are many tools that offer such forums.
      </p>
      <p>
        Forums represent the main way project members communicate, especially
        considering that geographical dispersion and time zone differences make
        it difficult to hold regular meetings. Although a casual glance at a
        forum shows a few simple purposs, such as discussing features and
        offering help, forums actually serve several critical functions:
      </p>
      <ul>
        <li>
          <b>Education.</b> New project members are initiated and guided here,
          and anyone can ask for and offer help.
        </li>
        <li>
          <b>Deliberation.</b> All sorts of community decisions are discussed
          (sometimes too much) on forums, such as new features, user interface
          elements, and project direction.
        </li>
        <li>
          <b>Documentation.</b> The answers provided to user questions are
          preserved so that others can find them later. The arguments and
          possibilites floated that make up a decision are also preserved for
          historical purposes. A good search feature is important to enable
          these benefits.
        </li>
        <li>
          <b>Community building.</b> People get to know and trust each other
          through discussion.
        </li>
        <li>
          <b>Inclusion.</b> Every project member can watch and participate in
          decisions.
        </li>
      </ul>
      <p>
        Because forums fulfill so many functions, a project often creates
        several different forums for different functions: newbie questions,
        technical design decisions, project governance, etc.
      </p>
      <p>
        Community inclusion and trust require all important discussions to be
        carried on in the forums, where all members can see them. It's fine for
        a couple people or a small group to talk separately, in order to iron
        out personal issues or create a proposal to show others. Similarly, some
        sensitive issues such as policies regarding paid staff or legal issues
        may require private discussions. But all decisions affecting technical
        issues and related activities such as marketing should be done made on
        the list.
      </p>
      <h4>Chat channels</h4>
      <p>
        A chat channel is usually a shared text messaging tool, such as Internet
        Relay Chat (IRC), where members hang out and educate each other. Because
        it involves rapid interaction in real-time, it can be useful for
        education and to handle personal issues. But it should not substitute
        for group discussion on the forums. Furthermore, the code of conduct
        should be enforced so people don't feel intimidated.
      </p>
      <p>
        As with other tools, GitLab provides a chat function. GitLab has
        integrated the open source Mattermost chat service, which resembles the
        Slack service popular in many businesses. To set up a chat channel, when
        you are logged in to opensource.ieee.com, do the following.
      </p>
      <ol>
        <li>
          Visit the{' '}
          <a href={'https://opensource-connect.ieee.org/'}>
            https://opensource-connect.ieee.org/
          </a>{' '}
          site.
        </li>
        <li>
          Click the Gitlab button on the right. If you aren't logged in to
          opensource.ieee.com, you must log in here to continue.
        </li>
        <li>On the next screen, choose "Create a team".</li>
        <li>
          On the next screen, choose a name with 2 to 15 characters and press
          the Next button.
        </li>
        <li>
          The next screen shows the URL that will be used, by you and other
          people you invite, to send and receive messages. For instance, if the
          name you chose was "Discuss guides", the URL is
          *https://opensource-connect.ieee.org/discuss-guides*. Change the
          default team URL if you want, and press Finish.
        </li>
        <li>
          Visit the team URL to invite people, send messages, and do other
          activities. chats are automatically created for each team: "Town
          Square" and "Off-Topic".For instance, you can chat on the "Discuss
          guides" channel by visiting
          *https://opensource-connect.ieee.org/discuss-guides/channels/town-square*.
        </li>
      </ol>
      <h4>Bug tracker</h4>
      <p>
        GitLab has built-in bug tracking, like many other common project tools.
      </p>
      <p>
        It's crucial to make sure that bug reports are answered. First of all,
        bugs make the product less useful, and may introduce dangers. Second,
        you want to show users that your project is responsive. Fixing bugs is
        one of the most direct ways to do this.
      </p>
      <p>
        Of course, many bug reports can be traced to user errors or a
        misunderstanding of the product. It is good community relations to
        respond as fast as possible, offering polite and helpful information.
      </p>
      <p>
        Most projects designate people to evaluate incoming reports, with the
        goal of providing an initial response to every report within days. Bug
        tracking systems such as Jira let you classify the severity of each
        error and track the progress in solving it. Bug reports that represent
        real bugs may be assigned to an individual, or simply publicized in the
        bug tracking system. All community members who are able to fix bugs are
        encouraged to choose bugs and fix them. In fact, bug-fixing is the
        classic starting point for aspiring new developers.
      </p>
      <h4>Version control</h4>
      <p>
        Any text that people work on and change should be stored in a system
        that makes changes easy to track. This is called *source control*, and
        ensures that developers can:
      </p>
      <ul>
        <li>
          See what was done over time, who made each change, and the differences
          between files at different stages (versions)
        </li>
        <li>
          Assign version numbers and tags or labels (such as "stable" or
          "released")
        </li>
        <li>Revert to earlier versions if necessary</li>
        <li>
          Create separate versions (branches) for separate groups of developers
          to work on in parallel, and merge their work when it's ready to be
          integrated
        </li>
      </ul>
      <p>
        GitLab is based on the open source Git tool, probably the most popular
        source control system, and certainly the most popular open source one.
        Gitlab stores the source code, making it easy to share and removing
        worries such as downtime and backups from the developers. However, many
        developers using GitLab install Git on their personal computer systems
        too, so they can keep their changes there before pushing them into
        GitLab.
      </p>
      <h4>Testing and continuous integration/continuous development (CI/CD)</h4>
      <p>
        CI/CD is central to modern development environments because it automates
        testing and deployment. GitLab offers built-in CI/CD through a menu
        item.
      </p>
      <p>
        Software usually requires a build process before it can be tested or
        distributed. Various tools, such as Ant and Gradle, offer a structured
        way to define and run the builds. Some types of document development
        also requires a build--for instance, to create a web page or PDF from
        the file in which the writer is working. CI/CD is your way to automate
        the build.
      </p>
      <p>
        Testing is critical to prevent regressions (bugs that break the way
        software works). To catch bugs early, developers should use CI/CD to run
        their tests at each stopping point in their coding. CI/CD allows the
        tests to be run on each commit, so that a bug can't even be checked in.
        (Of course, developers can't anticipate every bug, so some will slip
        through the testing process.)
      </p>
      <p>
        Finally, CI/CD can deploy a software package or document to the location
        where it can be used, and notify people that a new package or document
        is available.
      </p>
      <h3>Wiki</h3>
      <p>
        This may be useful on large projects to make it easy for members to
        write and update documentation. A wiki can also record decisions and
        show how they change over time.
      </p>
      <h3>Set up project activities</h3>
      <p>Regular processes should be in place for:</p>
      <ul>
        <li>Code reviews.</li>
        <li>
          Bug fixing. Recruit good coders to keep on top of reports, determine
          whether a report is accurate, and direct accurate reports to the
          people who can fix the problem. These volunteers should also politely
          respond to each report.
        </li>
        <li>Sprints and codeathons, if your team uses them.</li>
        <li>Education for new members.</li>
        <li>
          Meetings. For legal purposes, IEEE SA OPEN requires that each project
          hold regular team meetings, record the names and affiliations of all
          participants, and keep public minutes.
        </li>
      </ul>
      <p>Some other major activities follow.</p>
      <h4>Budgeting and finance</h4>
      <p>
        A trustworthy member should be chosen as treasurer. The project should
        follow all the proper financial practices of any organization, such as
        preserving receipts for expenditures and sharing budgets regularly (such
        as once a month) at project meetings.
      </p>
      <h4>Measuring progress (metrics)</h4>
      <p>Measurements play several valuable roles, including:</p>
      <ul>
        <li>
          Revealing who has contributed, and thus contributing to members'
          morale and their desire to contribute
        </li>
        <li>
          Gauging the effectiveness of the project (e.g., new product features,
          the rate of addressing bug reports)
        </li>
        <li>
          Gauging the health of the project and identifying aspects that need
          work
        </li>
      </ul>
      <p>
        Luckily, modern tools make it easy to collect many measurements through
        automated tools. You can set up and get access to metrics in GitLab
        through an icon on the left side of the screen.
      </p>
      <h4>Rewards</h4>
      <p>
        Volunteers come to a project for many reasons. Experts want to create
        standards around their practices, students come to hone their skills,
        and everyone wants to make better technology for the world. But at all
        levels, people appreciate being rewarded for their work and will
        contribute more if they feel rewarded.
      </p>
      <p>
        Many projects offer points or badges to key contributors. Important
        contributors can also be promoted into new roles, such as committers or
        board members.
      </p>
      <p>
        Besides technical contributions, the project should track help with
        documentation, marketing, advocacy, work with outside organizations such
        as IEEE SA, and anything else that is important to the success of the
        project.
      </p>
      <p>
        Metrics are crucial to rewarding contributors. And these must be
        meaningful metrics that reflect real achievements; otherwise they can
        offer perverse incentives to do things like make worthless commits. See
        the section "Who has contributed".
      </p>
      <p>
        Reports based on the metrics should also be shared with the community
        regularly, so that community members see how much activity is going on.
        Some of the things you can report are the number and size of each pull
        request, along with the name of the person submitting the pull request,
        and project resources used during that time period.
      </p>
    </Section>
  </Layout>
);

export default Projects;
