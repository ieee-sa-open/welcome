import PropTypes from 'prop-types';
import React from 'react';
import styles from './ArticleSlider.module.css';

const ArticleSlider = ({ articles }) => {
  const articleList = articles.map((item, i) => {
    return (
      <div className={styles.articleContainer} key={i}>
        <img className={styles.bioImage} src={item.image} />
        <p className={styles.articleDescription}>{item.description}</p>
        <a className={styles.articleMoreInfo} href={item.link}>See more {'>'}</a>
      </div>
    );
  });

  return <div className={styles.articleSlider}>{articleList}</div>;
};

ArticleSlider.propTypes = {
  articles: PropTypes.array,
};

ArticleSlider.defaultProps = {
  articles: [],
};

export default ArticleSlider;
