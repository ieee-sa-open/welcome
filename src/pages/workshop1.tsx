import React from 'react';
import { Helmet } from 'react-helmet';
import { PageProps } from 'gatsby';

import Layout from '../components/Layout/Layout';
import Hero from '../components/Hero/Hero';
import Section from '../components/Section/Section';
import Example from '../components/Example/Example';
import TableOfContents from '../components/TableOfContents/TableOfContents';

const Workshop1: React.FC<PageProps> = () => (
  <Layout>
     <Helmet>
      <title>Welcome - Advisory Group Workshop</title>
    </Helmet>
    <Hero heroIntro={'SA OPEN'} heroPrimary={'Workshop Events'} />
    <TableOfContents
      items={[
    {
          text: 'Workshop Notes',
          link: '#notes',
    },
	  {
	      text: 'Breakout Sessions',
	      link: '#breakouts',
    },
	    ]}
    />
    <Section type={'light'}>
     <h6>Advisory Group Workshop <br />
       The Spirit of Mentorship<br />
     December 8th, 2020</h6>  
    
   <p>
     <Example>
    <strong>"The greatness of a community is most accurately measured by the compassionate actions of its members." 
    <br />
    – Coretta Scott King</strong>
    </Example>
    </p>
  <p>
    Community in the world of open source is invaluable. As a contributor you afford others access to your skills, 
    knowledge, and any other contributions you willingly give to help projects and other communities succeed. 
    The same can be said for mentorship. 
  </p>
  <p>
    The Spirit of Mentorship event is for volunteers of all backgrounds, with different skill sets, 
    and from all walks of life who want to come together to make a difference for the advancement of 
    open source. We will be creating our visions and setting actionable goals for the 
    Community Advisory Group (CAG), Marketing Advisory Group (MAG) and the Technical 
    Advisory Group (TAG), that aid new and existing projects, users, and groups on 
    the platform reach success.
  </p>
   <p>
     Projects on the IEEE SA OPEN platform rely on our ability to collaborate 
     and help bring their products to life and aid humanity in becoming a better place. 
     When we help fix a bug, update documentation, review code, or help them to better 
     market their final project, we get to witness the impact it has on the way we work, 
     live, and communicate with one another. 
  </p>
  <p>
    We can't wait to see what good we can accomplish in open source, and we thank 
    you for helping along the way. 
  </p>
    </Section>

    <Section type={'dark'}>
  <a className={'anchor'} id={'notes'} />
      <h6>Workshop Notes:</h6>
    <div className={'center'}>
      <p>
      <a href="https://opensource.ieee.org/workshops/advisory-groups/workshop-notes-videos/-/blob/main/README.md" target='_blank'>Workshop Notes & Recordings</a>.
      </p>
      </div>
    </Section>
  <Section type={'light'}>
  <a className={'anchor'} id={'breakouts'} />
<p>The associated workshop sessions flowed well into each other, so we are presenting one set of notes and one video for each set of advisory group breakout rooms.</p>

<p>
<strong>Community Advisory Group Breakout Room</strong>
<ul>
<li><a href="https://opensource.ieee.org/workshops/advisory-groups/workshop-notes-videos/-/blob/main/supporting-documentation/Community-Advisory-Group-meeting.md" target='_blank'>Notes from the Workshop</a></li>
<li><a href="https://opensource.ieee.org/workshops/advisory-groups/workshop-notes-videos/-/blob/main/supporting-documentation/CAG-breakout.webm" target='_blank'>Video of this Session</a></li>
<li><b>Talk topics:</b></li>
<ul>
<li><b>Community Advisory Group 1:</b> Leveling up: Building Strong Community Leadership in Open Source<br />
Description: A discussion about the tools and processes we'd like to see on the platform to help with overall community leadership and structure.</li>
<li><b>Community Advisory Group 2:</b> Elevated Leaders: Creating Your Niche in Community Leadership<br />
Description: A discussion about how we, as community leaders, can create an action plan for a better open source community presence.</li>
</ul>
</ul>
</p>

<p>
<strong>Technical Advisory Group Breakout Room</strong>
<ul>
<li><a href="https://opensource.ieee.org/workshops/advisory-groups/workshop-notes-videos/-/blob/main/supporting-documentation/Technical-Advisory-Group-Breakout-Notes.md" target='_blank'>Notes from the Workshop</a></li>
<li><a href="https://opensource.ieee.org/workshops/advisory-groups/workshop-notes-videos/-/blob/main/supporting-documentation/TAG_breakout.webm" target='_blank'>Video of this Session</a></li>
<li><b>Talk topics:</b></li>
<ul>
<li><b>Technical Advisory Group 1:</b> Technology Titans: Designing a Technical Open Source Community<br />
Description: A discussion about the technical resources we can deliver to groups, projects, and the overall platform.</li>
<li><b>Technical Advisory Group 2:</b> Sharing Expertise: Your Role in the Technical Open Source Community<br />
Description: We're putting our vision into an action plan. We'll be discussing how we can collaborate to create structure within our technical scope.</li>
</ul>
</ul>
</p>

<p>
<strong>Marketing Advisory Group Breakout Room</strong>
<ul>
<li><a href="https://opensource.ieee.org/workshops/advisory-groups/workshop-notes-videos/-/blob/main/supporting-documentation/Spirit%20of%20Mentorship%20MAG%20Notes.md" target='_blank'>Notes from the Workshop</a></li>
<li><a href="https://opensource.ieee.org/workshops/advisory-groups/workshop-notes-videos/-/blob/main/supporting-documentation/MAG-breakout.webm" target='_blank'>Video of this Session</a></li>
<li><b>Talk topics:</b></li>
<ul>
<li><b>Marketing Advisory Group 1:</b> Masonry of Marketing in an Open Source Community<br />
Description: A discussion about our vision for the open source marketing resources we want to provide for groups and projects on the platform.</li>
<li><b>Marketing Advisory Group 2:</b> Using Your Strengths in the Marketing Open Source Community<br />
Description: We'll be discussing how we can turn our vision for open source marketing into a roadmap for success.</li>
</ul>
</ul>
</p>


<p>
<strong>Academia Breakout Room</strong>
<ul>
<li><a href="https://opensource.ieee.org/workshops/advisory-groups/workshop-notes-videos/-/blob/main/supporting-documentation/Academic_breakout_notes.md" target='_blank'>Notes from the Workshop</a></li>
<li> <a href="https://opensource.ieee.org/workshops/advisory-groups/workshop-notes-videos/-/blob/main/supporting-documentation/academia-breakout.webm" target='_blank'>Video of this session</a></li>
<li><b>Talk topics:</b></li>
<ul>
<li><b>Academia Group 1 & 2:</b> <br />
We'll be discussing different potential use cases of the SA OPEN platform for academic research projects and for classroom use.</li>
</ul>
</ul>
</p>

<p>
<strong>Tool Evaluation and Open Standards Breakout Room</strong>
<ul>
<li><a href="https://opensource.ieee.org/workshops/advisory-groups/workshop-notes-videos/-/blob/main/supporting-documentation/tool-evaluation-notes.md" target='_blank'>Notes from the Workshop</a></li>
<li><a href="https://opensource.ieee.org/workshops/advisory-groups/workshop-notes-videos/-/blob/main/supporting-documentation/tool-eval-breakout.webm" target='_blank'>Video of this session</a></li>
<li><b>Talk topics:</b></li>
<ul>
<li><b>Tool Evaluation 1:</b> Discussion on Metrics</li>
<li><b>Tool Evaluation 2:</b> Tools Evaluation: Why we need Free Software Standards<br />
Description: To encourage adoption by non-technical (organic) people, we will discuss the free software tools that are easiest for people to adopt and how we can all contribute to documentation, workshops and tutorials on free and open source tools available to everyone. Time to start thinking on ways to make the path fun and easy for all!  How can technical people conspire with organic people to help everyone make a smooth transition to ethical tools?</li>
</ul>
</ul>
</p>
</Section>
  </Layout>
);
export default Workshop1;