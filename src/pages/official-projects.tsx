import React from 'react';
import { Helmet } from 'react-helmet';
import { PageProps, Link } from 'gatsby';

import Layout from '../components/Layout/Layout';
import Hero from '../components/Hero/Hero';
import Section from '../components/Section/Section';

const OfficialProjects: React.FC<PageProps> = () => (
  <Layout>
    <Helmet>
      <title>Official Projects - IEEE SA OPEN</title>
    </Helmet>
    <Hero heroIntro={'IEEE SA OPEN'} heroPrimary={'About Us'} />
    <Section type={'light'}>
      <h1>What is an official project?</h1>
      <p>
        Project Leaders can present a project to the board that runs IEEE SA
        OPEN, the Open Source Committee (OSCom), and ask for it to be recognized
        as an official IEEE SA OPEN project. Official projects receive a great
        deal of attention, guidance, and advice from IEEE. They get legal advice
        to avoid problems with anti-trust laws and other regulations. If the
        project wants to establish an IEEE standard, the IEEE guides them along
        that process.
      </p>
      <p>
        A project must meet several strict criteria to become an official
        project. These requirements are laid out in{' '}
        <Link to={'https://opensource.ieee.org/community/manual/-/wikis/home'}>
          maintainers' manual
        </Link>
        . Some of the key requirements are:
      </p>
      <ul>
        <li>
          Providing all the documents that ensure that the project conforms to
          an open license and is free of conflicts of interest, such as the
          Contributing Agreement.
        </li>
        <li>
          Undergoing a rigorous code review, for projects with programming
          source code.
        </li>
        <li>Demonstrating a strong leadership team and good governance.</li>
      </ul>
      <p>
        Many projects have no need to become official projects. If the leaders
        do not intend to propose an IEEE standard and are seeing satisfactory
        progress with the governance structure and community they have, they can
        continue to use the opensource.ieee.org platform indefinitely without
        becoming official.
      </p>
    </Section>
  </Layout>
);

export default OfficialProjects;
