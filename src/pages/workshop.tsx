import React from 'react';
import { Helmet } from 'react-helmet';
import { PageProps } from 'gatsby';

import Layout from '../components/Layout/Layout';
import Hero from '../components/Hero/Hero';
import Section from '../components/Section/Section';
import TableOfContents from '../components/TableOfContents/TableOfContents';

const SAOPENEvents: React.FC<PageProps> = () => (
  <Layout>
    <Helmet>
      <title>
      TAG you're IT: Technical Infrastructure in Open Source Communities
      </title>
    </Helmet>
    <Hero heroIntro={'SA OPEN'} heroPrimary={'Events'} />
    <Section type={'blue'}>
      <h6>
        Technical Advisory Group workshop
        <br />
        TAG you're IT: Technical Infrastructure in Open Source Communities
      </h6>
      <h5>
        June 16, 2021 <br />
        10:00 AM - 2:30 PM Eastern Time (16:00-20:30 UTC)
      </h5>
    </Section>
    <TableOfContents
      items={[
        {
          text: 'Information',
          link: '#info',
        },
        {
          text: 'Help Desk',
          link: '#helpdesk',
        },
        {
          text: 'Main Hall',
          link: '#mainhall',
        },
        {
          text: 'Workshop 1',
          link: '#workshop1',
        },
        {
          text: 'Workshop 2',
          link: '#workshop2',
        },
      ]}
    />
    <Section type={'light'}>
      <a className={'anchor'} id={'info'} />
      <h1>General Information</h1>
      <h3>What You Need For the Workshop:</h3>
      <ul>
        <li>
          <a
            href="https://www.ieee.org/profile/public/createwebaccount/showCreateAccount.html?url=https%3A%2F%2Fwww.ieee.org%2F%2F"
            target="_blank"
          >
            Create an IEEE Account
          </a>
        </li>
        <li>
          <a
            href="https://opensource.ieee.org/workshops/june-tag-workshop/20210616-tag-workshop/-/issues/new?issuable_template=workshop-registration"
            target="_blank"
          >
            Register
          </a>
        </li>
        <li>
          <a
            href="https://opensource.ieee.org/workshops/june-tag-workshop/20210616-tag-workshop/-/blob/main/Event%20Information/20210616-event-schedule.md"
            target="_blank"
          >
            Schedule
          </a>
        </li>
        <li>
          <a href="https://saopen.ieee.org/participant-prep/" target="_blank">
            Participant Prep
          </a>
        </li>
        <li>
          <a href="/bbb-faq" target="_blank">
            BigBlueButton (BBB) FAQ
          </a>
        </li>
        <li>
          <a href="/facilitator-guide/" target="_blank">
            BBB Facilitator Guide
          </a>
        </li>
        <li>
          <a
            href="https://mm.leadingbit.com/event-support/channels/town-square"
            target="_blank"
          >
            Help Desk / Event Tech Support
          </a>
        </li>
      </ul>
    </Section>
    <Section type={'dark'}>
      <a className={'anchor'} id={'helpdesk'} />
      <h1>Help Desk</h1>
      <p>
        <b>Description</b> Go here if you are having any problems signing up for
        the workshop or if you have technical issues getting into the workshop
        sessions. Use the following link to reach our{' '}
        <a
          href="https://mm.leadingbit.com/event-support/channels/town-square"
          target="_blank"
        >
          Help Desk
        </a>
        .
      </p>
    </Section>
    <Section type={'light'}>
      <a className={'anchor'} id={'mainhall'} />
      <h1>Main Hall</h1>
      <p>
        <b>Description</b> Ready to join the workshop?{' '}
        <a href="https://bbb.virtualroadshow.io/b/kat-pes-bzd-ic5">
          Go here first to hear the main speaker events starting at 10:00AM
        </a>
        . This is our{' '}
        <a
          href="https://opensource.ieee.org/workshops/june-tag-workshop/20210616-tag-workshop/-/blob/main/Event%20Information/20210616-event-schedule.md"
          target="_blank"
        >
          workshop schedule
        </a>
        .
      </p>
    </Section>
    <Section type={'dark'}>
      <a className={'anchor'} id={'workshop1'} />
      <h1>Workshop Breakout 1:</h1>
      <p>
        <b>11:40 AM</b> <br />
        <ul>
          <li>
            <b>Breakout Room 1:</b>{' '}
            <strong>
              <a href="https://bbb.virtualroadshow.io/b/kat-2ww-ooa-eyo">
                Open Source Security
              </a>
            </strong>{' '}
            <br />
            Protecting your community from vulnerabilities is critical to your
            project's sustainability. Join us as we identify tools and best
            practices for open source security.
          </li>
          <li>
            <b>Breakout Room 2:</b>{' '}
            <strong>
              <a href="https://bbb.virtualroadshow.io/b/kat-qya-w9q-40y">
                Production Readiness: Planning for Success
              </a>
            </strong>
            <br />
            Building software for a global audience requires a good plan. Join
            this discussion to share ideas on what it takes to be ready for when
            your community grows!
          </li>
        </ul>
      </p>
    </Section>
    <Section type={'light'}>
      <a className={'anchor'} id={'workshop2'} />
      <h1>Workshop Breakout 2:</h1>
      <p>
        <b>1:05 PM</b> <br />
        <ul>
          <li>
            <b>Breakout Room 1:</b>{' '}
            <strong>
              <a href="https://bbb.virtualroadshow.io/b/kat-ejh-v7v-k7j">
                {' '}
                Source Marketing: Foundations for Success
              </a>{' '}
            </strong>{' '}
            <br />
            Explore how the Marketing Advisory Group on the IEEE SA OPEN
            platform can create processes and resources to help with project and
            community success.
          </li>
          <li>
            <b>Breakout Room 2:</b>{' '}
            <strong>
              <a href="https://bbb.virtualroadshow.io/b/kat-0uc-vms-zwc">
                Finding the Right Tools: Creating Best Practices for Feature and
                Tool Evaluation
              </a>
            </strong>{' '}
            <br />
            What tools do you use in creating, supporting and delivering open
            source projects? Join us as we develop feature and tool evaluation
            best practices for the IEEE SA OPEN Community.
          </li>
        </ul>
      </p>
    </Section>
  </Layout>
);

export default SAOPENEvents;
