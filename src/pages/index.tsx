import React from 'react';
import { Helmet } from 'react-helmet';

import Layout from '../components/Layout/Layout';
import Hero from '../components/Hero/Hero';
import Section from '../components/Section/Section';


const Index: React.FC<PageProps> = () => (
  <Layout>
    <Helmet>
      <title>Welcome - IEEE SA OPEN</title>
    </Helmet>
    <Hero heroIntro={'WELCOME TO'} heroPrimary={'IEEE SA OPEN'} /> 
    <Section type={'light'}>
      <h5>Creating the standard for open source communities</h5>
      <h3>WE&nbsp;&nbsp;WANT YOU TO KNOW...</h3>
      <p>
        The addition of open source to the IEEE SA collaboration platform is a key 
        milestone in the organization’s ongoing transformation and expansion into 
        new business and industry sectors. IEEE SA OPEN is intended to bridge the 
        gap between standards developers and other open technical communities to 
        enable nimble and creative technical solutions. IEEE SA OPEN address common 
        challenges faced by the open-source development community, such as lack of 
        relevant engagement in projects, solution incompatibility, and complexity 
        around intellectual property (IP) licensing. IEEE SA OPEN development 
        projects are underway in diverse areas of technology development. 
        IEEE SA OPEN provides community management, DevOps infrastructure, and 
        governance, along with a growing set of templates and guides—thus supporting 
        and strengthening the collaboration, growth, and sustainability of open source 
        communities and their projects. IEEE SA OPEN conveys to individuals and 
        organizations the benefits of neutral, third-party administration, and 
        unique service add-ons utilizing the skills of more than 422,000 IEEE 
        members in over 160 countries.
      </p>
      <p>
        Employing  a customized offering of the proven, stable, and easy-to-use GitLab 
        software, and Mattermost team collaboration and messaging, IEEE SA OPEN leverages 
        the globally recognized expertise of IEEE in technical publishing by adding 
        the option of supplementing technical publications with open source code 
        that implements the published research results. As the platform continues 
        to grow, upgrades will be added to increase the support of project and 
        community structures.
      </p>
      <p>
        IEEE SA OPEN offers a variety of à la carte support services. For example, 
        comprehensive fiscal sponsorship for mission-based humanitarian projects is 
        available through a fund, which is a joint activity of IEEE SA and the 
        IEEE Foundation. Development, quality assurance, maintenance, and documentation 
        support also are available. With the addition of IEEE SA OPEN, IEEE SA avails 
        developers proven mechanisms throughout the lifecycle of incubating promising 
        technologies—including research, open source, standardization, and 
        go-to-market services.
      </p>
      <h4>What We Can Do For You</h4>
      <p>IEEE SA OPEN is a comprehensive open source development platform 
        that harnesses the power of familiar open source tools. 
        Using the platform you can:</p>
      <p>
        <ul>
        <li>Give your project the trust and authenticity of IEEE</li>
        <li>Experience an open, flexible development environment</li>
        <li>Enjoy unique service add-ons and customizations</li>
        <li>Easily initiate a project thru Our Project Pipeline Process</li> 
        <li>Put your project on the fast track to IEEE standardization</li>
        <li>Have access to technical, marketing, community resources and best practices 
        from subject matter experts</li>
        </ul>
      </p>
      <p>The platform encompasses IEEE's unparalleled member network, technical 
        expertise and resources with a goal to accelerate innovation and enable 
        technological advancements that positively impact the way people live, 
        work, and communicate. It's all open source, which allows for contributions 
        to platform features and community collaboration.
      </p>
      <p>IEEE SA OPEN enables you to focus on the technical, community, and marketing 
        aspects of your project by handling all the activities necessary to have your 
        organization up and running in days, instead of months. The community also provides 
        best practices to manage and grow your project with beneficial governance processes, 
        and expertise provided by subject matter experts who are there for you during any 
        stage of development. 
      </p>
    </Section>
  </Layout>
);

export default Index;
