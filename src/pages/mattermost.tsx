import React from 'react';
import { Helmet } from 'react-helmet';

import Layout from '../components/Layout/Layout';
import MultiColumn from '../components/MultiColumn/MultiColumn';
import Column from '../components/Column/Column';
import Hero from '../components/Hero/Hero';
import Section from '../components/Section/Section';
import Example from '../components/Example/Example';
import TableOfContents from '../components/TableOfContents/TableOfContents';

import i105_1 from '../images/getting-started/105_1.png';
import i105_2 from '../images/getting-started/105_2.png';
import i105_3 from '../images/getting-started/105_3.png';
import i105_4 from '../images/getting-started/105_4.png';



const Mattermost: React.FC<PageProps> = () => (
  <Layout>
    <Helmet>
      <title>Getting Started - IEEE SA OPEN</title>
    </Helmet>
    <Hero heroIntro={'IEEE SA Open'} heroPrimary={'Mattermost'} />
    <TableOfContents
      items={[
        {
          text: 'Table of Contents',
          link: '/getting-started',
        },
        {
          text: 'Mattermost Basics',
          link: '#mmbasics',
        },
        {
          text: 'More on Mattermost',
          link: '#mattermost',
        },
      ]}
    />
    <Section type={'light'}>
      <a className={'anchor'} id={'mmbasics'} />
      <a className={'anchor'} id={'#mmbasics'} />
     <h1>Mattermost Basics</h1>
<p>
  IEEE SA OPEN has integrated the open source Mattermost chat service into
  GitLab CE. Mattermost resembles the Slack service popular in many
  businesses. Because chat is a rapid, real-time interaction, it can be
  incredibly useful for mentoring, troubleshooting and handling personal
  issues. IEEE SA OPEN has adopted a 
  <a href="https://www.ieee.org/content/dam/ieee-org/ieee/web/org/about/ieee_code_of_conduct.pdf" target='_blank'> Code of Conduct</a>  to foster a healthy atmosphere.
</p>

<p>
  When you are logged in to the IEEE SA OPEN GitLab, you may also visit
  the <a href="https://opensource-connect.ieee.org/" target='_blank'>https://opensource-connect.ieee.org/site</a> to join in the community discussion on Mattermost. The same
  credentials act as a single sign-on (SSO) to both tools.
</p>

<p>
  Click the Gitlab button on the right. If you aren't logged in to <a href="https://opensource-connect.ieee.org/" target='_blank'>opensource.ieee.org</a>, you will be guided to a login screen.
</p>

<Example>
  <img alt="mattermost 1" src={i105_1} />
  <p>
    On your first visit to Mattermost, you will be offered a list of teams
    that you can join. A team is a digital workspace where you can
    collaborate. Each team will host several channels for discussion.
  </p>
  <p>
    Scroll through the team list to find a particular team you would like
    to join first. If there is none, Welcome is always a good place to
    start.
  </p>
  <p>
    After choosing one team, you will be guided through a very brief
    three-screen tutorial. Guidance for joining additional teams follows.
  </p>
  <img alt="mattermost 2" src={i105_2} />
  <p>
    There are two places to join additional teams on the main Mattermost
    screen: the Team sidebar and the Main Menu. Both will return you to
    the screen above.
  </p>
</Example>
<MultiColumn>
  <Column>
    <h3>Joining More Teams</h3>
    <p>
      The Team sidebar (at the far right side of the main Mattermost
      screen) is a useful tool that allows you to switch between teams and
      also to join another team by clicking the [+] icon.
    </p>
    <p>
      The example displayed also shows a second option for taking this
      action, the [Join Another Team] option in the Main Menu. This menu
      can be accessed by clicking on the three horizontal lines (or
      hamburger menu). This action will need to be repeated for each team
      you wish to join.
    </p>
  </Column>
  <Column>
    <img alt="mattermost 3" src={i105_3} />
  </Column>
</MultiColumn>
<MultiColumn>
  <Column>
    <h3>Joining Channels</h3>
    <p>
      Channels are used to organize conversations across different topics.
      They are located on a panel at the left side of the Mattermost. All
      users are automatically subscribed to the Town Square and the Off-Topic channel in each team. The Town
      Square channel is for general conversations and the
      Off-Topic channel is a great place for socializing.
    </p>
    <p>
      There are three types of channels: public channels, private
      channels, and direct messages. IEEE SA OPEN encourages our community
      members to create public channels, so that we can engage with one
      another and create new ideas. Channels correspond to the different
      projects in each GitLab group. These are places where you can ask
      questions, problem solve, and collaborate with other people working
      on similar projects.
    </p>
  </Column>
  <Column>
    <img alt="mattermost 3" src={i105_4} />
  </Column>
</MultiColumn>
<h3>Messages</h3>
<p>
  If you want to add more than just plain text, take advantage of
  Mattermost's support for Markdown formatting. This allows you to write
  formatted messages, insert tables, and provide related information while
  collaborating with other members. You can label your comments in the
  chat using hashtags so they can be searched for and referenced by anyone
  later on. You can use an @ symbol to tag someone in a comment if you are
  responding to them or need to direct a comment to them specifically in
  the open chat room.
</p>

<h3>Additional Account Settings</h3>
<p>
  You may want to take this opportunity to configure your profile settings
  and notification preferences. Click on Account Settings at the top of
  the Main Menu to do so.
</p>

<h4>Profile Settings</h4>

<Example>
  <p>
    <b>Full Name</b>
    <br />
    If you choose to enter a full name, it will appear in the direct
    messages member list. By default, you will receive mention
    notifications when someone types your first name. Entering a full name
    is optional.
  </p>
  <p>
    <b>Username</b>
    <br />
    Usernames are unique identifiers appearing next to all posts. Your
    username is created when your IEEE SA OPEN GitLab account is created.
    It cannot be changed here.
  </p>
  <p>
    <b>Nickname</b>
    <br />
    Nicknames appear in the direct messages member list and team
    management modal. You will not receive mention notifications when
    someone types your nickname unless you add it to the Words That
    Trigger Mentions in as noted in the Notifications section below.
  </p>
  <p>
    <b>Position</b>
    <br />
    Nicknames appear in the direct messages member list and team
    management modal. You will not receive mention notifications when
    someone types your nickname unless you add it to the Words That
    Trigger Mentions in as noted in the Notifications section below.
  </p>
  <p>
    <b>Email</b>
    <br />
    Nicknames appear in the direct messages member list and team
    management modal. You will not receive mention notifications when
    someone types your nickname unless you add it to the Words That
    Trigger Mentions in as noted in the Notifications section below.
  </p>
  <p>
    <b>Profile Picture</b>
    <br />
    Nicknames appear in the direct messages member list and team
    management modal. You will not receive mention notifications when
    someone types your nickname unless you add it to the Words That
    Trigger Mentions in as noted in the Notifications section below.
  </p>
</Example>

<h4>Notifications</h4>

<Example>
  <p>
    <b>Desktop Notifications</b>
    <br />
    Desktop notifications appear in the corner of your main monitor when
    there is activity in Mattermost.
    <br />
    When Desktop notifications are set to [Only for mentions and direct
    messages], an empty red circle is displayed over the upper right
    corner of the Mattermost dock icon when any message without an
    at-mention is received. A solid red circle with a post count is
    displayed when a message with an at-mention is received.
    <br />
    Choose what activity triggers a desktop notification. This setting
    applies globally, but this preference is customizable for each channel
    from the channel name drop-down menu. A notification sound can also be
    enabled for any activity that would trigger a desktop notification.
  </p>
  <p>
    <b>Email Notifications</b>
    <br />
    Email notifications are sent for mentions and direct messages after
    you've been offline for more than 60 seconds or away from Mattermost
    for more than 5 minutes. Email Batching has been enabled, allowing
    user to select how often email notifications will be sent. All
    notifications received over a chosen time period are combined and sent
    in a single email.
  </p>
  <p>
    <b>Mobile Push Notifications</b>
    <br />
    Push notifications can be sent to your mobile device if you have the
    Android or iOS app installed. You can choose the type of activity that
    will send a notification. By default, push notifications are sent [For
    mentions and direct messages]. If push notifications are sent [Never],
    the Mattermost setting to trigger push notifications depending on your
    status is hidden. Otherwise, you can choose when to send push
    notifications depending on your status. By default, push notifications
    are sent if your status is Away or offline.
  </p>
  <p>
    <b>Words That Trigger Mentions</b>
    <br />
    By default, you receive notifications when someone posts a message
    that contains your non-case sensitive username or @username. You also
    receive notifications when someone uses the @channel, @all, and @here
    mentions. You can customize the words that trigger mentions by typing
    them into the input box. This is useful if you want to be notified of
    all posts on a certain topic, for example, marketing.
  </p>
  <p>
    <b>Reply Notifications</b>
    <br />
    In addition to Words that Trigger Mentions, this setting allows you to
    receive mention notifications when someone replies to a thread that
    you started or participated in. You are considered to start a thread
    when you post a message to which other members of your team reply. You
    are considered to participate in a thread when you post a message
    using the reply button in an already existing thread.
  </p>
</Example>
</Section>
<Section type={'dark'}>
<a className={'anchor'} id={'mattermost'} />
<a className={'anchor'} id={'#mattermost'} />
<h1>More on Mattermost</h1>

<h3>Challenges</h3>

<p>
  Logging onto Mattermost at{' '}
  <a href="https://opensource-connect.ieee.org/" target='_blank'>https://opensource-connect.ieee.org/</a>
  takes you to the main page which contains a single button that says
  [GitLab]. Clicking on that button takes you to a page that appears to be
  the Gitlab sign-in. When you click on [sign in with IEEE login] and
  input login information, it then directs you to Mattermost. This process
  makes the user think they are logging into Gitlab, instead of
  Mattermost.
</p>
</Section>
<Section type={'light'}>
    <div className={'center'}>
      <p><strong>Next section -></strong> <a href="/bigbluebutton">Big Blue Button</a></p>
      </div>
    </Section>
</Layout>
);

export default Mattermost;
