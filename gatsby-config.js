module.exports = {
  siteMetadata: {
    title: `IEEE SA Open`,
    description: `IEEE SA Open`,
    siteUrl: `https://saopen.ieee.org`,
    author: `@gatsbyjs`,
   },
   plugins: [
    {
      resolve: `gatsby-plugin-google-analytics`,
      options: {
        // The property ID
        trackingId: 'UA-180899778-1',
        head: true,
      },
    },
    `gatsby-plugin-react-helmet`,
    {
      resolve: `gatsby-source-filesystem`,
      options: {
        name: `images`,
        path: `${__dirname}/src/images`,
      },
    },
      {
        resolve: 'gatsby-plugin-matomo',
        options: {
          siteId: '2',
          matomoUrl: 'https://matomo.leadingbit.com/',
          siteUrl: 'https://saopen.ieee.org',
        },
    },
    `gatsby-transformer-sharp`,
    `gatsby-plugin-sharp`,
    {
      resolve: `gatsby-plugin-manifest`,
      options: {
        name: `ieee-sa-open`,
        short_name: `ieeeSaOpen`,
        start_url: `/`,
        background_color: `#663399`,
        theme_color: `#663399`,
        display: `minimal-ui`,
        icon: `src/images/favicon.png`, // This path is relative to the root of the site.
      },
    },
    `gatsby-plugin-typescript`,
    `gatsby-plugin-sitemap`,
  ],
};
