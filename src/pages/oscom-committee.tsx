import React from 'react';
import { Helmet } from 'react-helmet';
import { PageProps } from 'gatsby';

import Layout from '../components/Layout/Layout';
import Hero from '../components/Hero/Hero';
import Section from '../components/Section/Section';
import Bio from '../components/Bio/Bio';


// Headshots
import RobbyRobsonHeadshot from '../images/oscom/robby-robson-headshot.png';
import DonWrightHeadshot from '../images/oscom/don-wright-headshot.png';
import ChuckAdamsHeadshot from '../images/oscom/chuck-adams-headshot.png';
import CatAllmanHeadshot from '../images/oscom/cat-allman-headshot.png';
import HongPhucHeadshot from '../images/oscom/hong-phuc-headshot.png';
import DennisBrophyHeadshot from '../images/oscom/dennis-brophy-headshot.png';


const OscomCommittee: React.FC<PageProps> = () => (
  <Layout>
    <Helmet>
      <title>OSCOM Committee Membership</title>
    </Helmet>
    <Hero heroIntro={'IEEE SA Open'} heroPrimary={'OSCOM Committee'} />
    <Section type={'light'}>
      <h1>Membership</h1>
      <Bio
        image={RobbyRobsonHeadshot}
        name={'Robby Robson'}
        title={"Chair"}
        description={
          'Dr. Robby Robson is a researcher, entrepreneur, and standards professional who is co-founder and CEO of Eduworks Corporation and serves as a member of the IEEE Standards Association Board of Governors and as chair of the IEEE Standards Education Committee as well as the Open Source Committee and IEEE Computer Society Standards Activity Board Standards Committee. He has contributed to areas ranging from pure mathematics to web-based learning, digital libraries, and applications of AI to learning systems and has been involved in developing interoperability and data standards and open source technology related to learning, education, training, and workforce development for over 20 years. His recent efforts focus on applying AI and open data to provide talent analytics insights and enable rapid, effective, and equitable reskilling for workers. '
        }
      />

      <Bio
        image={HongPhucHeadshot}
        name={'Hong Phuc Dang'}
        title={"Vice-Chair"}
        description={
          'Hong Phuc is the founder of FOSSASIA, an organization that strives to improve people’s lives through sharing open technologies and fostering global connections and sustainable production. She serves a member of the IEEE Standards Association Board of Governors and as vice chair of the Open Source Committee. She is also a board member of the Open Source Business Alliance and the Open Source Initiative. Beside her contributions in the global open source community, she also supports companies in their technology transition to open source and innersource development models. In recent years, she has focused on bringing open source hardware into production at scale with the pocket science lab project as an example.'
        }
      />
      <Bio
        image={DonWrightHeadshot}
        name={'Don Wright'}
        description={
          'Mr. Wright is the President of Standards Strategies, LLC, an information and communication technology standardization engineering firm. He is the retired Director of   Worldwide Standards for Lexmark International, a leading developer of printing technology and products. He has over 40 years of experience in standards, engineering, software development and marketing. Mr. Wright is currently the IEEE Standards Association (SA) VP for IEEE Technical Engagement and Innovation, and the SA Treasurer. He is also a past President of the IEEE Standards Association, a past member of the IEEE Board of Directors, and a past chair of the SA Standards Board.'
        }
      />
      <Bio
        image={ChuckAdamsHeadshot}
        name={'Dr. W. Charlton (Chuck) Adams, Jr.'}
        description={
          "Chuck is a Distinguished Standards Strategist for Futurewei Technologies. An IEEE Senior Life Member, he has been actively engaged in the IEEE-SA’s global advancement in governance, standards development, and open source strategy for two decades. Chuck served as the IEEE SA President (2008 – 2011) and coordinated the founding of the IEEE SA Corporate Advisory Group, with the goal of integrating and expanding industry and entity community involvement within Asia, Europe, and developing communities.  Expanding IEEE SA's global awareness included Open Source, industry association collaboration, Life Cycle standardization, and expanded global cross-industry programs. Chuck has served on the IEEE-SA Board of Governors, Standards Board, Corporate Advisory Group Board, and the ISTO Board. A member of the IEEE Computer and IEEE Communications Societies, he engaged in the initiation of the IEEE SA Open Source strategy, IoT Strategy, Future Communications strategy, Smart Grid cross industry strategy, and vertical industry engagement strategy; and has participated in several developmental programs, including Blockchain / Smart Cities / Smart Grid / Edge-Cloud / IoT activities within the IEEE-SA. Chuck’s 40-year career with IBM included worldwide standards management, intellectual property coordination, and global open source policy development. He was responsiblie for developing and implementing IBM’s open source business model."
        }
      />
      <Bio
        image={CatAllmanHeadshot}
        name={'Cat Allman'}
        description={
          'Cat is the Program Manager, Google, Developer Ecosystem Team, San Francisco. Cat has been with Google for 14 years working on open source, interdisciplinary innovation, and "maker" community development. Her startup experience includes Mt Xinu, Pilot Network Services and Sendmail Inc. She has spoken at events and institutions including http://makerstown.eu/, LinuxConfAU, GoOpen Arctic Forum, CMU, OSCON, ACM-SIGCSE, NUS Singapore, and UCSC, and has organized conferences including the 1st Open Source Track at Grace Hopper, Sustain!, Science Foo Camp, and Education & Lifelong Learning Foo. She is a 4 term USENIX.org Board member, 2x Jury member for Falling-Walls.com, and judge for the 2018 UNESCO Hackathon.'
        }
      />

      <Bio
        image={DennisBrophyHeadshot}
        name={'Dennis Brophy'}
        description={
          'Dennis Brophy is director of strategic business development in the Design Verification Technology group at Siemens EDA where he manages a global interoperability program to promote product integrations with the company’s functional verification products.  He manages group standardization strategy and has a long history of successful collaboration with industry peers for end user benefit. Dennis has been in the electronic design automation industry for the past 41 years.  He was first with Hewlett-Packard for five years where, as a software engineer, he worked on early PCB design automation systems and later became a product manager and launched HP’s electronic design automation services business. He then joined Mentor Graphics, now Siemens EDA, where he has held several positions the past 36 years. Dennis received a B.S. from the University of California at Davis in electrical engineering and computer engineering '
        }
      />
    </Section>
  </Layout>
);

export default OscomCommittee;


