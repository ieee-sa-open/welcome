import React from 'react';
import styles from './Column.module.css';
import PropTypes from 'prop-types';

const Column = ({ children }) => {
  return <div className={styles.column}>{children}</div>;
};

Column.propTypes = {
  children: PropTypes.node,
};

export default Column;
