import React from 'react';
import { Helmet } from 'react-helmet';
import { PageProps } from 'gatsby';

import Layout from '../components/Layout/Layout';
import Hero from '../components/Hero/Hero';
import Section from '../components/Section/Section';

const Sitemap: React.FC<PageProps> = () => (
  <Layout>
    <Helmet>
      <title>SA OPEN Sitemap</title>
    </Helmet>
    <Hero heroIntro={'SA OPEN'} heroPrimary={'Sitemap'} />
    <Section type={'light'}>
      <p>
        <ul>
          <li>
            <strong>Welcome</strong>:{' '}
            <a href="https://saopen.ieee.org/">https://saopen.ieee.org</a>
          </li>
          <li>
            <strong>Community</strong>:{' '}
            <a href="https://saopen.ieee.org/community">
              https://saopen.ieee.org/community
            </a>
          </li>
          <ul>
            <li>
              OSCom Committee Membership:{' '}
              <a href="https://saopen.ieee.org/oscom-committee">
                https://saopen.ieee.org/oscom-committee
              </a>
            </li>
          </ul>
          <li>
            <strong>Getting Started</strong>:{' '}
            <a href="https://saopen.ieee.org/getting-started">
              https://saopen.ieee.org/getting-started
            </a>
          </li>
          <ul>
            <li>
              Participation:{' '}
              <a href="https://saopen.ieee.org/participation">
                https://saopen.ieee.org/participation
              </a>
            </li>
            <li>
              Creating in GitLab:{' '}
              <a href="https://saopen.ieee.org/creating-in-gitlab">
                https://saopen.ieee.org/creating-in-gitlab
              </a>
            </li>
            <li>
              GitLab Project Management:{' '}
              <a href="https://saopen.ieee.org/gitlab">
                https://saopen.ieee.org/gitlab
              </a>
            </li>
            <li>
              Mattermost:{' '}
              <a href="https://saopen.ieee.org/mattermost">
                https://saopen.ieee.org/mattermost
              </a>
            </li>
            <li>
              Big Blue Button:{' '}
              <a href="https://saopen.ieee.org/bigbluebutton">
                https://saopen.ieee.org/bigbluebutton
              </a>
            </li>
            <li>
              Facilitator Guide:{' '}
              <a href="https://saopen.ieee.org/facilitator-guide">
                https://saopen.ieee.org/facilitator-guide
              </a>
            </li>
            <li>
              Participant Prep:{' '}
              <a href="https://saopen.ieee.org/participant-prep">
                https://saopen.ieee.org/participant-prep
              </a>
            </li>
          </ul>
          <li>
            <strong>Projects</strong>:{' '}
            <a href="https://saopen.ieee.org/projects">
              https://saopen.ieee.org/projects
            </a>
          </li>
          <li>
            <strong>Products</strong>:{' '}
            <a href="https://saopen.ieee.org/products">
              https://saopen.ieee.org/products
            </a>
          </li>
          <li>
            <strong>Events</strong>:{' '}
            <a href="https://saopen.ieee.org/events">
              https://saopen.ieee.org/events
            </a>
          </li>
          <ul>
            <li>
              All Things Open Conference 2021:{' '}
              <a href="https://saopen.ieee.org/ato2021">
                https://saopen.ieee.org/ato2021
              </a>
            </li>
            <li>
              GitLab Commit Virtual Conference 2021:{' '}
              <a href="https://saopen.ieee.org/gitlab-commit-2021">
                https://saopen.ieee.org/gitlab-commit-2021
              </a>
            </li>
            <li>
              TechnicalGroup Workshop - TAG You're IT: Technical Infrastructure
              In Open Source Communities:{' '}
              <a href="https://saopen.ieee.org/workshop3">
                https://saopen.ieee.org/workshop3
              </a>
            </li>
            <li>
              Marketing Group Workshop - MAGic In The Making: Creating Open
              Source Marketing Resources:{' '}
              <a href="https://saopen.ieee.org/workshop2">
                https://saopen.ieee.org/workshop2
              </a>
            </li>
            <li>
              Advisory Group Workshop - The Spirit Of Mentorship:{' '}
              <a href="https://saopen.ieee.org/workshop1">
                https://saopen.ieee.org/workshop1
              </a>
            </li>
          </ul>
          <li>
            <strong>Upcoming Events</strong>:{' '}
            <a href="https://saopen.ieee.org/upcoming-events">
              https://saopen.ieee.org/upcoming-events
            </a>
          </li>
          <li>
            <strong>Social Hour</strong>:{' '}
            <a href=" https://saopen.ieee.org/social-hour">
              {' '}
              https://saopen.ieee.org/social-hour
            </a>
          </li>
          <li>
            <strong>Our Team</strong>:{' '}
            <a href="https://saopen.ieee.org/our-team">
              https://saopen.ieee.org/our-team
            </a>
          </li>
          <li>
            <strong>Legal</strong>:{' '}
            <a href="https://saopen.ieee.org/legal">
              https://saopen.ieee.org/legal
            </a>
          </li>
        </ul>
      </p>
    </Section>
  </Layout>
);

export default Sitemap;

