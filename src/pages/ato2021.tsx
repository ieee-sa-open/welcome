import React from 'react';
import { Helmet } from 'react-helmet';
import { PageProps } from 'gatsby';

import Layout from '../components/Layout/Layout';
import Hero from '../components/Hero/Hero';
import Section from '../components/Section/Section';


const ATO2021: React.FC<PageProps> = () => (
  <Layout>
    <Helmet>
      <title>All Things Open Conference 2021</title>
    </Helmet>
    <Hero heroIntro={'SA OPEN'} heroPrimary={'All Things Open 2021'} />
    <Section type={'light'}>
      <div className={'center'}>
        <iframe
          width="560"
          height="315"
          src="https://www.youtube.com/embed/65k67Ycp3mc"
          title="YouTube video player"
          frameborder="0"
          allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture"
          allowfullscreen
        ></iframe>
        <p>
          <b>Silona Bonewald - Executive Director of IEEE SA OPEN</b>
        </p>
      </div>
      <div className={'center'}>
        <iframe
          width="560"
          height="315"
          src="https://www.youtube.com/embed/Y1gCPM-Y7Jg"
          title="YouTube video player"
          frameborder="0"
          allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture"
          allowfullscreen
        ></iframe>
        <p>
          <b>Matt Cantu - CHAOSS, IEEE SA OPEN Badging Contributor</b>
        </p>
      </div>
      <div className={'center'}>
        <iframe
          width="560"
          height="315"
          src="https://www.youtube.com/embed/A8hfNOTnQXA"
          title="YouTube video player"
          frameborder="0"
          allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture"
          allowfullscreen
        ></iframe>
        <p>
          <b>
            Georg Link - Director of Sales Bitergia, CHAOSS, IEEE SA OPEN
            Community Advisory Group Facilitator
          </b>
        </p>
      </div>
      <div className={'center'}>
        <iframe
          width="560"
          height="315"
          src="https://www.youtube.com/embed/LwgRhusLZWo"
          title="YouTube video player"
          frameborder="0"
          allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture"
          allowfullscreen
        ></iframe>
        <p>
          <b>Stephen Jacobs - OpenRIT, IEEE SA OPEN Contributor</b>
        </p>
      </div>
      <div className={'center'}>
        <iframe
          width="560"
          height="315"
          src="https://www.youtube.com/embed/_AIXyc_54BU"
          title="YouTube video player"
          frameborder="0"
          allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture"
          allowfullscreen
        ></iframe>
        <p>
          <b>Charles Eckel - OpenRIT, IEEE SA OPEN Contributor</b>
        </p>
      </div>
      <div className={'center'}>
        <iframe
          width="560"
          height="315"
          src="https://www.youtube.com/embed/krqiJ5Uo20Y"
          title="YouTube video player"
          frameborder="0"
          allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture"
          allowfullscreen
        ></iframe>
                <p>
          <b>Adam Newman - IEEE-ISTO Chief Operating Officer</b>
        </p>
      </div>
    </Section>
  </Layout>
);

export default ATO2021;
