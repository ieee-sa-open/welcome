import React from 'react';
import { Helmet } from 'react-helmet';
import { PageProps, Link } from 'gatsby';

import Layout from '../components/Layout/Layout';
import Hero from '../components/Hero/Hero';
import Section from '../components/Section/Section';
import Example from '../components/Example/Example';
import TableOfContents from '../components/TableOfContents/TableOfContents';

import i329_1 from '../images/getting-started/329_1.png';
import i329_2 from '../images/getting-started/329_2.png';
import i329_3 from '../images/getting-started/329_3.png';
import i329_4 from '../images/getting-started/329_4.png';

import i340_1_1 from '../images/getting-started/340_1_1.png';
import i340_2_1 from '../images/getting-started/340_2_1.png';
import i340_3_1 from '../images/getting-started/340_3_1.png';
import i340_4_1 from '../images/getting-started/340_4_1.png';


const Gitlab: React.FC<PageProps> = () => (
  <Layout>
    <Helmet>
      <title>Getting Started - IEEE SA OPEN</title>
    </Helmet>
    <Hero heroIntro={'IEEE SA Open'} heroPrimary={'Gitlab Project Management'} />
    <TableOfContents
      items={[
        {
          text: 'Table of Contents',
          link: '/getting-started',
        },
        {
          text: 'Project Management Basics',
          link: '#tutorial',
        },
        {
          text: 'Using Labels',
          link: '#labels',
        },
        {
          text: 'More on GitLab',
          link: '#gitlab',
        },
      ]}
    />
    <Section type={'light'}>
      <a className={'anchor'} id={'tutorial'} />
      <a className={'anchor'} id={'#tutorial'} />
      <h1>Gitlab Project Basics</h1>

      <h3>Managing Projects with Gitlab</h3>

      <p>
        The GitLab Issue Board is a software project management tool used to
        plan, organize, and visualize a workflow. It can be used as a{' '}
        <a href={'https://en.wikipedia.org/wiki/Kanban_(development)'}>
          Kanban
        </a>{' '}
        or a{' '}
        <a href={'https://en.wikipedia.org/wiki/Scrum_(software_development)'}>
          Scrum
        </a>{' '}
        board.
      </p>

      <p>
        The Issue Board pairs issue tracking and project management, keeping
        everything in the same place, so that you don't need to jump between
        different platforms to organize your workflow.
      </p>

      <p>
        Issue boards show your issues as cards in vertical lists, organized by
        their assigned labels. They can help you to visualize and manage your
        entire process in GitLab. Add labels, and then create the corresponding
        list for your existing issues. When you're ready, you can drag your
        issue cards from one step to another one.
      </p>

      <p>
        An issue board will show you what issues your team is working on, who is
        assigned to each issue, and where in the workflow those issues are.
      </p>

      <p>
        To let your team members organize their own workflows, use multiple
        issue boards.
      </p>

      <Example>
        <img alt={'gitlab tutorial 1'} src={i329_1} />
      </Example>

      <h3>Use Cases for Issue Boards</h3>

      <p>
        You can tailor GitLab issue boards to your own preferred workflow. Here
        are some common use cases for issue boards.
      </p>

      <h4>Use cases for a Single Issue Board</h4>

      <p>
        With the GitLab Workflow you can discuss proposals in issues, categorize
        them with labels, and from there, organize and prioritize them with
        issue boards.
      </p>

      <p>For example, let's consider this simplified development workflow:</p>

      <Example>
        <ol>
          <li>
            You have a repository that hosts your application's codebase, and
            your team actively contributes code.
          </li>
          <li>
            Your backend team starts working on a new implementation, gathers
            feedback and approval, and passes it over to the frontend team.
          </li>
          <li>
            When front end is complete, the new feature is deployed to a staging
            environment to be tested.
          </li>
          <li>When successful, the new code is deployed to production.</li>
        </ol>
      </Example>

      <p>
        If you have the labels [Backend], [Frontend], [Staging], and
        [Production], and an issue board with a list for each, you can:
      </p>

      <Example>
        <ul>
          <li>
            Visualize the entire flow of implementations from the beginning of
            the development life cycle through deployment to production.
          </li>
          <li>Prioritize the issues in a list by moving them vertically.</li>
          <li>
            Move issues between lists to organize them according to the labels
            you've set.
          </li>
          <li>
            Add multiple issues to lists in the board by selecting one or more
            existing issues.
          </li>
        </ul>
      </Example>

      <h4>Use Cases for Multiple Issue Boards</h4>

      <p>
        With multiple issue boards, each team can have their own board to
        organize their workflow separately.
      </p>

      <Example>
        <p>
          <b>Scrum Team</b>
          <br />
          With multiple issue boards, each team has one board. Now you can move
          issues through each part of the process. For instance: from [To Do] to
          [Doing] to [Done].
        </p>
        <p>
          <b>Organization of Topics</b>
          <br />
          Create lists to order issues by topic and quickly change them between
          topics or groups, such as between [UX], [Frontend], and [Backend]. The
          changes are reflected across boards, as changing lists updates the
          labels on each issue accordingly.
        </p>
        <p>
          <b>Advanced Team Handoff</b>
          <br />
          For example, suppose we have a UX team with an issue board that
          contains:
          <br />
          - To Do
          <br />
          - Doing
          <br />
          - Frontend
          <br />
        </p>
        <p>
          When finished with something, they move the card to Frontend. The
          Frontend team's board looks like:
          <br />
          - Frontend
          <br />
          - Doing
          <br />
          - Done
          <br />
        </p>
        <p>
          Cards finished by the UX team automatically appear in the Frontend
          column when they are ready for them.
        </p>
        <p>
          <b>Quick Assignments</b>
          <br />
          Create lists for each of your team members and quickly drag issues
          onto each team member's list.
        </p>
      </Example>

      <h3>Issue Board Terminology</h3>

      <Example>
        <p>
          <b>Issue Board</b>
          <br />
          An issue board represents a unique view of your issues. It can have
          multiple lists, with each list consisting of issues represented by
          cards.
        </p>
        <p>
          <b>List</b>
          <br />A List is a column on the issue board that displays issues
          matching certain attributes. In addition to the default [Open] and
          [Closed] lists, each additional list shows issues matching your chosen
          label. On the top of each list you can see the number of issues that
          belong to it. Types of lists include:
        </p>
        <ul>
          <li>
            <b>Open</b> (default): all open issues that do not belong to one of
            the other lists. [Open] always appears as the leftmost list.
          </li>
          <li>
            <b>Closed</b> (default): all closed issues. [Closed] always appears
            as the rightmost list.
          </li>
          <li>
            <b>Label list:</b> all open issues for a label. A list with the
            appropriate label must be added for this to function.
          </li>
        </ul>
        <p>
          <b>Card</b>
          <br />A card looks like a box on a list, and it represents an issue.
          You can drag cards from one list to another to change their label. The
          information you can see on a card includes:
        </p>
        <ul>
          <li>Issue title</li>
          <li>Associated labels</li>
          <li>Issue number</li>
          <li>Assignee</li>
        </ul>
      </Example>

      <h3>Permissions</h3>

      <p>
        Users with the Reporter and higher roles can use all the functionality
        of the Issue Board feature to create or delete lists and drag issues
        from one list to another.
      </p>

      <h3>How Issues are Ordered in a List</h3>

      <p>
        Issues on each board appear in an order in each list. You're able to
        change that order by dragging the issues up or down. The changed order
        is saved, so that anybody who visits the same board later sees the
        reordering, with some exceptions.
      </p>

      <p>
        The first time a given issue appears in any board (that is, the first
        time a user loads a board containing that issue), it is ordered in
        relation to other issues in that list according to label priority. At
        this point, that issue is assigned a relative order value by the system,
        representing its relative order with respect to the other issues in the
        list. Any time you reorder that issue by dragging, its relative order
        value changes accordingly.
      </p>

      <p>
        Also, any time that issue appears in any board, the updated relative
        order value is used for the ordering. It's only the first time an issue
        appears that it takes its ordering from the priority order mentioned
        above. This means that if two issues are reordered by any user in a
        given board inside your GitLab instance, the new order is maintained any
        time those two issues are subsequently loaded in any board in the same
        instance (could be a different project board or a different group board,
        for example).
      </p>

      <p>
        This ordering also affects issue lists. Changing the order in an issue
        board changes the ordering in an issue list, and vice versa.
      </p>

      <h3>Creating an Issue Board</h3>

      <p>To create a new issue board:</p>

      <Example>
        <p>
          Click the dropdown with the current board name in the upper left
          corner of the Issue Boards page.
        </p>
        <img alt={'gitlab tutorial 2'} src={i329_2} />
        <p>Click [Create new board] (highlighted in green above).</p>
        <p>Enter the new board's name.</p>
      </Example>

      <h3>Deleting an Issue Board</h3>

      <p>To delete the currently active issue board:</p>

      <Example>
        <p>
          Click the dropdown with the current board name in the upper left
          corner of the Issue Boards page.
        </p>
        <p>Click Delete board.</p>
        <p>Click Delete to confirm.</p>
      </Example>

      <h3>Focus Mode</h3>

      <p>
        Click the button with four arrows at the top right beside the dark green
        [Add Issues] button to toggle focus mode on and off. In focus mode, the
        navigation UI is hidden, allowing you to focus on issues in the board.
      </p>

      <Example>
        <img alt={'gitlab tutorial 3'} src={i329_3} />
      </Example>

      <h3>Actions You Can Take on an Issue Board</h3>

      <Example>
        <ul>
          <li>Create a new list.</li>
          <li>Delete an existing list.</li>
          <li>Add issues to a list.</li>
          <li>Remove an issue from a list.</li>
          <li>Filter issues that appear across your issue board.</li>
          <li>Create workflows. (See below.)</li>
          <li>Drag issues between lists.</li>
          <li>Drag and reorder the lists.</li>
          <li>Change issue labels (by dragging an issue between lists).</li>
          <li>Close an issue (by dragging it to the Done list).</li>
        </ul>
      </Example>

      <p>
        If you're not able to do some of the things above, check to make sure
        you have been granted the correct permissions.
      </p>

      <h3>Create Workflows</h3>

      <p>
        By reordering your lists, you can create workflows. As lists in issue
        boards are based on labels, this process works out of the box with your
        existing issues.
      </p>

      <p>A typical workflow using an issue board would be:</p>

      <Example>
        <ol>
          <li>
            You have <Link to={'#labels'}>created and prioritized labels</Link>{' '}
            so that you can easily categorize your issues.
          </li>
          <li>You have a bunch of issues (ideally labeled).</li>
          <li>
            You visit the issue board and start creating lists to create a
            workflow.
          </li>
          <li>
            You move issues around in lists so that your team knows who should
            be working on what issue.
          </li>
          <li>
            When the work by one team is done, the issue can be dragged to the
            next list so someone else can pick it up.
          </li>
          <li>
            When the issue is finally resolved, the issue is moved to the Done
            list and gets automatically closed.
          </li>
        </ol>
      </Example>

      <p>
        For example, you can create a list based on the label of Frontend and
        one for Backend. A designer can start working on an issue by adding it
        to the Frontend list. That way, everyone knows that this issue is now
        being worked on by the designers.
      </p>

      <p>
        Then, once they're done, all they have to do is drag it to the next
        list, Backend, where a backend developer can eventually pick it up. Once
        they're done, they move it to Done, to close the issue.
      </p>

      <p>
        This process can be seen clearly when visiting an issue, since with
        every move to another list the label changes and a system note is
        recorded.
      </p>

      <h4>Drag issues between lists</h4>

      <p>
        When you drag issues between lists, different behavior occurs depending
        on the source list and the target list.
      </p>

      <Example>
        <img alt={'gitlab tutorial 4'} src={i329_4} />
      </Example>

      <h3>Tips and Tricks</h3>

      <p>A few things to remember:</p>

      <Example>
        <ul>
          <li>
            Moving an issue between lists removes the label from the list it
            came from and adds the label from the list it goes to.
          </li>
          <li>
            An issue can exist in multiple lists if it has more than one label.
          </li>
          <li>
            Lists are populated with issues automatically if the issues are
            labeled.
          </li>
          <li>
            Clicking the issue title inside a card takes you to that issue.
          </li>
          <li>
            Clicking a label inside a card quickly filters the entire issue
            board and show only the issues from all lists that have that label.
          </li>
          <li>
            For performance and visibility reasons, each list shows the first 20
            issues by default. If you have more than 20 issues, start scrolling
            down and the next 20 appear.
          </li>
        </ul>
      </Example>
    </Section>
    <Section type={'dark'}>
      <a className={'anchor'} id={'labels'} />
      <a className={'anchor'} id={'#labels'} />
      <h1>Using Labels</h1>

      <p>
        As the quantity of issues and merge requests increases in GitLab, it's
        more and more challenging to keep track of those items. This is where
        labels come in. They help you organize and tag your work so you can
        track and find the work items you're interested in.
      </p>

      <p>Labels are a key part of issue boards. With labels you can:</p>

      <Example>
        <ul>
          <li>
            Categorize issues and merge requests using colors and descriptive
            titles like bug, feature request, or docs.
          </li>
          <li>Dynamically filter and manage issues and merge requests.</li>
          <li>
            Search lists of issues and merge requests as well as issue boards.
          </li>
        </ul>
      </Example>

      <h4>Project Labels and Group Labels</h4>

      <p>There are two types of labels in GitLab:</p>

      <Example>
        <ul>
          <li>
            Project labels can be assigned to issues and merge requests in that
            project only.
          </li>
          <li>
            Group labels can be assigned to issues and merge requests in any
            project in the selected group or its subgroups.
          </li>
        </ul>
      </Example>

      <h3>Assigning and Unassigning Labels</h3>

      <p>
        Every issue and merge request can be assigned any number of labels. The
        labels are managed in the right sidebar, where you can assign or
        unassign labels as needed.
      </p>

      <p>To assign a label to an issue or merge request:</p>

      <Example>
        <ul>
          <li>In the label section of the sidebar, click Edit.</li>
          <li>
            In the list, click the labels you want. Each label is flagged with a
            checkmark.
          </li>
          <li>
            Find labels by entering a search query and clicking the magnifying
            glass icon, then click on them. You can search repeatedly and add
            more labels.
          </li>
          <li>
            Click X or anywhere outside the label section and the labels are
            applied.
          </li>
        </ul>
      </Example>

      <h3>Creating Labels</h3>

      <p>
        Users with a permission level of Reporter or higher are able to create
        and edit labels.
      </p>

      <h3>Project Labels</h3>

      <p>
        View the project labels list by going to the project and navigating the
        menu at the left to Issues and then to Labels (highlighted in bright
        green). The list includes all labels that are defined at the project
        level, as well as all labels inherited from the immediate parent group.
        You can filter the list by entering a search query at the top and
        clicking the magnifying glass icon.
      </p>

      <Example>
        <img alt={'gitlab using labels 1'} src={i340_1_1} />
      </Example>

      <p>To create a new project label:</p>

      <Example>
        <ul>
          <li>
            Click the New Label button (as shown on the screen above) to open
            the New Label screen.
          </li>
        </ul>
        <img alt={'gitlab using labels 2'} src={i340_2_1} />
        <p>
          <b>Title</b>
          <br />
          This text will appear on the label. Try to make it concise and clear.
        </p>
        <p>
          <b>Description</b>
          <br />
          You can enter a description here to help your team better understand
          the purpose. This is optional.
        </p>
        <p>
          <b>Background Color</b>
          <br />
          Select a background color by clicking on the available colors, or
          input a hex color value for a specific color.
        </p>
        <ul>
          <li>Click the Create Label button to create the label.</li>
        </ul>
      </Example>

      <p>
        You can also create a new project label from within an issue or merge
        request. In the label section of the right sidebar of an issue or a
        merge request:
      </p>

      <Example>
        <img alt={'gitlab using labels 3'} src={i340_3_1} />
        <ul>
          <li>
            Click Edit (highlighted green above) to open the Assign Labels menu.
          </li>
          <li>Click [Create project label].</li>
        </ul>
        <p>
          <b>Name New Label</b>
          <br />
          Fill in the Name New Label field. Note that you can't specify a
          description if creating a label this way. You can add a description
          later by editing the label (see below).
        </p>
        <p>
          <b>Assign Custom Color</b>
          <br />
          Optionally, select a color by clicking on the available colors, or
          input a hex color value for a specific color in the Assign Custom
          Color field.
        </p>
        <p>
          <b>Add List</b>
          <br />
          Click this checkbox if you would like to create a list on your issue
          board with this label as the descriptor.
        </p>
        <ul>
          <li>Click Create.</li>
        </ul>
      </Example>

      <h3>Managing Project Labels</h3>

      <h4>Editing a Label</h4>

      <p>Once created, you can edit a label by clicking the pencil icon.</p>

      <p>
        You can also delete a label by clicking the three dots next to the
        Subscribe button and selecting Delete.
      </p>

      <p>
        <i>
          Caution: If you delete a label, it is permanently deleted. You will
          not be able to undo the deletion, and all references to the label will
          be removed from the system.
        </i>
      </p>

      <h4>Promote a Project Label to a Group Label</h4>

      <p>
        If you previously created a project label and now want to make it
        available for other projects within the same group, you can promote it
        to a group label.
      </p>

      <p>
        If other projects in the same group have a label with the same title,
        they will all be merged with the new group label. If a separate group
        label with the same title exists, it will also be merged.
      </p>

      <p>
        All issues, merge requests, issue board lists, issue board filters, and
        label subscriptions with the old labels will be assigned to the new
        group label.
      </p>

      <p>
        <i>
          Caution: Promoting a label is a permanent action, and cannot be
          reversed.
        </i>
      </p>

      <p>To promote a project label to a group label:</p>

      <Example>
        <ul>
          <li>
            Navigate to the project. In the menu at the left, navigate to Issues
            and then to the Labels in the project.
          </li>
          <li>
            Click on the three dots icon next to the Subscribe button and select
            Promote to Group Label.
          </li>
        </ul>
      </Example>

      <h3>Group Labels</h3>

      <p>
        View the Group Labels list by going to the group and navigating the menu
        at the left to Issues and then to Labels. The list includes all labels
        that are defined at the group level only. It does not list any labels
        that are defined in projects. You can filter the list by entering a
        search query at the top and clicking the magnifying glass icon.
      </p>

      <p>
        To create a group label, navigate the menu at the left to Issues and
        then to Labels and follow the same process as creating a project label.
      </p>

      <h4>Label Priority</h4>

      <p>
        Labels can have relative priorities, which are used in the Label
        Priority and Priority sort orders of the Issue and Merge Request List
        pages. Prioritization for both group and project labels happens at the
        project level, and cannot be done from a Group Label list.
      </p>

      <p>
        From the Project Label list page, star a label to indicate that it has
        priority.
      </p>

      <Example>
        <img alt={'gitlab using labels 4'} src={i340_4_1} />
      </Example>

      <p>
        Drag starred labels up and down the list to change their priority, where
        higher in the list means higher priority.
      </p>

      <h3>Sorting Issues and Merge Requests</h3>

      <p>
        On the Merge Request and Issue List pages (for both groups and projects)
        you can sort by Label Priority or Priority.
      </p>

      <h4>By Label Priority</h4>

      <p>
        If you sort by Label Priority, GitLab uses this sort comparison order:
      </p>

      <Example>
        <ol>
          <li>Items with a higher priority label.</li>
          <li>Items without a prioritized label.</li>
        </ol>
      </Example>

      <p>
        Ties are broken arbitrarily. Note that only the highest prioritized
        label is checked, and labels with a lower priority are ignored.
      </p>

      <h4>By Priority</h4>

      <p>If you sort by Priority, GitLab uses this sort comparison order:</p>

      <Example>
        <ol>
          <li>
            Items with milestones that have due dates, where the soonest
            assigned milestone is listed first.
          </li>
          <li>Items with milestones with no due dates.</li>
          <li>Items with a higher priority label.</li>
          <li>Items without a prioritized label.</li>
        </ol>
      </Example>

      <p>Ties are broken arbitrarily.</p>
    </Section>
    <Section type={'light'}>
      <a className={'anchor'} id={'gitlab'} />
      <a className={'anchor'} id={'#gitlab'} />
      <h1>More on Gitlab</h1>
      <p>
        If you’d like to learn more about GitLab, here are some resources that
        we’ve found helpful.
      </p>

      <h3>Video References</h3>
      <Example>
        <ul>
          <li>
            Onboarding conversation at Gitlab:{' '}
            <a href={'https://www.youtube.com/watch?v=6VI_SqkcQIQ]'}>
              https://www.youtube.com/watch?v=6VI_SqkcQIQ]
            </a>{' '}
            Length: 1:21:00.
          </li>
          <li>
            GitLab for Non-Tech and Project Management Use -- Contribute NOLA
            breakout session:{' '}
            <a href={'https://www.youtube.com/watch?v=tbg8KSyIWVg'}>
              https://www.youtube.com/watch?v=tbg8KSyIWVg
            </a>{' '}
            Start at 13:30.
          </li>
        </ul>
      </Example>
      <h3>Articles</h3>
      <Example>
        <ul>
          <li>
            GitLab 101 – a primer for the non-technical:{' '}
            <a
              href={
                'https://about.gitlab.com/blog/2019/08/02/gitlab-for-the-non-technical/'
              }
            >
              https://about.gitlab.com/blog/2019/08/02/gitlab-for-the-non-technical/
            </a>{' '}
          </li>
          <li>
            2 Examples of how GitLab can be used to manage complex marketing
            projects:{' '}
            <a
              href={'https://about.gitlab.com/blog/2019/12/11/gl-for-pm-prt-2/'}
            >
              https://about.gitlab.com/blog/2019/12/11/gl-for-pm-prt-2/
            </a>{' '}
          </li>
          <li>
            How GitLab tool fosters collaborative project management:{' '}
            <a
              href={
                'https://about.gitlab.com/blog/2019/12/06/gitlab-for-project-management-one/'
              }
            >
              https://about.gitlab.com/blog/2019/12/06/gitlab-for-project-management-one/
            </a>{' '}
          </li>
        </ul>
      </Example>
    </Section>
    <Section type={'dark'}>
    <div className={'center'}>
      <p><strong>Next section -></strong> <a href="/mattermost">Mattermost</a></p>
      </div>
    </Section>
  </Layout>
);

export default Gitlab;
