#!/usr/bin/env bash

if [[ "$CI_PAGES_DOMAIN" = "gitlab.io" ]]; then
    sed -i 's_//  path_  path_' gatsby-config.js
fi
