import React from 'react';
import { Helmet } from 'react-helmet';
import { PageProps } from 'gatsby';

import Layout from '../components/Layout/Layout';
import Hero from '../components/Hero/Hero';
import TableOfContents from '../components/TableOfContents/TableOfContents';
import Section from '../components/Section/Section';
import Button from '../components/Button/Button';

const Products: React.FC<PageProps> = () => (
  <Layout>
    <Helmet>
      <title>Products - IEEE SA OPEN</title>
    </Helmet>
    <Hero heroIntro={'IEEE SA OPEN'} heroPrimary={'Products'} />
    <TableOfContents
      items={[
        {
          text: 'IEEE SA OPEN Product Support Process',
          link: '#support',
        },
        {
          text: 'IEEE SA OPEN Community',
          link: '#community',
        },
        {
          text: 'IEEE SA OPEN Venture',
          link: '#venture',
        },
        {
          text: 'IEEE SA OPEN Philanthropy',
          link: '#philanthropy',
        },
      ]}
    />
    <Section type={'light'}>
      <a className={'anchor'} id={'support'} />
      <h1>
        IEEE SA OPEN Product Support Process
      </h1>
      <h5>Our process is simple. We take a look at the following 
        areas and assist you with making the right decisions for 
        your organization.</h5>
      <p>
        IEEE provides the infrastructure for your open source software, open
        hardware and open data projects.
      </p>
      <ul>
        <li>
          <b>Your Needs</b>: We focus on learning what your needs are to help you 
          achieve the success that you and your organization are looking to attain.
        </li>
        <li>
          <b>Governance</b>: Do you need a business organization or mission-based 
          foundation? We can help you sort through your options and choose what works best.
        </li>
        <li>
          <b>Technology</b>: Is your technology open source or on a standards track?
        </li>
        <li>
          <b>Community</b>: How large is your community of members? How often do 
          you meet? With our community of subject matter experts we can take your 
          project to the next level. Our advisory groups can assist your team with 
          marketing advice, technological questions, or offer community best practices 
          to help your project during any stage.
        </li>
        <li>
          <b>IEEE</b>: Does your project align with existing IEEE groups or standards?
        </li>
        <li>
          <b>Source code</b>:Will you require a place to host and maintain your source code? 
        </li>
        <li>
          <b>Professional Services</b>: Do you need assistance with membership 
          or events to help further your project?
        </li>
        </ul>
          <h1>Our Recommendations</h1>: 
        <p>
            We share our recommendations on how we can help you accomplish your organizational 
          goals, and together we finalize your custom solutions to get your organization started.  
        </p>
    </Section>
    <Section type={'dark'}>
      <div className={'center'}>
        <h4>Ready to Get Started?</h4>
        <p>
          Join the IEEE SA OPEN Community and get started on your project today!
        </p>
        <Button link={'mailto:opensource@ieee.org'}>Contact Us</Button>
      </div>
    </Section>
    <Section type={'light'}>
      <a className={'anchor'} id={'community'} />
      <h1>IEEE SA OPEN Community</h1>
      <h5>The Community you need.</h5>
      <p>
        <b>IEEE SA OPEN Community</b> is IEEE's open source software, open hardware, 
        and open data project platform that enables the open source community to create, 
        collaborate, and manage their projects for free.
      </p>
      <p>
        GitLab, our open source technology platform, includes source code management, 
        project planning, continuous integration, community organization, CLAs, governance, 
        and document processing. The platform currently features Redmine, a flexible project 
        management application, which is the first of many integrations to come.
      </p>
      <p>
        We have everything you need to successfully develop, launch, and maintain an 
        open source project. IEEE SA Open is here to help bridge the gap between 
        standards development and open source communities.
      </p>
      <ul>
        <li>
          <b>Membership</b>: <br>
          </br>Our FREE membership is for anyone interested in
          contributing or managing with an open source project. 
          You and your team are welcome to use our platform for 
          general collaboration.
        </li>
        <li>
          <b>Governance</b>:<br> 
          </br>We have three open source software licenses available. Two
          for software, one for hardware and a future license for open data. 
          All licensing is subject to IEEE SA OPEN general governance.
        </li>
        <li>
          <b>Open Source</b>:<br>
          </br>All projects must be open source software, open hardware, or open data.
        </li>
        <li>
          <b>Technology</b>:<br>
          </br>IEEE SA OPEN Community is a customized technology platform built on GitLab CE, 
          Mattermost, and Gatsby open source modules. More features and tool 
          integrations will be available to users soon.
        </li>
      </ul>
      <h4>Why host your open source project at IEEE SA OPEN?</h4>
      <ul>
        <li>Give your project the trust and authenticity of IEEE</li>
        <li>Access to IEEEs global membership</li>
        <li>Built in Contributor License Agreements (CLA)</li>
        <li>Access to technical, marketing and community best practices</li>
        <li>Easy to initiate a project</li>
        <li>Fast track to IEEE Standards</li>
        <li>Free</li>
      </ul>
    </Section>
    <Section type={'dark'}>
      <div className={'center'}>
        <h4>Ready to Get Started?</h4>
        <p>
          Join the IEEE SA OPEN Community and get started on your project today!
        </p>
        <Button link={'mailto:opensource@ieee.org'}>Contact Us</Button>
      </div>
    </Section>
    <Section type={'light'}>
      <a className={'anchor'} id={'venture'} />
      <h1>IEEE SA OPEN Venture</h1>
      <h5>Your organization’s customized open source solution.</h5>
      <p>
        <b>Open Ventures</b> enables rapid initiation of professional membership 
        open source organizations. With the IEEE’s decades of knowledge and 
        resources you can launch your organization in a matter of weeks instead 
        of months. Your custom branded community is built on top of the IEEE’s 
        SA OPEN Platform using GitLab, and includes technical, community, governance, 
        and licensing modules which are customized to your specific needs.
      </p>
      <ul>
        <li>
          <b>Membership</b>:<br>
          </br>IEEE SA OPEN will help you create your community and expand your membership.
        </li>
        <li>
          <b>Governance</b>:<br>
          </br>IEEE SA OPEN will assist you in designing governance and applicable licenses that work best for you.
        </li>
        <li>
          <b>Open Source</b>:<br>
          </br>We can work with you on implementing various Open source standards to support your projects.
        </li>
        <li>
          <b>Technology</b>:<br>
          </br>Custom site options built on IEEE SA OPEN platform and GitLab technologies. 
          IEEE SA OPEN VENTURE features comprehensive video conferencing by Big Blue Button 
          and an open source, flexible project management application by Redmine.   
          You'll receive regular upgrades as new features are created. 
        </li>
      </ul>
      <h4>Why create your business consortium using IEEE SA OPEN?</h4>
      <p>
        IEEE SA OPEN enables you to focus on the technical work by handling the
        legal and logistical activities necessary to have your organization
        incorporated in days, instead of months. ISTO IEEE takes care of the
        legal incorporation, licenses, website, and account management; while
        IEEE SA OPEN provides best practices to manage and grow your community.
      </p>
      <ul>
        <li>Give your project the trust and authenticity of IEEE</li>
        <li>A neutral, third-party administrator</li>
        <li>Open, flexible development environment</li>
        <li>Unique service add-ons and customization</li>
        <li>Easy for your members to initiate a project</li>
      </ul>
      <p>
        IEEE SA OPEN enables your organization to focus on the what of your
        legacy, not the how to do it. Contact us today to learn more.
      </p>
    </Section>
    <Section type={'dark'}>
      <a className={'anchor'} id={'philanthropy'} />
      <h1>IEEE SA OPEN Philanthropy</h1>
      <h5>Giving back to humanity one project at a time</h5>
      <p>
        <b>IEEE SA OPEN Philanthropy</b> helps you support a specific technology or 
        topic that benefits humanity. Donations made to the fund are used to support 
        the IEEE SA OPEN Community platform or support specific open source project 
        areas you want to help. A custom technology community is created inside of 
        the IEEE SA OPEN Platform and can be customized to fit your needs.
      </p>
      <ul>
        <li>
          <b>Membership</b>:<br>
          </br>Free to anyone working to address a specific topic, including existing IEEE members.
        </li>
        <li>
          <b>Governance</b>:<br>
          </br>Subject to IEEE and customized community governance.
        </li>
        <li>
          <b>Technology</b>:<br>
          </br>Community uses IEEE SA OPEN Community platform.
        </li>
      </ul>
      <h4>Why support mission-based technology projects with IEEE SA OPEN?</h4>
      <p>
        IEEE SA OPEN enables you to focus on mission-driven open source
        software, open hardware or open data projects, instead of the legal
        requirements and paperwork necessary to incorporate a foundation. IEEE
        SA OPEN takes care of the legal incorporation, licenses, website, and
        account management; while providing best practices to manage and grow
        your community.
      </p>
      <ul>
        <li>Give your project the trust and authenticity of IEEE</li>
        <li>A neutral, third-party administrator</li>
        <li>Open, flexible development environment</li>
        <li>Unique service add-ons and customization</li>
        <li>Easy for your members to initiate a project</li>
      </ul>
      <p>
        IEEE SA OPEN enables your organization to focus on the what of your
        legacy, not the how to do it. Contact us today to learn more.
      </p>
    </Section>
  </Layout>
);

export default Products;
