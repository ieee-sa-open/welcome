import React from 'react';
import styles from './Iframe.module.css';
import PropTypes from 'prop-types';

const Iframe = ({ src, height, width }) => {
  return (
    <div>
      <iframe
        className={styles.iframe}
        src={src}
        height={height}
        width={width}
      />
    </div>
  );
};

Iframe.propTypes = {
  src: PropTypes.string,
  height: PropTypes.string,
  width: PropTypes.string,
};

export default Iframe;
