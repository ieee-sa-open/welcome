import PropTypes from 'prop-types';
import React from 'react';
import styles from './Bio.module.css';

const Bio = ({ image, name, title, description }) => (
  <div className={styles.bio}>
    <div><img className={styles.bioImage} src={image} /></div>
    <div className={styles.bioTextContainer}>
      <div className={styles.bioTitle}>
        <b>{name}</b> {title}
      </div>
      <div className={styles.bioText}>{description}</div>
    </div>
  </div>
);

Bio.propTypes = {
  image: PropTypes.string,
  name: PropTypes.string,
  title: PropTypes.string,
  description: PropTypes.string,
};

Bio.defaultProps = {
  image: ``,
  name: ``,
  title: ``,
  description: ``,
};

export default Bio;
