import React from 'react';
import { Helmet } from 'react-helmet';
import { PageProps } from 'gatsby';

import Layout from '../components/Layout/Layout';
import Hero from '../components/Hero/Hero';
import Section from '../components/Section/Section';

import BBB_1 from '../images/bbb/BBB-join-with-mic.png';
import BBB_2 from '../images/bbb/BBB-echo-test.png';
import BBB_3 from '../images/bbb/BBB-shared-notes.png';
import BBB_4 from '../images/bbb/BBB-share-webcam.png';
import BBB_5 from '../images/bbb/BBB-share-webcam-permissions.png';
import BBB_6 from '../images/bbb/BBB-private-chat-start.png';
import BBB_7 from '../images/bbb/BBB-private-chat-pt2.png';


const ParticipantPrep: React.FC<PageProps> = () => (
  <Layout>
    <Helmet>
      <title>Community - IEEE SA OPEN</title>
    </Helmet>
    <Hero heroIntro={'IEEE SA OPEN'} heroPrimary={'Guides'} />
    <Section type={'light'}>
    <h6>Preparing for Video Conferencing on BigBlueButton</h6>

    <h6>Testing Your Equipment Before the Event</h6>

<p>Go to the BigBlueButton test site to test your browser and equipment: <a href="https://test.bigbluebutton.org/" target="_blank">https://test.bigbluebutton.org/</a>
</p>

<p>
<ul>
<li>Check to make sure headset and mic works</li>
<li>Check your internet connection</li>
</ul>
</p>

<p>The BigBlueButton room runs in the internet browser window and works best with Google Chrome and Firefox</p>

<h6>Joining the Meeting</h6>

<p>
<ul>
<li>Open the event link in Google Chrome or Mozilla Firefox</li>
<li>Put your name in the box and click join</li>
<li>When prompted “How would you like to join the audio?” select JOIN WITH MICROPHONE</li>
<div className={'center'}>
<p><img alt={'join with mic'} src={BBB_1} /></p>
</div>

<li>Do the echo test to ensure your microphone and speakers are configured correctly</li>
<div className={'center'}>
<p><img alt={'echo test'} src={BBB_2} /></p>
</div>

<li>Mute your audio when you join the video conference room using the mic button at the bottom of the screen. You can also click on your user name and select mute</li>
<li>Put your name and affiliation in the shared notes on the left side bar</li>
</ul>
</p>
<div className={'center'}>
<p><img alt={'shared notes'} src={BBB_3} /></p>
</div>

<h6>Participating in the Meeting</h6>
<p>Unmute to speak</p>
<p>
<ul>
<li>If you want to speak click on the mic button at the bottom of the screen or click your user name and select unmute in the User list on the left side of the screen
</li>
</ul>
</p>
<p>Turn on webcam</p>
<p>
<ul>
<li>If you want to join the meeting with video, select the camera icon at the bottom of the screen.
</li>
<div className={'center'}>
<p><img alt={'share webcam'} src={BBB_4} /></p>
</div>
<li>Give your browser permission to access your webcam when the permission request pops up</li>
<div className={'center'}>
<p><img alt={'webcam permissions'} src={BBB_5} /></p>
</div>
</ul>
</p>


<p>Start a private chat</p>
<p>
<ul>
<li>Click on the users name in the user list and select START A PRIVATE CHAT</li>
<div className={'center'}>
<p><img alt={'start private chat'} src={BBB_6} /></p>
</div>

<li>The private chat can be accessed under the public chat tab on the left side</li>
<div className={'center'}>
<p><img alt={'start private chat part 2'} src={BBB_7} /></p>
</div>
</ul>
</p>

<p>Use public chat</p>
<p>
<ul>
<li>The public chat is the first thing visible in the left pane when you log onto the BigBlueButton meeting</li>
<li>Any public conversations that occurred before you joined the meeting will be above the welcome box in the public chat</li>
<li>The public chat is not recorded in the meeting recording. If there are notes or information you want recorded for later reference, put it in the shared notes if they are being used for the meeting. Shared notes can be copy and pasted into another location at the end of the meeting</li>
</ul>
</p>

<p>Collaborate in shared notes</p>
<p>
<ul>
<li>Any user in the meeting can write on the shared notes pad</li>
<li>Open shared notes and type in the window.</li> 
<div className={'center'}>
<p><img alt={'shared notes'} src={BBB_3} /></p>
</div>
<li>The user name does not appear next to the text that each person types. If you want your name attached to what you contribute in the notes, you will need to put your name in the text</li>
<li>Shared notes do not get captured in the recording. If you want a copy of the notes at the end of the meeting, you will need to copy and paste them into another location</li>
</ul>
</p>

<h6>Conferencing etiquette</h6>
<p>
<ul>
<li>Choose a quiet location without distractions</li>
<li>Use a headset to eliminate background noise and feedback that your microphone picks up</li>
<li>If you will have your webcam turned on, keep in mind that BigBlueButton does not have virtual backgrounds. Sit in a place that has a background you are okay with other people seeing</li>
<li>Stay muted when you are not speaking to prevent background noise and feedback</li>
</ul>
</p>    
    </Section>
  </Layout>
);

export default ParticipantPrep;
